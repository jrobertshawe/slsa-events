//
//  SelectLocation.h
//  SLSA Risk Assessment
//
//  Created by Julian on 13/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreData/CoreData.h>

@interface SelectLocation : UIViewController <MKMapViewDelegate> {
    
    MKMapView *mapView;
    NSManagedObject*       entity; 
    NSString*               entityKey;
    float latitude;
    float longitude;
    CLLocationManager* locationManager;
	CLLocation* currentLocation;
	IBOutlet UISegmentedControl *mapType;
    NSString*               notifyQueue;  
}
@property (nonatomic,retain)          NSString*               notifyQueue; 
@property (nonatomic, retain) IBOutlet MKMapView *mapView;

- (BOOL) writeApplicationData:(NSData *)data toFile:(NSString *)fileName;
@property (nonatomic,retain)  NSManagedObject						*entity;
@property (nonatomic,retain)  NSString                              *entityKey;


@property float latitude;
@property float longitude;
@property (nonatomic, retain) NSManagedObject *selectedObject;
@property (nonatomic, retain) CLLocationManager* locationManager;
@property (nonatomic, retain) CLLocation* currentLocation;
-(IBAction)DoneButtonPressed;
-(IBAction)CaptureButtonPressed;
- (IBAction)changeType:(id) sender;
@end

