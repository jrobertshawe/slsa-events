//
//  GenerateReport.m
//  SLSARiskAssessment
//
//  Created by Julian on 17/12/10.
//  Copyright 2010 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "GenerateReport.h"
#import "EventAppDelegate.h"

@implementation GenerateReport
@synthesize webView;
@synthesize assessment;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *navBar;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        navBar = @"ipad_img_blue-header.png";
    }
    else {
        navBar = @"iphone_img_blueheader.png";
    }
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:navBar];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;

    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonImage = [UIImage imageNamed:@"but_done_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(displayComposerSheet) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.rightBarButtonItem = backButtonItem;
    self.title = @"Report";
    	
	fileName = @"report.pdf";
}
- (void) didTapBackButton:(id)sender {
    
    
    
    
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return NO;
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)viewWillAppear:(BOOL)animated
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *saveDirectory = [paths objectAtIndex:0];
	NSString *newFilePath = [saveDirectory stringByAppendingPathComponent:fileName];
	
	[self CreatePDFFile:CGRectMake(0, 0, PAGEWIDTH, PAGEHEIGHT)];
	
    NSURL *url = [NSURL fileURLWithPath:newFilePath];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [webView loadRequest:urlRequest];
    
}

-(void)CreatePDFFile:(CGRect) pageRect
{
	kmlMapString = [[NSMutableString alloc] init];
	[kmlMapString appendString:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n"];
	[kmlMapString appendString:@"<Document>\n<name>map.kml</name>\n"];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	
	//save the PDF
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *saveDirectory = [paths objectAtIndex:0];
	NSString *newFilePath = [saveDirectory stringByAppendingPathComponent:fileName];
	const char *filename = [newFilePath UTF8String];
	
	// This code block sets up our PDF Context so that we can draw to it
	CGContextRef pdfContext;
	CFStringRef path;
	CFURLRef url;
	CFMutableDictionaryRef myDictionary = NULL;
	
	// Create a CFString from the filename we provide to this method when we call it
	path = CFStringCreateWithCString (NULL, filename,
									  kCFStringEncodingUTF8);
	
	// Create a CFURL using the CFString we just defined
	url = CFURLCreateWithFileSystemPath (NULL, path,
										 kCFURLPOSIXPathStyle, 0);
	CFRelease (path);
	
	// This dictionary contains extra options mostly for 'signing' the PDF
	myDictionary = CFDictionaryCreateMutable(NULL, 0,
											 &kCFTypeDictionaryKeyCallBacks,
											 &kCFTypeDictionaryValueCallBacks);
	CFDictionarySetValue(myDictionary, kCGPDFContextTitle, CFSTR("My PDF File"));
	CFDictionarySetValue(myDictionary, kCGPDFContextCreator, CFSTR("My Name"));
	
	// Create our PDF Context with the CFURL, the CGRect we provide, and the above defined dictionary
	pdfContext = CGPDFContextCreateWithURL (url, &pageRect, myDictionary);

	
	// Cleanup our mess
	CFRelease(myDictionary);
	CFRelease(url);
	// Done creating our PDF Context, now it's time to draw to it
	
	// Starts our first page
	CGContextBeginPage (pdfContext, &pageRect);
	
	NSInteger VerticalPos = pageRect.size.height - 10;
	NSString* beachName;

    
	localSite  = assessment.LocalSite;

	
    
	VerticalPos -=20;
    
  
	
	VerticalPos -=100;

    //Events  Box
	CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
	CGContextStrokePath(pdfContext);
    
    
    int columnspace = (RIGHTMARGIN-LEFTMARGIN)/6;
    int topofBox = VerticalPos;
    VerticalPos -=12;
    NSString* EventLabel = @"Events:";
    [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+50 yPos:VerticalPos str:[EventLabel UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    int colPos = 1;
    for (Event* event in assessment.Events)
    {
        [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*colPos)+columnspace/2 yPos:VerticalPos str:[event.name UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
        
         
        
        colPos++;
        if (colPos == 6)
        {
            CGContextMoveToPoint(pdfContext,LEFTMARGIN+columnspace*1,VerticalPos-5);
        CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos-5);   
        VerticalPos-=16;
        colPos = 1;
        }
    }
    
     VerticalPos -=4;
    
    CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
	CGContextStrokePath(pdfContext);
    
    
    
    
    for (int ic=1;ic<6;ic++)
    {
        
        CGContextMoveToPoint(pdfContext,LEFTMARGIN+columnspace*ic,VerticalPos);
        CGContextAddLineToPoint(pdfContext,LEFTMARGIN+columnspace*ic,topofBox);
        CGContextStrokePath(pdfContext);
    }

    CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,LEFTMARGIN,topofBox);
	CGContextStrokePath(pdfContext);
    
    CGContextMoveToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,topofBox);
	CGContextStrokePath(pdfContext);
    
    
    VerticalPos -=20;
    
    
    //Weather Cond.
     NSString* WeatherLabel = @"Weather Conditions:";
    
     [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+10 yPos:VerticalPos str:[WeatherLabel UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    VerticalPos -=10;
    
    CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
	CGContextStrokePath(pdfContext);
    
       columnspace = (RIGHTMARGIN-LEFTMARGIN)/3;
    topofBox = VerticalPos;
    VerticalPos -=15;
    colPos = 1;
    [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*1)+columnspace/2 yPos:VerticalPos str:[@"PREVAILLING" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*2)+columnspace/2 yPos:VerticalPos str:[@"PREDICTED" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
   
      VerticalPos -=10;
    CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
	CGContextStrokePath(pdfContext);

     VerticalPos -=15;
     [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+50 yPos:VerticalPos str:[@"Air Temperature" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*1)+columnspace/2 yPos:VerticalPos str:[assessment.CurrentWeatherTemp UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
    [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*2)+columnspace/2 yPos:VerticalPos str:[assessment.PredictionWeatherTemp UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
    
    VerticalPos -=10;
    CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
	CGContextStrokePath(pdfContext);
    
    VerticalPos -=15;
    [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+50 yPos:VerticalPos str:[@"Wind Direction" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*1)+columnspace/2 yPos:VerticalPos str:[assessment.CurrentWindDirection UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
    [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*2)+columnspace/2 yPos:VerticalPos str:[assessment.PredictionWindDirection UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];

    VerticalPos -=10;
    CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
	CGContextStrokePath(pdfContext);
    
    VerticalPos -=15;
    [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+50 yPos:VerticalPos str:[@"Wind Speed" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*1)+columnspace/2 yPos:VerticalPos str:[assessment.CurrentWindSpeed UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
    [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*2)+columnspace/2 yPos:VerticalPos str:[assessment.PredictionWindSpeed UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
    
    VerticalPos -=10;
    CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
	CGContextStrokePath(pdfContext);
    
    VerticalPos -=15;
    [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+50 yPos:VerticalPos str:[@"Weather" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*1)+columnspace/2 yPos:VerticalPos str:[assessment.CurrentCloud UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
    [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*2)+columnspace/2 yPos:VerticalPos str:[assessment.PredictionCloud UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
    
      VerticalPos -=10;
    
    for (int ic=1;ic<3;ic++)
    {
        
        CGContextMoveToPoint(pdfContext,LEFTMARGIN+columnspace*ic,VerticalPos);
        CGContextAddLineToPoint(pdfContext,LEFTMARGIN+columnspace*ic,topofBox);
        CGContextStrokePath(pdfContext);
    }

    CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,LEFTMARGIN,topofBox);
	CGContextStrokePath(pdfContext);
    
    CGContextMoveToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,topofBox);
	CGContextStrokePath(pdfContext);
    
    CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
	CGContextStrokePath(pdfContext);
    
    VerticalPos -=20;
    
    if (assessment.CurrentSwellSize != nil)
    {
        //Surf Cond.
        NSString* SurfLabel = @"Surf Conditions:";
        
        [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+10 yPos:VerticalPos str:[SurfLabel UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
        VerticalPos -=10;
        
        CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
        CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
        CGContextStrokePath(pdfContext);
        
        columnspace = (RIGHTMARGIN-LEFTMARGIN)/3;
        topofBox = VerticalPos;
        VerticalPos -=15;
        colPos = 1;
        [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*1)+columnspace/2 yPos:VerticalPos str:[@"PREVAILLING" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
        [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*2)+columnspace/2 yPos:VerticalPos str:[@"PREDICTED" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
        
        VerticalPos -=10;
        CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
        CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
        CGContextStrokePath(pdfContext);
        
        VerticalPos -=15;
        [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+50 yPos:VerticalPos str:[@"Swell Size" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
        [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*1)+columnspace/2 yPos:VerticalPos str:[assessment.CurrentSwellSize UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
       [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*2)+columnspace/2 yPos:VerticalPos str:[assessment.PredictionSwellSize UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
        
        VerticalPos -=10;
        CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
        CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
        CGContextStrokePath(pdfContext);
        
        VerticalPos -=15;
        [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+50 yPos:VerticalPos str:[@"Wave Type" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
          [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*1)+columnspace/2 yPos:VerticalPos str:[assessment.CurrentWaveType UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
        [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*2)+columnspace/2 yPos:VerticalPos str:[assessment.PredictionWaveType UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
        
        VerticalPos -=10;
        CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
        CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
        CGContextStrokePath(pdfContext);
        
        VerticalPos -=15;
        [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+50 yPos:VerticalPos str:[@"Tide" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
        [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*1)+columnspace/2 yPos:VerticalPos str:[assessment.CurrentTide UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
        
        NSString* tidetimes = [NSString stringWithFormat:@"LOW:%@ High:%@",assessment.PredictionLowTide,assessment.PredictionHighTide];
        [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*2)+columnspace/2 yPos:VerticalPos str:[tidetimes UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
        
        VerticalPos -=10;
        CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
        CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
        CGContextStrokePath(pdfContext);
        
        VerticalPos -=15;
        [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+50 yPos:VerticalPos str:[@"Swell Period" UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
        [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*1)+columnspace/2 yPos:VerticalPos str:[assessment.CurrentSwellTime UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
        [self DrawStringCentered:pdfContext xPos:(LEFTMARGIN+columnspace*2)+columnspace/2 yPos:VerticalPos str:[assessment.PredictionSwellTime UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
        
        VerticalPos -=10;
        
        for (int ic=1;ic<3;ic++)
        {
            
            CGContextMoveToPoint(pdfContext,LEFTMARGIN+columnspace*ic,VerticalPos);
            CGContextAddLineToPoint(pdfContext,LEFTMARGIN+columnspace*ic,topofBox);
            CGContextStrokePath(pdfContext);
        }
        
        CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
        CGContextAddLineToPoint(pdfContext,LEFTMARGIN,topofBox);
        CGContextStrokePath(pdfContext);
        
        CGContextMoveToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
        CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,topofBox);
        CGContextStrokePath(pdfContext);
        
        CGContextMoveToPoint(pdfContext,LEFTMARGIN,VerticalPos);
        CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,VerticalPos);
	CGContextStrokePath(pdfContext);
    }
    VerticalPos -=40;
    
    //Notes.

    
     
    
    //Sighted.
    NSString* SightedLabel = @"Sighted By:";
    
    [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN yPos:VerticalPos str:[SightedLabel UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    CGContextMoveToPoint(pdfContext,LEFTMARGIN+250,VerticalPos);
	CGContextAddLineToPoint(pdfContext,LEFTMARGIN+500,VerticalPos);
    CGContextStrokePath(pdfContext);
     [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+100 yPos:VerticalPos str:[@"Name:"  UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    
    [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+250 yPos:VerticalPos+5 str:[localSite.GovernmentArea.ContactName  UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    
    
    VerticalPos -=30;
    
     [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+100 yPos:VerticalPos str:[@"Signature:"  UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    
    CGContextMoveToPoint(pdfContext,LEFTMARGIN+250,VerticalPos);
	CGContextAddLineToPoint(pdfContext,LEFTMARGIN+500,VerticalPos);
    CGContextStrokePath(pdfContext);
    
    VerticalPos -=30;
    
     [self DrawStringLeftAlign:pdfContext xPos:LEFTMARGIN+100 yPos:VerticalPos str:[@"Postion:"  UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    
    CGContextMoveToPoint(pdfContext,LEFTMARGIN+250,VerticalPos);
	CGContextAddLineToPoint(pdfContext,LEFTMARGIN+500,VerticalPos);
    CGContextStrokePath(pdfContext);

    
    
    UIGraphicsPushContext(pdfContext);
    CGContextTranslateCTM(pdfContext, 0, 20);
    CGContextScaleCTM(pdfContext, 1.0, -1.0);
    VerticalPos = [self WriteHeaderBlock2:pdfContext StartyPostion:VerticalPos];
    
    
    CGContextEndPage (pdfContext);
    UIGraphicsPopContext();

    
    if (assessment.Notes )
    {
        
        VerticalPos = pageRect.size.height - 30;
        CGContextBeginPage (pdfContext, &pageRect);
        
        
        VerticalPos -=100;  
        
        NSString* NotesLabel = @"Notes:";
        
        UIGraphicsPushContext(pdfContext);
        CGContextTranslateCTM(pdfContext, 0, 20);
        CGContextScaleCTM(pdfContext, 1.0, -1.0);
        
        [self DrawNSStringLeftAlign:pdfContext xPos:LEFTMARGIN+10 yPos:VerticalPos str:NotesLabel  textBold:YES fontsizing:LARGEFONTSIZE];
        VerticalPos -=10;
        
        
        
        
			//quote Terms
			NSArray *lineItems  = [assessment.Notes componentsSeparatedByString:@"\n"];
				
			
			int line = 0;
			
			for (NSString* lineString in lineItems)
			{
				
				line++;
				[self DrawNSStringLeftAlign:pdfContext xPos:LEFTMARGIN+10 yPos:VerticalPos -line*13 str:lineString  textBold:NO fontsizing:LARGEFONTSIZE];
				
				
			}
	     VerticalPos = [self WriteHeaderBlock2:pdfContext StartyPostion:VerticalPos];
        
         
        CGContextEndPage (pdfContext);
        	UIGraphicsPopContext();
    }
    
    
    for (NSManagedObject* managedObject in assessment.Hazzards)
	{
		if ([managedObject isKindOfClass:[Hazard class]]) //It is a risk object so update data
		{
                  VerticalPos = pageRect.size.height - 30;
    CGContextBeginPage (pdfContext, &pageRect);
    
            UIGraphicsPushContext(pdfContext);
            CGContextTranslateCTM(pdfContext, 0, 20);
            CGContextScaleCTM(pdfContext, 1.0, -1.0);
            VerticalPos = [self WriteHeaderBlock2:pdfContext StartyPostion:VerticalPos];
            
            
          VerticalPos -=20;  
	//Hazard Title Box
	CGContextMoveToPoint(pdfContext,LEFTMARGIN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,20-VerticalPos);
	CGContextStrokePath(pdfContext);
	
	CGContextMoveToPoint(pdfContext,LEFTMARGIN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,LEFTMARGIN,20-VerticalPos+TITLEROWHEIGHT);
	CGContextStrokePath(pdfContext);
	
	CGContextMoveToPoint(pdfContext,RIGHTMARGIN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,20-VerticalPos+TITLEROWHEIGHT);
	CGContextStrokePath(pdfContext);
	
	CGContextMoveToPoint(pdfContext,HAZARDCOLUMN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,HAZARDCOLUMN,20-VerticalPos+TITLEROWHEIGHT);
	CGContextStrokePath(pdfContext);
	
	CGContextMoveToPoint(pdfContext,PHOTOCOLUMN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,PHOTOCOLUMN,20-VerticalPos+TITLEROWHEIGHT);
	CGContextStrokePath(pdfContext);

	CGContextMoveToPoint(pdfContext,RISKCOLUMN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,RISKCOLUMN,20-VerticalPos+TITLEROWHEIGHT);
	CGContextStrokePath(pdfContext);
	
	CGContextMoveToPoint(pdfContext,RISKSCORECOLUMN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,RISKSCORECOLUMN,20-VerticalPos+TITLEROWHEIGHT);
	CGContextStrokePath(pdfContext);
    
    CGContextMoveToPoint(pdfContext,CONSEQCOLUMN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,CONSEQCOLUMN,20-VerticalPos+TITLEROWHEIGHT);
	CGContextStrokePath(pdfContext);
    
    CGContextMoveToPoint(pdfContext,RISKSCOREMIDDLECOLUMN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,RISKSCOREMIDDLECOLUMN,20-VerticalPos+TITLEROWHEIGHT);
	CGContextStrokePath(pdfContext);
    
	
	CGContextMoveToPoint(pdfContext,EXISTINGCONTROLSCOLUMN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,EXISTINGCONTROLSCOLUMN,20-VerticalPos+TITLEROWHEIGHT);
	CGContextStrokePath(pdfContext);
	
	CGContextMoveToPoint(pdfContext,RISKRATINGCOLUMN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,RISKRATINGCOLUMN,20-VerticalPos+TITLEROWHEIGHT);
	CGContextStrokePath(pdfContext);
	
	CGContextMoveToPoint(pdfContext,PROPOSEDCONTROLSCOLUMN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,PROPOSEDCONTROLSCOLUMN,20-VerticalPos+TITLEROWHEIGHT);
	CGContextStrokePath(pdfContext);
 
	
	[self drawRectGrey:pdfContext drawrect:CGRectMake(LEFTMARGIN,20-VerticalPos, RIGHTMARGIN-LEFTMARGIN  , TITLEROWHEIGHT)];
	[self DrawNSStringCenteredWhite:pdfContext xPos:HAZARDCOLUMN-(HAZARDCOLUMN-LEFTMARGIN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/2) str:@"Hazard" textBold:YES];
	[self DrawNSStringCenteredWhite:pdfContext xPos:PHOTOCOLUMN-(PHOTOCOLUMN-HAZARDCOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/2) str:@"Photo" textBold:YES];
	
	[self DrawNSStringCenteredWhite:pdfContext xPos:RISKCOLUMN-(RISKCOLUMN-PHOTOCOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/3) str:@"Risk associated" textBold:YES];
	[self DrawNSStringCenteredWhite:pdfContext xPos:RISKCOLUMN-(RISKCOLUMN-PHOTOCOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/3)-(TITLEROWHEIGHT/3) str:@"with hazard" textBold:YES];
	[self DrawNSStringCenteredWhite:pdfContext xPos:RISKSCORECOLUMN-(RISKSCORECOLUMN-RISKCOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/3) str:@"Risk Group" textBold:YES];
    
	[self DrawNSStringCenteredWhite:pdfContext xPos:CONSEQCOLUMN-(CONSEQCOLUMN-RISKSCORECOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/2) str:@"Consequence" textBold:YES];
	[self DrawNSStringCenteredWhite:pdfContext xPos:RISKSCOREMIDDLECOLUMN-(RISKSCOREMIDDLECOLUMN-CONSEQCOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/2) str:@"Likelihood" textBold:YES];
	[self DrawNSStringCenteredWhite:pdfContext xPos:EXISTINGCONTROLSCOLUMN-(EXISTINGCONTROLSCOLUMN-RISKSCOREMIDDLECOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/2) str:@"Existing Controls -Adequacy" textBold:YES];
	[self DrawNSStringCenteredWhite:pdfContext xPos:RISKRATINGCOLUMN-(RISKRATINGCOLUMN-EXISTINGCONTROLSCOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/3) str:@"Risk" textBold:YES];
	[self DrawNSStringCenteredWhite:pdfContext xPos:RISKRATINGCOLUMN-(RISKRATINGCOLUMN-EXISTINGCONTROLSCOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/3)-(TITLEROWHEIGHT/3) str:@"Rating" textBold:YES];
	[self DrawNSStringCenteredWhite:pdfContext xPos:PROPOSEDCONTROLSCOLUMN-(PROPOSEDCONTROLSCOLUMN-RISKRATINGCOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/2) str:@"Proposed Risk Treatment" textBold:YES];
	[self DrawNSStringCenteredWhite:pdfContext xPos:RIGHTMARGIN-(RIGHTMARGIN-PROPOSEDCONTROLSCOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/3) str:@"Residual" textBold:YES];
    [self DrawNSStringCenteredWhite:pdfContext xPos:RIGHTMARGIN-(RIGHTMARGIN-PROPOSEDCONTROLSCOLUMN)/2 yPos:VerticalPos-(TITLEROWHEIGHT/3)-(TITLEROWHEIGHT/3) str:@"Risk" textBold:YES];
	VerticalPos -= TITLEROWHEIGHT;
	CGContextMoveToPoint(pdfContext,LEFTMARGIN,20-VerticalPos);
	CGContextAddLineToPoint(pdfContext,RIGHTMARGIN,20-VerticalPos);
	CGContextStrokePath(pdfContext);


			Hazard* hazard = (Hazard*) managedObject;
			[kmlMapString appendString:@"<Placemark>\n"];
			[kmlMapString appendFormat:@"<name>%@</name>\n",[[hazard valueForKey:@"Description"] description]];
			[kmlMapString appendString:@"<Point>\n"];
			[kmlMapString appendFormat:@"<coordinates>%f,%f</coordinates>\n", [[hazard Longitude] doubleValue],[[hazard Latitute] doubleValue]];
			[kmlMapString appendString:@"</Point>\n"];
       
			VerticalPos = [self WriteHazardLine:pdfContext HazardObject:hazard StartyPostion:VerticalPos];
			[kmlMapString appendFormat:@"</Placemark>\n"];
	
	
	
	
	// We are done drawing to this page, let's end it
	CGContextEndPage (pdfContext);
            	UIGraphicsPopContext();
		}
	}
    
	// We are done with our context now, so we release it
	CGContextRelease (pdfContext);
	[kmlMapString appendString:@"</Document></kml>\n"];
	kmlfileName = @"map.kml";
	newFilePath = [saveDirectory stringByAppendingPathComponent:kmlfileName];

	
	//create content - four lines of text
	//save content to the documents directory
	[kmlMapString writeToFile:newFilePath 
			  atomically:NO 
				encoding:NSStringEncodingConversionAllowLossy 
				   error:nil];

}
- (int)WriteHeaderBlock2:(CGContextRef) context StartyPostion:(int)startyPostion
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
    NSDate * assementDate = [assessment valueForKey:@"AssessmentDate"];
    
    NSString* AreaLabel = @"Area:";
    NSString* AssessmentLabel = @"Assessment:";
    NSString* DateLabel = @"Date & Time";
    NSString* AssessorLabel = @"Completed By:";
    int VerticalPos = PAGEHEIGHT-30;
    [self DrawNSStringLeftAlign:context xPos:LEFTMARGIN+790 yPos:VerticalPos+10 str:@"v 1.0"   textBold:NO fontsizing:FONTSIZE];
    
    [self DrawNSStringLeftAlign:context xPos:LEFTMARGIN yPos:VerticalPos str:AreaLabel   textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawNSStringLeftAlign:context xPos:LEFTMARGIN+100 yPos:VerticalPos str:localSite.Name   textBold:NO fontsizing:LARGEFONTSIZE];
    VerticalPos -=20;
    
    [self DrawNSStringLeftAlign:context xPos:LEFTMARGIN yPos:VerticalPos str:AssessmentLabel   textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawNSStringLeftAlign:context xPos:LEFTMARGIN+100 yPos:VerticalPos str:assessment.Description textBold:NO fontsizing:LARGEFONTSIZE];
 	
    VerticalPos -=20;
    [self DrawNSStringLeftAlign:context xPos:LEFTMARGIN yPos:VerticalPos str:DateLabel   textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawNSStringLeftAlign:context xPos:LEFTMARGIN+100 yPos:VerticalPos str:[formatter stringFromDate:assementDate]   textBold:NO fontsizing:LARGEFONTSIZE];
    VerticalPos -=20;
    [self DrawNSStringLeftAlign:context xPos:LEFTMARGIN yPos:VerticalPos str:AssessorLabel   textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawNSStringLeftAlign:context xPos:LEFTMARGIN+100 yPos:VerticalPos str:assessment.Assessor  textBold:NO fontsizing:LARGEFONTSIZE];
    
   
    UIImage *image  = [UIImage imageNamed:@"aussie.png"];
    
	
    ///  this is where we work out how to size the resize image without it looking funny
    CGFloat width =100;
    CGFloat height = 100;
    
    CGRect bounds = CGRectMake(0, 0, width, height);
    bounds.size.height = 100;
    bounds.size.width = 100;
    width =  bounds.size.width;
    height = bounds.size.height;
    [image drawInRect:CGRectMake(700, -575, bounds.size.width, bounds.size.height)];

    
    
       return VerticalPos;
}
- (int)WriteHeaderBlock:(CGContextRef) context StartyPostion:(int)startyPostion
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
    NSDate * assementDate = [assessment valueForKey:@"AssessmentDate"];
    
   NSString* AreaLabel = @"Area:";
    NSString* AssessmentLabel = @"Assessment:";
    NSString* DateLabel = @"Date & Time";
    NSString* AssessorLabel = @"Completed By:";
    int VerticalPos = startyPostion;
    [self DrawStringLeftAlign:context xPos:LEFTMARGIN yPos:VerticalPos str:[AreaLabel UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawStringLeftAlign:context xPos:LEFTMARGIN+100 yPos:VerticalPos str:[localSite.Name UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
    VerticalPos -=20;
    
    [self DrawStringLeftAlign:context xPos:LEFTMARGIN yPos:VerticalPos str:[AssessmentLabel UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawStringLeftAlign:context xPos:LEFTMARGIN+100 yPos:VerticalPos str:[assessment.Description UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
 	
    VerticalPos -=20;
    [self DrawStringLeftAlign:context xPos:LEFTMARGIN yPos:VerticalPos str:[DateLabel UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawStringLeftAlign:context xPos:LEFTMARGIN+100 yPos:VerticalPos str:[[formatter stringFromDate:assementDate] UTF8String]  textBold:NO fontsizing:LARGEFONTSIZE];
    VerticalPos -=20;
    [self DrawStringLeftAlign:context xPos:LEFTMARGIN yPos:VerticalPos str:[AssessorLabel UTF8String]  textBold:YES fontsizing:LARGEFONTSIZE];
    [self DrawStringLeftAlign:context xPos:LEFTMARGIN+100 yPos:VerticalPos str:[assessment.Assessor UTF8String] textBold:NO fontsizing:LARGEFONTSIZE];
    
 
    
    
    
    
  
    
    
    
    return VerticalPos;
}
- (int)WriteHazardLine:(CGContextRef) context HazardObject:(Hazard* )hazard StartyPostion:(int)startyPostion
{
	EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];

	NSMutableString *stringWrap = [[NSMutableString alloc] init];
	int topOfRow  = startyPostion;
	startyPostion -= LINEHEIGHT;
	int endyPostion = startyPostion;
	int lineOffset = 0;
	lineOffset -=  [self DrawNSStringLeftAlignWrap:context xPos:HAZARDCOLUMN-(HAZARDCOLUMN-LEFTMARGIN)+5 yPos:startyPostion+lineOffset str:[[hazard valueForKey:@"Description"] description] textBold:NO Width:HAZARDCOLUMN-LEFTMARGIN-30]+5;
	if (startyPostion+lineOffset < endyPostion)
		endyPostion = startyPostion+lineOffset;
	for (Photo* photoItem in hazard.Photos)
	{
		NSString *photoImage = [NSString stringWithFormat:@"small_%@",[photoItem Name]];
		
		
		NSString* photoImageFile = [[delegate applicationDocumentsDirectory] stringByAppendingPathComponent:photoImage];
		UIImage *image  = [UIImage imageWithContentsOfFile:photoImageFile];
		
	
		///  this is where we work out how to size the resize image without it looking funny
		CGFloat width =image.size.width;
		CGFloat height = image.size.height;
		
		CGRect bounds = CGRectMake(0, 0, width, height);
		if (width > PHOTOCOLUMN-HAZARDCOLUMN || height > (PHOTOCOLUMN-HAZARDCOLUMN)+25)
		{
			CGFloat ratio = width/height;
			if (ratio > 1)  ///keep the height and reduce the width
			{
				bounds.size.width = PHOTOCOLUMN-HAZARDCOLUMN;
				bounds.size.height = bounds.size.width / ratio;
				width =  PHOTOCOLUMN-HAZARDCOLUMN/( image.size.height/PHOTOCOLUMN-HAZARDCOLUMN);
				height = (PHOTOCOLUMN-HAZARDCOLUMN)+25;
				
			}
			else if (ratio == 1)  //it is a square
			{
				bounds.size.height = PHOTOCOLUMN-HAZARDCOLUMN;
				bounds.size.width = bounds.size.height * ratio;
				width =  bounds.size.width;
				height = bounds.size.height;
			}
			else   ///keep the width and reduce the height
			{
				CGFloat ratio2 = height/width;
				bounds.size.width = PHOTOCOLUMN-HAZARDCOLUMN;
				bounds.size.height = bounds.size.width * ratio2;;
				width =  PHOTOCOLUMN-HAZARDCOLUMN;
				height = (PHOTOCOLUMN-HAZARDCOLUMN) /( image.size.width/(PHOTOCOLUMN-HAZARDCOLUMN));
			}
		}
     	[image drawInRect:CGRectMake(PHOTOCOLUMN-(PHOTOCOLUMN-HAZARDCOLUMN), 5-startyPostion-lineOffset, bounds.size.width, bounds.size.height)];
		lineOffset -= bounds.size.height;
		
	}
	
	if (startyPostion+lineOffset < endyPostion)
		endyPostion = startyPostion+lineOffset;
	
	
	//redraw the image with the new size
	lineOffset = 0;
		
	
	
	for (Risk* risk in hazard.Risks)
	{
	
		lineOffset -= [self DrawNSStringLeftAlignWrap:context xPos:RISKCOLUMN-(RISKCOLUMN-PHOTOCOLUMN)+5 yPos:startyPostion+lineOffset str:[[risk valueForKey:@"Description"] description] textBold:NO Width:RISKSCORECOLUMN-RISKCOLUMN ];
			lineOffset -= LINEHEIGHT;
	}
	if (startyPostion+lineOffset < endyPostion)
		endyPostion = startyPostion+lineOffset;
   lineOffset = 0;
	
	for (RiskGroup* riskGroup in hazard.RiskGroups)
	{
		
		lineOffset -=  [self DrawNSStringLeftAlignWrap:context xPos:RISKSCORECOLUMN-(RISKSCORECOLUMN-RISKCOLUMN)+5 yPos:startyPostion+lineOffset str:[[riskGroup valueForKey:@"Description"] description] textBold:NO Width:CONSEQCOLUMN-RISKSCORECOLUMN ];
		lineOffset -= LINEHEIGHT;
	}
	
	if (startyPostion+lineOffset < endyPostion)
		endyPostion = startyPostion+lineOffset;
    lineOffset = 0;

	
	
	
	NSString* riskScoreConsequenceLabel;
	int riskScoreConsequence = [[hazard RiskScoreConsequence]  intValue];
	switch (riskScoreConsequence) 
	{
		case 4:
			riskScoreConsequenceLabel = @"Insignificant";
			break;
		case 0:
			riskScoreConsequenceLabel = @"Minor";
			break;
		case 1:
			riskScoreConsequenceLabel = @"Medium";
			break;
		case 2:
			riskScoreConsequenceLabel = @"High";
			break;
		case 3:
			riskScoreConsequenceLabel = @"Extreme";
			break;	
		default:
			riskScoreConsequenceLabel = @"";
			break;
	}
	lineOffset -=  [self DrawNSStringLeftAlignWrap:context xPos:CONSEQCOLUMN-(CONSEQCOLUMN-RISKSCORECOLUMN)+5 yPos:startyPostion str:riskScoreConsequenceLabel textBold:NO Width:50];
    
    if (startyPostion+lineOffset < endyPostion)
		endyPostion = startyPostion+lineOffset;

	
	NSString* riskScoreLikelihoodLabel;
	int riskScoreLikelihood = [[hazard RiskScoreLikelihood]  intValue];
	switch (riskScoreLikelihood) 
	{
		case 4:
			riskScoreLikelihoodLabel = @"Rare";
			break;
		case 3:
			riskScoreLikelihoodLabel = @"Unlikely";
			break;
		case 2:
			riskScoreLikelihoodLabel = @"Possible";
			break;
		case 1:
			riskScoreLikelihoodLabel = @"Likely";
			break;
		case 0:
			riskScoreLikelihoodLabel = @"Almost Certain";
			break;	
		default:
			riskScoreLikelihoodLabel = @"";
			break;
	}
    [self DrawNSStringLeftAlign:context xPos:RISKSCOREMIDDLECOLUMN-(RISKSCOREMIDDLECOLUMN-CONSEQCOLUMN)+5 yPos:startyPostion str:riskScoreLikelihoodLabel textBold:NO fontsizing:FONTSIZE];

	
	
	
	lineOffset = 0;
	for (Control* control in hazard.ExistingControls)
	{
		[stringWrap setString:@""];
		NSString* effectivenessLabel;
		int effectiveness = [[control Effectiveness]  intValue];
		switch (effectiveness) 
		{
			case 0:
				effectivenessLabel = @"Inadequate";
				break;	
			case 1:
				
				effectivenessLabel = @"Poor";
				break;		
			case 2:
				
				effectivenessLabel = @"Moderate";
				break;	
			case 3:
				effectivenessLabel = @"Good";
				break;	
			case 4:
				
				effectivenessLabel = @"Excellent";
				break;
				
			default:
				break;
		}
		
		
		
		
		
		[stringWrap appendFormat:@"%@ - %@.\n",[control valueForKey:@"Effectiveness"],[[control valueForKey:@"Description"] description] ];
		lineOffset -=  [self DrawNSStringLeftAlignWrap:context xPos:EXISTINGCONTROLSCOLUMN-(EXISTINGCONTROLSCOLUMN-RISKSCOREMIDDLECOLUMN)+5 yPos:startyPostion+lineOffset str:stringWrap textBold:NO Width:EXISTINGCONTROLSCOLUMN-RISKSCOREMIDDLECOLUMN-30]+5;
			lineOffset -= LINEHEIGHT;
		
		
		
		
	}
	
	
	
	
	
	
	//[self DrawNSStringCentered:context xPos:RISKSCORECOLUMN-(RISKSCORECOLUMN-RISKSCOREMIDDLECOLUMN)/2 yPos:startyPostion str:riskScoreLikelihoodLabel textBold:NO];

	
	
	if (startyPostion+lineOffset < endyPostion)
		endyPostion = startyPostion+lineOffset;
	
	
		
	
	lineOffset = 0;
	int iCount = 97;
	for (Control* control in hazard.RecommendedControls)
	{
		[stringWrap setString:@""];
		[stringWrap appendFormat:@"%c. %@\n",iCount,[[control valueForKey:@"Description"] description]];
		iCount++;
		lineOffset -=  [self DrawNSStringLeftAlignWrap:context xPos:PROPOSEDCONTROLSCOLUMN-(PROPOSEDCONTROLSCOLUMN-RISKRATINGCOLUMN)+5 yPos:startyPostion+lineOffset str:stringWrap textBold:NO Width:PROPOSEDCONTROLSCOLUMN-RISKRATINGCOLUMN-30]+5;
        	lineOffset -= LINEHEIGHT;
		
	}
	
	if (startyPostion+lineOffset < endyPostion)
		endyPostion = startyPostion+lineOffset;
	
	
	endyPostion -= LINEHEIGHT;
	
	CGContextMoveToPoint(context,LEFTMARGIN,20-topOfRow);
	CGContextAddLineToPoint(context,LEFTMARGIN,20-endyPostion);
	CGContextStrokePath(context);
	
	CGContextMoveToPoint(context,RIGHTMARGIN,20-topOfRow);
	CGContextAddLineToPoint(context,RIGHTMARGIN,20-endyPostion);
	CGContextStrokePath(context);
	
	CGContextMoveToPoint(context,HAZARDCOLUMN,20-topOfRow);
	CGContextAddLineToPoint(context,HAZARDCOLUMN,20-endyPostion);
	CGContextStrokePath(context);

	CGContextMoveToPoint(context,PHOTOCOLUMN,20-topOfRow);
	CGContextAddLineToPoint(context,PHOTOCOLUMN,20-endyPostion);
	CGContextStrokePath(context);
	
	CGContextMoveToPoint(context,RISKCOLUMN,20-topOfRow);
	CGContextAddLineToPoint(context,RISKCOLUMN,20-endyPostion);
	CGContextStrokePath(context);
	
	CGContextMoveToPoint(context,RISKSCOREMIDDLECOLUMN,20-topOfRow);
	CGContextAddLineToPoint(context,RISKSCOREMIDDLECOLUMN,20-endyPostion);
	CGContextStrokePath(context);
	
	CGContextMoveToPoint(context,RISKSCORECOLUMN,20-topOfRow);
	CGContextAddLineToPoint(context,RISKSCORECOLUMN,20-endyPostion);
	CGContextStrokePath(context);
    
    
	CGContextMoveToPoint(context,CONSEQCOLUMN,20-topOfRow);
	CGContextAddLineToPoint(context,CONSEQCOLUMN,20-endyPostion);
	CGContextStrokePath(context);
	
	CGContextMoveToPoint(context,EXISTINGCONTROLSCOLUMN,20-topOfRow);
	CGContextAddLineToPoint(context,EXISTINGCONTROLSCOLUMN,20-endyPostion);
	CGContextStrokePath(context);
	
	CGContextMoveToPoint(context,RISKRATINGCOLUMN,20-topOfRow);
	CGContextAddLineToPoint(context,RISKRATINGCOLUMN,20-endyPostion);
	CGContextStrokePath(context);
	
	CGContextMoveToPoint(context,PROPOSEDCONTROLSCOLUMN,20-topOfRow);
	CGContextAddLineToPoint(context,PROPOSEDCONTROLSCOLUMN,20-endyPostion);
	CGContextStrokePath(context);
	
	
	NSString *riskScore = [[hazard valueForKey:@"RiskScore"] description];
	if ([riskScore hasPrefix:@"L"] == YES)
		[self drawRectGreen:context drawrect:CGRectMake(RISKRATINGCOLUMN-(RISKRATINGCOLUMN-EXISTINGCONTROLSCOLUMN),20-topOfRow+1, (RISKRATINGCOLUMN-EXISTINGCONTROLSCOLUMN)  , 0-endyPostion+topOfRow-1)];
	else if ([riskScore hasPrefix:@"M"] == YES)
		[self drawRectYellow:context drawrect:CGRectMake(RISKRATINGCOLUMN-(RISKRATINGCOLUMN-EXISTINGCONTROLSCOLUMN),20-topOfRow+1, (RISKRATINGCOLUMN-EXISTINGCONTROLSCOLUMN)  , 0-endyPostion+topOfRow-1)];
	else if ([riskScore hasPrefix:@"H"] == YES)
		[self drawRectOrange:context drawrect:CGRectMake(RISKRATINGCOLUMN-(RISKRATINGCOLUMN-EXISTINGCONTROLSCOLUMN),20-topOfRow+1, (RISKRATINGCOLUMN-EXISTINGCONTROLSCOLUMN)  , 0-endyPostion+topOfRow-1)];

	else
		[self drawRectRed:context drawrect:CGRectMake(RISKRATINGCOLUMN-(RISKRATINGCOLUMN-EXISTINGCONTROLSCOLUMN),20-topOfRow, (RISKRATINGCOLUMN-EXISTINGCONTROLSCOLUMN)  , 0-endyPostion+topOfRow)];
	[self DrawNSStringCenteredBig:context xPos:RISKRATINGCOLUMN-(RISKRATINGCOLUMN-EXISTINGCONTROLSCOLUMN)/2  yPos:startyPostion str:riskScore textBold:YES];

	NSString *residualRiskScore = [[hazard valueForKey:@"ResidualRiskScore"] description];
	if ([residualRiskScore hasPrefix:@"L"] == YES)
		[self drawRectGreen:context drawrect:CGRectMake(RIGHTMARGIN-(RIGHTMARGIN-PROPOSEDCONTROLSCOLUMN),20-topOfRow+1, (RIGHTMARGIN-PROPOSEDCONTROLSCOLUMN)  , 0-endyPostion+topOfRow-1)];
	else if ([residualRiskScore hasPrefix:@"M"] == YES)
		[self drawRectYellow:context drawrect:CGRectMake(RIGHTMARGIN-(RIGHTMARGIN-PROPOSEDCONTROLSCOLUMN),20-topOfRow+1, (RIGHTMARGIN-PROPOSEDCONTROLSCOLUMN)  , 0-endyPostion+topOfRow-1)];
	else if ([residualRiskScore hasPrefix:@"H"] == YES)
		[self drawRectOrange:context drawrect:CGRectMake(RIGHTMARGIN-(RIGHTMARGIN-PROPOSEDCONTROLSCOLUMN),20-topOfRow+1, (RIGHTMARGIN-PROPOSEDCONTROLSCOLUMN)  , 0-endyPostion+topOfRow-1)];
	
	else
		[self drawRectRed:context drawrect:CGRectMake(RIGHTMARGIN-(RIGHTMARGIN-PROPOSEDCONTROLSCOLUMN),20-topOfRow+1, (RIGHTMARGIN-PROPOSEDCONTROLSCOLUMN)  , 0-endyPostion+topOfRow-1)];

	[self DrawNSStringCenteredBig:context xPos:RIGHTMARGIN-(RIGHTMARGIN-PROPOSEDCONTROLSCOLUMN)/2 yPos:startyPostion str:residualRiskScore textBold:YES];
		CGContextMoveToPoint(context,LEFTMARGIN,20-endyPostion);
	CGContextAddLineToPoint(context,RIGHTMARGIN,20-endyPostion);
	CGContextStrokePath(context);
	return endyPostion;
}
- (void)drawRectGrey:(CGContextRef) context drawrect:(CGRect)rect
{   
    CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.3); // black color, half transparent
	CGContextFillRect(context,rect); // a square at the bottom left-hand corner
	CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 1); // black color, half transparent

}
- (void)drawRectRed:(CGContextRef) context drawrect:(CGRect)rect
{   
    CGContextSetRGBFillColor(context, 1.0, 0.0, 0.0, 0.7); // black color, half transparent
	CGContextFillRect(context,rect); // a square at the bottom left-hand corner
	CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 1); // black color, half transparent
	
}
- (void)drawRectYellow:(CGContextRef) context drawrect:(CGRect)rect
{   
    CGContextSetRGBFillColor(context, 1.0, 0.95, 0.0, 0.7); // black color, half transparent
	CGContextFillRect(context,rect); // a square at the bottom left-hand corner
	CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 1); // black color, half transparent
	
}
- (void)drawRectOrange:(CGContextRef) context drawrect:(CGRect)rect
{   
    CGContextSetRGBFillColor(context, 1, 0.7, 0.0, 0.7); // black color, half transparent
	CGContextFillRect(context,rect); // a square at the bottom left-hand corner
	CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 1); // black color, half transparent
	
}
- (void)drawRectGreen:(CGContextRef) context drawrect:(CGRect)rect
{   
    CGContextSetRGBFillColor(context, 0.0, 1.0, 0.0, 0.7); // black color, half transparent
	CGContextFillRect(context,rect); // a square at the bottom left-hand corner
	CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 1); // black color, half transparent
	
}

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
    if ([MFMailComposeViewController canSendMail])
    {
		EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
	
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        [picker setSubject:@"Report"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *saveDirectory = [paths objectAtIndex:0];
        NSString *newFilePath = [saveDirectory stringByAppendingPathComponent:fileName];
        NSURL *url = [NSURL fileURLWithPath:newFilePath];
        NSData *pdfData = [NSData dataWithContentsOfURL:url];
        [picker addAttachmentData:pdfData mimeType:@"application/pdf" fileName:fileName];
        NSString* toAddress = [[localSite GovernmentArea] Address];
        if (toAddress != nil)
        {
            NSArray *toRecipients = [NSArray arrayWithObject:toAddress]; 
            [picker setToRecipients:toRecipients];
        }
        newFilePath = [saveDirectory stringByAppendingPathComponent:kmlfileName];
        url = [NSURL fileURLWithPath:newFilePath];
        pdfData = [NSData dataWithContentsOfURL:url];
        
        [picker addAttachmentData:pdfData mimeType:@"text/xml" fileName:kmlfileName];
        
        for (NSManagedObject* managedObject in assessment.Hazzards)
        {
            if ([managedObject isKindOfClass:[Hazard class]]) //It is a risk object so update data
            {
                Hazard* hazard = (Hazard*) managedObject;
                for (Photo* photoItem in hazard.Photos)
                {
                    NSString *photoImage = [NSString stringWithFormat:@"small_%@",[photoItem Name]];
                    
                   
                    NSString* photoImageFile = [[delegate applicationDocumentsDirectory] stringByAppendingPathComponent:photoImage];
                     NSData *myData = [NSData dataWithContentsOfFile:photoImageFile];
                    [picker addAttachmentData:myData mimeType:@"image/png" fileName:photoImage];
            }
        }
        }
        
        [self presentModalViewController:picker animated:YES];
 
    }
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	NSString *title;
	NSString *alertMessage;
	NSString *yes;
	title = @"Email";
	yes = @"OK";
	
	// open an alert with just an OK button
	
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			alertMessage = @"Email Dispatch canceled";
			break;
		case MFMailComposeResultSaved:
			alertMessage = @"Email Saved";
			break;
		case MFMailComposeResultSent:
			alertMessage = @"Email Sent";
			break;
		case MFMailComposeResultFailed:
			alertMessage = @"Email failed to send";
			break;
		default:
			alertMessage = @"Email was not sent";
			break;
	}
	
	[self dismissModalViewControllerAnimated:YES];
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    [self.navigationController popToViewController:delegate.areaViewController animated:YES];
    return;

	
}

//based on the x and y make the text centered

- (void)DrawStringCentered:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(const char *) str textBold:(bool)textBold  fontsizing:(int)fontSizing

{
    if (str == nil)
        return;
	CGPoint pt;
	if (textBold == YES)
		CGContextSelectFont (gc, "Arial Bold", fontSizing, kCGEncodingMacRoman);
	else
		CGContextSelectFont (gc, "Arial", fontSizing, kCGEncodingMacRoman);
	CGContextSetTextDrawingMode(gc, kCGTextInvisible);
	CGContextShowTextAtPoint(gc, 0, 0, str, strlen(str));
	pt = CGContextGetTextPosition(gc);
	CGContextSetTextDrawingMode (gc, kCGTextFill);
	CGContextShowTextAtPoint(gc, xPos - pt.x / 2, yPos, str, strlen(str));
}
- (void)DrawNSStringCentered:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString*) str textBold:(bool)textBold

{
    if (str == nil)
        return;
	
	CGPoint pt;
	NSString* fontName;
	if (textBold == YES)
		fontName = @"Arial-BoldMT";
	else
		fontName = @"Arial";
	CGContextSaveGState(gc);
	CGContextSetTextDrawingMode(gc, kCGTextInvisible);
	[str drawInRect:CGRectMake(0, 0, 300, 50) withFont:[UIFont fontWithName:fontName size:FONTSIZE]];
	pt = CGContextGetTextPosition(gc);
	CGContextRestoreGState(gc);
	CGContextSetTextDrawingMode (gc, kCGTextFill);
	pt.x = xPos - pt.x / 2;
	pt.y = yPos;
	CGContextSetTextDrawingMode (gc, kCGTextFill);
	CGContextSetRGBFillColor(gc, 0, 0, 0, 1);
	
	[str drawInRect:CGRectMake(pt.x, 10-yPos, 300, 50) withFont:[UIFont fontWithName:fontName size:FONTSIZE]];
}
- (void)DrawNSStringCenteredBig:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString*) str textBold:(bool)textBold

{
    if (str == nil)
        return;
	
	CGPoint pt;
	NSString* fontName;
	if (textBold == YES)
		fontName = @"Arial-BoldMT";
	else
		fontName = @"Arial";
	CGContextSaveGState(gc);
	CGContextSetTextDrawingMode(gc, kCGTextInvisible);
	[str drawInRect:CGRectMake(0, 0, 300, 50) withFont:[UIFont fontWithName:fontName size:18]];
	pt = CGContextGetTextPosition(gc);
	CGContextRestoreGState(gc);
	CGContextSetTextDrawingMode (gc, kCGTextFill);
	pt.x = xPos - pt.x / 2;
	pt.y = yPos;
	CGContextSetTextDrawingMode (gc, kCGTextFill);
	CGContextSetRGBFillColor(gc, 0, 0, 0, 1);
	
	[str drawInRect:CGRectMake(pt.x, 10-yPos, 300, 50) withFont:[UIFont fontWithName:fontName size:18]];
}
- (void)DrawNSStringCenteredWhite:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString*) str textBold:(bool)textBold

{
    if (str == nil)
        return;
	
	CGPoint pt;
	NSString* fontName;
	if (textBold == YES)
		fontName = @"Arial-BoldMT";
	else
		fontName = @"Arial";
	CGContextSaveGState(gc);
	CGContextSetTextDrawingMode(gc, kCGTextInvisible);
	[str drawInRect:CGRectMake(0, 0, 300, 50) withFont:[UIFont fontWithName:fontName size:FONTSIZE]];
	pt = CGContextGetTextPosition(gc);
	CGContextRestoreGState(gc);
	CGContextSetTextDrawingMode (gc, kCGTextFill);
	pt.x = xPos - pt.x / 2;
	pt.y = yPos;
	CGContextSetTextDrawingMode (gc, kCGTextFill);
	CGContextSetRGBFillColor(gc, 1, 1, 1, 1);
	
	[str drawInRect:CGRectMake(pt.x, 10-yPos, 300, 50) withFont:[UIFont fontWithName:fontName size:FONTSIZE]];
}

//based on the x and y make the text left aligned

- (void)DrawStringLeftAlign:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(const char *) str textBold:(bool)textBold fontsizing:(int)fontSizing

{
    if (str == nil)
        return;
	
	if (textBold == YES)
		CGContextSelectFont (gc, "Arial Bold", fontSizing, kCGEncodingMacRoman);
	else
		CGContextSelectFont (gc, "Arial", fontSizing, kCGEncodingMacRoman);
	CGContextShowTextAtPoint(gc, xPos, yPos, str, strlen(str));
}
- (void)DrawNSStringLeftAlign:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString*) str textBold:(bool)textBold fontsizing:(int)fontSizing;

{
	NSString* fontName;
	if (textBold == YES)
		fontName = @"Arial-BoldMT";
	else
		fontName = @"Arial";
	
	
	[str drawInRect:CGRectMake(xPos, 10-yPos, 300, 50) withFont:[UIFont fontWithName:fontName size:fontSizing]];

}
- (int )DrawNSStringLeftAlignWrap:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString*) str textBold:(bool)textBold  Width:(int) width

{
    if (str == nil)
        return 0;
	
	NSString* fontName;
	if (textBold == YES)
		fontName = @"Arial-BoldMT";
	else
		fontName = @"Arial";
	CGPoint pt;
	CGContextSetRGBFillColor(gc, 0, 0, 0, 1);
	
	CGContextSaveGState(gc);
	CGContextSetTextDrawingMode(gc, kCGTextInvisible);
	pt = CGContextGetTextPosition(gc);
	[str drawInRect:CGRectMake(0, 0, width, 50) withFont:[UIFont fontWithName:fontName size:FONTSIZE]];
	pt = CGContextGetTextPosition(gc);
	CGContextRestoreGState(gc);
	CGContextSetTextDrawingMode (gc, kCGTextFill);
	pt.x = xPos-pt.x;
	[str drawInRect:CGRectMake(xPos, 10-yPos, width, 50) withFont:[UIFont fontWithName:fontName size:FONTSIZE]];
	return pt.y;
	
}
//based on the x and y make the text right aligned
- (void)DrawStringRightAlign:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(const char *) str textBold:(bool)textBold

{
    if (str == nil)
        return;
	
	CGPoint pt;
	if (textBold == YES)
		CGContextSelectFont (gc, "Arial-BoldMT", FONTSIZE, kCGEncodingMacRoman);
	else
		CGContextSelectFont (gc, "Arial", FONTSIZE, kCGEncodingMacRoman);
	CGContextSetTextDrawingMode(gc, kCGTextInvisible);
	CGContextShowTextAtPoint(gc, 0, 0, str, strlen(str));
	pt = CGContextGetTextPosition(gc);
	CGContextSetTextDrawingMode (gc, kCGTextFill);
	CGContextShowTextAtPoint(gc, xPos - pt.x, yPos, str, strlen(str));
}

//based on the x and y make the text right aligned
- (void)DrawNSStringRightAlign:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString* ) str textBold:(bool)textBold  Width:(int) width
 
{
    if (str == nil)
        return;
	
	NSString* fontName;
	if (textBold == YES)
		fontName = @"Arial Bold";
	else
		fontName = @"Arial";
	CGPoint pt;

	CGContextSetRGBFillColor(gc, 0, 0, 0, 1);
	
	CGContextSaveGState(gc);
	CGContextSetTextDrawingMode(gc, kCGTextInvisible);
	pt = CGContextGetTextPosition(gc);
	[str drawInRect:CGRectMake(0, 0, width, 50) withFont:[UIFont fontWithName:@"Arial" size:FONTSIZE]];
	pt = CGContextGetTextPosition(gc);
	CGContextRestoreGState(gc);
	CGContextSetTextDrawingMode (gc, kCGTextFill);
	pt.x = xPos-pt.x;
	[str drawInRect:CGRectMake(xPos, 10-yPos, width, 50) withFont:[UIFont fontWithName:fontName size:FONTSIZE]];
	return;
}
//based on the x and y make the text right aligned
- (void)DrawNSStringLeftAlignRect:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString* ) str textBold:(bool)textBold

{
    if (str == nil)
        return;
	
	if (textBold == YES)
		CGContextSelectFont (gc, "Arial Bold", FONTSIZE, kCGEncodingMacRoman);
	else
		CGContextSelectFont (gc, "Arial", FONTSIZE, kCGEncodingMacRoman);
	CGContextSetRGBFillColor(gc, 0, 0, 0, 1);
	
	[str drawInRect:CGRectMake(xPos, 10-yPos, 300, 50) withFont:[UIFont fontWithName:@"Arial" size:FONTSIZE]];
}
-(int)HeightForStringDrawing:(NSString *)myString FontSize:(int) fontSize RectWidth:(int) myWidth
{
	CGSize boundingSize = CGSizeMake(myWidth, CGFLOAT_MAX);
	CGSize requiredSize = [myString sizeWithFont:[UIFont systemFontOfSize:fontSize]
							   constrainedToSize:boundingSize
								   lineBreakMode:UILineBreakModeWordWrap];
	CGFloat requiredHeight = requiredSize.height;
	return requiredHeight;

}



@end
