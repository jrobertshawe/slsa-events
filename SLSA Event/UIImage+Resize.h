//
//  UIImage+Resize.h
//  SLSA Risk Assessment
//
//  Created by Julian robertshawe on 25/03/12.
//  Copyright (c) 2012 MISSING PIECE TECHNOLOGY. All rights reserved.
//

@interface UIImage (Resize)

+ (UIImage*)imageWithImage:(UIImage*)image 
              scaledToSize:(CGSize)newSize;

@end


