//
//  SingleEditCell.h
//  AussieIpad
//
//  Created by Julian robertshawe on 12/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectItemItem.h"
#import "SelectTime.h"
#import "SelectInterval.h"
#import "SelectFixedItemList.h"

@interface SingleEditCell : UITableViewCell
{
    IBOutlet UILabel *celllabelText;
    IBOutlet UILabel *cellDetailText;
    IBOutlet UILabel *cellbiglabelText;
    NSManagedObject*             entity ;
    NSString*               selectList;
      NSString*               entityLabel;
    NSString*               entityKey;
    NSString*               entityColumn;
    int                        selectListType;
    NSString*               notifyQueue;  
    NSString*               parentViewQueue; 
    NSString*               selectlistTitle;
    IBOutlet UIButton *updateButton;
    UIPopoverController *popoverController;
    UIViewController* parentController;

}
@property (nonatomic,retain)  IBOutlet UIViewController *parentController;
@property (nonatomic,retain)  IBOutlet UIButton *updateButton;
@property (nonatomic,retain)  UILabel						*cellbiglabelText;
@property (nonatomic,retain)  UILabel						*celllabelText;
@property (nonatomic,retain)  UILabel						*cellDetailText;
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString  entitySelectList:(NSString*) entitySelectList entitySelectListType:(int) entitySelectListType parentQueue:(NSString*) parentQueue;
-(IBAction)UpdateButtonPressed:(id)sender;
@end
