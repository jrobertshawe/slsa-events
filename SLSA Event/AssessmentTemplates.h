//
//  AssessmentTemplates.h
//  SLSARiskAssessment
//
//  Created by Julian on 14/12/10.
//  Copyright 2010 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

@interface AssessmentTemplates : UIViewController {
@private
	bool isEditing;	
	NSArray* itemArray;
	
	IBOutlet UITableView		*_tableView;
	NSMutableArray* selectedItems;
    NSManagedObject* object;
}

@property (nonatomic, retain) UITableView		*_tableView;
-(void)setupData:(NSManagedObject*) entityObject;

@end