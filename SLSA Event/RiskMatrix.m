//
//  RiskMatrix.m
//  SLSA Risk Assessment
//
//  Created by Julian on 10/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "RiskMatrix.h"
#import "EventAppDelegate.h"

@implementation RiskMatrix
@synthesize notifyQueue;
@synthesize entityKey;
@synthesize label;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
      [super viewDidLoad];
    NSString *navBar;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        navBar = @"ipad_blue-header.png";
    }
    else {
        navBar = @"iphone_blueheader.png";
    }
    
        UIImage *image = [UIImage imageNamed:navBar];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
   
    
    
    
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(CancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonImage = [UIImage imageNamed:@"but_done_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(DoneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
   // self.navigationItem.rightBarButtonItem = backButtonItem;
    
    
  
    lastButton = nil;
    
    
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
    
    
       // Do any additional setup after loading the view from its nib.
}
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString
{
    
    
    entity = entityObject;
    

    entityKey = entityKeyString;
    NSString* consequenceColum = [NSString stringWithFormat:@"%@Consequence",entityKey];
	NSString* likelihoodColum = [NSString stringWithFormat:@"%@Likelihood",entityKey];
    int row =  	 [[entity valueForKey:likelihoodColum] intValue];
     switch (row) {
        case 5:
            row = 0;
            break;
        case 4:
            row = 1;
            break;
        case 3:
            row = 2;
            break;
        case 2:
            row = 3;
            break;
        case 1:
            row = 4;
            break;
        default:
            break;
    }
	int column =   [[entity valueForKey:consequenceColum] intValue];
    if (column == 0)
        row++;
    
	int buttontag = row*5;
    buttontag += column;
    UIButton *button =  (UIButton*) [self.view viewWithTag:buttontag];
    if (button != nil)
    {
        button.alpha = 0;
        lastButton = button;
    }
  self.title = @"Risk Matrix";
}

-(IBAction)DoneButtonPressed
{
    NSString* riskValue =  @"Matrix Error";
	int row = lastButton.tag/5;
	int column = lastButton.tag%5;
	if (column == 0)
    {
		row = row -1;
        column = 5;
    }

    
  	switch (lastButton.tag) 
    {
        case 6:
            riskValue = @"L4";
            break;
        case 11:
            riskValue = @"L3";
            break;
        case 16:
            riskValue = @"L2";
            break;
        case 17:
            riskValue = @"L4";
            break;
        case 21:
            riskValue = @"L1";
            break;
        case 22:
            riskValue = @"L2";
            break;
        case 23:
            riskValue = @"L3";
            break;
        case 1:
            riskValue = @"M4";
            break;
        case 7:
            riskValue = @"M8";
            break;
        case 12:
            riskValue = @"M6";
            break;
        case 18:
            riskValue = @"M6";
            break;
        case 24:
            riskValue = @"M4";
            break;
        case 25:
            riskValue = @"M5";
            break;
            
        case 2:
            riskValue = @"H10";
            break;
        case 3:
            riskValue = @"H15";
            break;
        case 8:
            riskValue = @"H12";
            break;
        case 13:
            riskValue = @"H9";
            break;
        case 14:
            riskValue = @"H12";
            break;
        case 19:
            riskValue = @"H8";
            break;
        case 20:
            riskValue = @"H10";
            break;
            
        case 4:
            riskValue = @"E20";
            break;
        case 5:
            riskValue = @"E25";
            break;
        case 9:
            riskValue = @"E16";
            break;
        case 10:
            riskValue = @"E20";
            break;
        case 15:
            
            riskValue = @"E15";
            break;
            
        default:
            break;
    }

    
    
    
    
    
    
    
    int liklihood;
    switch (row) {
        case 0:
            liklihood = 5;
            break;
        case 1:
            liklihood = 4;
            break;
        case 2:
            liklihood = 3;
            break;
        case 3:
            liklihood = 2;
            break;
        case 4:
            liklihood = 1;
            break;
        default:
            break;
    }
    
    NSString* consequenceColum = [NSString stringWithFormat:@"%@Consequence",entityKey];
	NSString* likelihoodColum = [NSString stringWithFormat:@"%@Likelihood",entityKey];

	[entity setValue:[NSString stringWithFormat:@"%d",column] forKey:consequenceColum];
    
    [entity setValue:riskValue forKey:entityKey];
    
    [entity setValue:[NSString stringWithFormat:@"%d",liklihood] forKey:likelihoodColum];
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];
}
-(IBAction)CancelButtonPressed
{
    [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(IBAction)MatrixButton:(id)sender
{
	UIButton *button = (UIButton*)sender;
    button.alpha = 0;
    if (lastButton != nil)
        lastButton.alpha = .1;

	
    lastButton = button;
    [self DoneButtonPressed];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
