//
//  EventViewControllerHazardDetail.m
//  SLSA Event
//
//  Created by Julian robertshawe on 1/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerHazardDetail.h"
#define HAZARDNUMBEROFSECTION 7
#define SECTIONHAZARDDETAILS 0
#define SECTIONHAZARDRISKS 1
#define SECTIONHAZARDRISKGROUP 2
#define SECTIONHAZARDRISKSCORE 3
#define SECTIONHAZARDEXISTINGCONTROLS 4
#define SECTIONHAZARDRECOMMENDEDCONTROLS 5
#define SECTIONHAZARDPHOTOS 6

#define NAME 0
#define HAZARDLOCATION  1
#define SECTIONHAZARDRISKSCORECURRENT 30
#define SECTIONHAZARDRISKSCORERESIDUAL 31
@interface EventViewControllerHazardDetail ()

@end

@implementation EventViewControllerHazardDetail

@synthesize _tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)setupData:(NSManagedObject*) entityObject  assessment:(Assessment*) assessmentObject
{
    if ([entityObject isKindOfClass:[NSString class]]) //It is a risk object so update data
    {
        EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
        
        hazardItem = (Hazard *)[NSEntityDescription insertNewObjectForEntityForName:@"Hazard" inManagedObjectContext:[delegate managedObjectContext]];
        
    }
    else
    {
        hazardItem = entityObject;
    if (hazardItem != nil)
        hazardItem.status = @"Reviewed";
    }
    assessment = assessmentObject;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HazardsViewController" object:nil userInfo:nil];
    
    
	
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */
- (void)DonePressed
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self._tableView setEditing:YES animated:YES];
    notifyQueue = @"HazardDetailViewController";
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:notifyQueue
     object:nil];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonImage = [UIImage imageNamed:@"gps-red.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    // [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.rightBarButtonItem = backButtonItem;
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotificationGPSOn:)
     name:@"GPSOn"
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotificationGPSOff:)
     name:@"GPSOff"
     object:nil];
    self.title = @"Hazard Details";
    GPSon = NO;
    
}
-(void)handleNotificationGPSOff:(NSNotification *)pNotification
{
    if (GPSon == NO)
        return;
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"gps-red.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    // [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
   UIBarButtonItem*  backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.rightBarButtonItem = backButtonItem;
    GPSon = NO;
}
-(void)handleNotificationGPSOn:(NSNotification *)pNotification
{
    if (GPSon == YES)
        return;
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"gps_on.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    // [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
   UIBarButtonItem*  backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.rightBarButtonItem = backButtonItem;
    GPSon = YES;
}
- (void) didTapBackButton:(id)sender {
    
    if ([[assessment.Hazzards allObjects] containsObject:hazardItem] == NO)
        [assessment addHazzardsObject:hazardItem];
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    [delegate saveContext];
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)handleNotification:(NSNotification *)pNotification
{
    if([popoverController isPopoverVisible])
    {
        //close the popover view
        [popoverController dismissPopoverAnimated:YES];
        
    }
    [self dismissModalViewControllerAnimated:YES];
    [self._tableView reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}



#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav3.png",delegate.imagePrefix];
    
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    
    [hv addSubview:image];
    //  hv.backgroundColor = [UIColor whiteColor];
    tableView.tableHeaderView = hv;
    return HAZARDNUMBEROFSECTION;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)] ;
	tableView.sectionHeaderHeight = headerView.frame.size.height;
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, headerView.frame.size.width - 20, 22)] ;
	label.text = [self tableView:tableView titleForHeaderInSection:section];
	label.font = [UIFont boldSystemFontOfSize:16.0];
	label.shadowOffset = CGSizeMake(0, 1);
	label.shadowColor = [UIColor blackColor];
	label.backgroundColor = [UIColor clearColor];
    
	label.textColor = [UIColor whiteColor];
    
	[headerView addSubview:label];
	return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
    if (section == SECTIONHAZARDPHOTOS)
    {
        NSObject* item = [photos objectAtIndex:row];
        if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
            return 69;
        else
            return 200.0;
    }
    
    if (section == SECTIONHAZARDRISKSCORE)
        return 69;
    if (section == SECTIONHAZARDRISKSCORE)
        return 69;
    switch (sectionRow)
    {
        case HAZARDLOCATION:
            return 112.0;
            break;
        default:
            return 69;
            break;
    }
    
    
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *titleString =@"";
	NSSortDescriptor * descriptor;
	switch ( section)
    {
        case SECTIONHAZARDDETAILS:
            titleString =  @"Details";
            break;
        case SECTIONHAZARDRISKSCORE:
            titleString =  @"Risk Score";
            break;
        case SECTIONHAZARDRISKS:
            titleString =  @"Risks";
            if (risks == nil)
                risks = [[NSMutableArray alloc] init];
            else
                [risks removeAllObjects];
            
            [risks addObjectsFromArray:[hazardItem.Risks allObjects]];
            [ risks insertObject:@"Add Risk"  atIndex:0];   //add temp line to allow adding items
            break;
        case SECTIONHAZARDRISKGROUP:
            titleString =  @"Risk Groups";
            if (riskgroups == nil)
                riskgroups = [[NSMutableArray alloc] init];
            else
                [riskgroups removeAllObjects];
            
            [riskgroups addObjectsFromArray:[hazardItem.RiskGroups allObjects]];
            [ riskgroups insertObject:@"Add Groups"  atIndex:0];    //add temp line to allow adding items
            break;
        case SECTIONHAZARDEXISTINGCONTROLS:
            titleString =  @"Existing Controls";
            if (existingControls == nil)
                existingControls = [[NSMutableArray alloc] init];
            else
                [existingControls removeAllObjects];
            
            descriptor = [[NSSortDescriptor alloc] initWithKey:@"display_order" ascending:NO] ;
            [existingControls addObjectsFromArray:[[hazardItem valueForKey:@"ExistingControls"] sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]]];
            
            
            
            [ existingControls insertObject:@"Add Control"  atIndex:0];    //add temp line to allow adding items
            break;
        case SECTIONHAZARDRECOMMENDEDCONTROLS:
            titleString =  @"Recommended Controls";
            if (recommendControls == nil)
                recommendControls = [[NSMutableArray alloc] init];
            else
                [recommendControls removeAllObjects];
            
            descriptor = [[NSSortDescriptor alloc] initWithKey:@"display_order" ascending:NO] ;
            [recommendControls addObjectsFromArray:[[hazardItem valueForKey:@"RecommendedControls"] sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]]];
            
            
            
            [ recommendControls insertObject:@"Add Control"  atIndex:0];   //add temp line to allow adding items
            break;
        case SECTIONHAZARDPHOTOS:
            titleString =  @"Photos";
            if (photos == nil)
                photos = [[NSMutableArray alloc] init];
            else
                [photos removeAllObjects];
            descriptor = [[NSSortDescriptor alloc] initWithKey:@"display_order" ascending:NO] ;
            [photos addObjectsFromArray:[[hazardItem valueForKey:@"Photos"] sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]]];
            
            
            
            [ photos insertObject:@"Add Photo"  atIndex:0];    //add temp line to allow adding items
            break;
            
    }
    return titleString;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int sectionRows = 0;
    switch ( section)
    {
        case SECTIONHAZARDDETAILS:
            sectionRows =  2;
            break;
        case SECTIONHAZARDRISKSCORE:
            sectionRows =  2;
            break;
        case SECTIONHAZARDRISKS:
            sectionRows =  [risks count];
            break;
        case SECTIONHAZARDRISKGROUP:
            sectionRows =  [riskgroups count];
            break;
        case SECTIONHAZARDEXISTINGCONTROLS:
            sectionRows =  [existingControls count];
            break;
        case SECTIONHAZARDRECOMMENDEDCONTROLS:
            sectionRows =  [recommendControls count];
            
            break;
        case SECTIONHAZARDPHOTOS:
            sectionRows =  [photos count];
            break;
    }
    
    return sectionRows;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
    
    UITableViewCell * tableCell = nil;
	NSString* Identifier;
	// Configure the cell.
    NSString* IdentifierLandScape;
    IdentifierLandScape= @"";
    static NSString *CellIdentifier = @"SingleEditCell";
    static NSString *SingleLineListItemCellIdentifier = @"SingleLineListItemCell";
	
    
    if (sectionRow == NAME)
    {
        SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                         reuseIdentifier:CellIdentifier];
            
        }
        
        [cell setupData:hazardItem entityKeyString:@"Description" entityLabelString:@"Hazard Type" entitySelectList:@"HazardTemplate"  entitySelectListType:1 parentQueue:@"HazardsViewController"];
        tableCell = cell;
        cell.parentController = self;
        cell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
        cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
    }
    else if (sectionRow == HAZARDLOCATION)
    {
        
        
        LocationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LocationCell"];
        if (cell == nil) {
            cell = [[LocationCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                       reuseIdentifier:@"LocationCell"];
            
        }
        
        [cell setupData:hazardItem];
        tableCell = cell;
            cell.accessoryType =    UITableViewCellAccessoryNone;
         cell.editingAccessoryType = UITableViewCellAccessoryNone;
    }
    else if (section == SECTIONHAZARDRISKS)
    {
        
        
        SingleLineListItemCell *cell = [tableView dequeueReusableCellWithIdentifier:SingleLineListItemCellIdentifier];
        if (cell == nil) {
            cell = [[SingleLineListItemCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                 reuseIdentifier:SingleLineListItemCellIdentifier];
            
        }
        NSManagedObject* item = [risks objectAtIndex:row];
        [cell setupData:item];
        tableCell = cell;
         cell.accessoryType =    UITableViewCellAccessoryNone;
            cell.editingAccessoryType = UITableViewCellAccessoryNone;
        
    }
    else if (section == SECTIONHAZARDRISKGROUP)
    {
        SingleLineListItemCell *cell = [tableView dequeueReusableCellWithIdentifier:SingleLineListItemCellIdentifier];
        if (cell == nil) {
            cell = [[SingleLineListItemCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                 reuseIdentifier:SingleLineListItemCellIdentifier];
            
        }
        NSManagedObject* item = [riskgroups objectAtIndex:row];
        [cell setupData:item];
        tableCell = cell;
         cell.accessoryType =    UITableViewCellAccessoryNone;
    }
    else if (sectionRow == SECTIONHAZARDRISKSCORECURRENT)
    {
        Identifier = [NSString stringWithFormat:@"%@%@", @"SingleImageCellMatrix",IdentifierLandScape];
        
        SingleImageCell *cell = (SingleImageCell *)[tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[SingleImageCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:SingleLineListItemCellIdentifier];
        }
        
        cell.parentController = self;
        cell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
        cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [cell setupData:hazardItem entityKeyString:@"RiskScore" entityLabelString:@"Current Risk Score" cameraImage:NO riskImage:YES  imageList:NO];
        tableCell = cell;
    }
    else if (sectionRow == SECTIONHAZARDRISKSCORERESIDUAL)
    {
        Identifier = [NSString stringWithFormat:@"%@%@", @"SingleImageCellMatrix",IdentifierLandScape];
        
        SingleImageCell *cell = (SingleImageCell *)[tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[SingleImageCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:SingleLineListItemCellIdentifier];
        }
        cell.parentController = self;
        cell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
        cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [cell setupData:hazardItem entityKeyString:@"ResidualRiskScore" entityLabelString:@"Residual Risk Score" cameraImage:YES riskImage:YES  imageList:NO];
        tableCell = cell;
    }
    else if (section == SECTIONHAZARDEXISTINGCONTROLS)
    {
        
        Identifier = [NSString stringWithFormat:@"%@%@", @"SingleLineListControlCell",IdentifierLandScape];
        
        
        SingleLineListControlCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[SingleLineListControlCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                    reuseIdentifier:Identifier];
            
        }
        cell.parentController = self;
        
        
        
        NSManagedObject* item = [existingControls objectAtIndex:row];
        
           
        [cell setupData:item entityKeyString:@"Effectiveness" entitySelectListString:@"EffectivenessTemplate" parentQueue:@"HazardViewController"];
        if ([item isKindOfClass:[NSString class]] == NO) //It is a sting object row must be add mode
        {
            cell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
            cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        else
            cell.accessoryType =    UITableViewCellAccessoryNone;
        tableCell = cell;
    }
    else if (section == SECTIONHAZARDRECOMMENDEDCONTROLS)
    {
        
        SingleLineListItemCell *cell = [tableView dequeueReusableCellWithIdentifier:SingleLineListItemCellIdentifier];
        if (cell == nil) {
            cell = [[SingleLineListItemCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                 reuseIdentifier:SingleLineListItemCellIdentifier];
            
        }
        NSManagedObject* item = [recommendControls objectAtIndex:row];
        [cell setupData:item];
        tableCell = cell;
         cell.accessoryType =    UITableViewCellAccessoryNone;
        
        
    }
    else if (section == SECTIONHAZARDPHOTOS)
    {
        
        Identifier = [NSString stringWithFormat:@"%@%@", @"SingleImageCell",IdentifierLandScape];
        
        SingleImageCell *cell = (SingleImageCell *)[tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[SingleImageCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:Identifier];
        }
        
        
        NSManagedObject* item = [photos objectAtIndex:row];
        
        [cell setupData:item entityKeyString:@"name" entityLabelString:@"Add Hazard Photo" cameraImage:YES riskImage:NO  imageList:YES];
        UIButton* button = (UIButton*) [cell viewWithTag:11];
        button.hidden = YES;
        tableCell = cell;
         cell.accessoryType =    UITableViewCellAccessoryNone;
            cell.editingAccessoryType = UITableViewCellAccessoryNone;
    }
   tableCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return tableCell;
}
- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	NSUInteger section = [indexPath section];
    if (section == SECTIONHAZARDRISKS || section == SECTIONHAZARDRISKGROUP || section == SECTIONHAZARDEXISTINGCONTROLS   || section == SECTIONHAZARDRECOMMENDEDCONTROLS || section == SECTIONHAZARDPHOTOS)
		return YES;
	
	return NO;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath   ///what edit mode are we entering
{
    if (self._tableView.editing == YES)
	{
        NSUInteger row = [indexPath row];
		NSUInteger section = [indexPath section];
        NSManagedObject *item;
		if (section == SECTIONHAZARDRISKS || section == SECTIONHAZARDRISKGROUP || section == SECTIONHAZARDEXISTINGCONTROLS || section == SECTIONHAZARDRECOMMENDEDCONTROLS || section == SECTIONHAZARDPHOTOS)
		{
            if (section == SECTIONHAZARDRISKS)
                item = [risks objectAtIndex:row];
            
            if (section == SECTIONHAZARDRISKGROUP)
                item = [riskgroups objectAtIndex:row];
            if (section == SECTIONHAZARDEXISTINGCONTROLS)
                item = [existingControls objectAtIndex:row];
            if (section == SECTIONHAZARDRECOMMENDEDCONTROLS)
                item = [recommendControls objectAtIndex:row];
            if (section == SECTIONHAZARDPHOTOS)
                item = [photos objectAtIndex:row];
            if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
			{
				return UITableViewCellEditingStyleInsert;
				
			}
			else //else show delete option
			{
				return UITableViewCellEditingStyleDelete;
			}
			
		}
        
    }
    return UITableViewCellEditingStyleNone;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    NSUInteger section = [indexPath section];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (section == SECTIONHAZARDRISKS )
    {
     	NSManagedObject *item = [risks objectAtIndex:row];
        if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
        {
            
            [self AddPopover:@"RiskTemplate" entityKey:@"Risks" forRowAtIndexPath:indexPath selectListType:SELECTLISTTEMPLATE title:@"Risk"];
            
            
        }
    }
        if (section == SECTIONHAZARDRISKGROUP )
        {
            NSManagedObject *item = [riskgroups objectAtIndex:row];
            if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
            {
                
                [self AddPopover:@"RiskGroupTemplate" entityKey:@"RiskGroups" forRowAtIndexPath:indexPath selectListType:SELECTLISTTEMPLATE title:@"Risk Group"];
                
                
            }
        }
        if (section == SECTIONHAZARDEXISTINGCONTROLS )
        {
            NSManagedObject *item = [existingControls objectAtIndex:row];
            if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
            {
                
                [self AddPopover:@"ControlTemplate" entityKey:@"ExistingControls" forRowAtIndexPath:indexPath selectListType:SELECTLISTTEMPLATE title:@"Existing Control"];
                
                
            }
            else //else show delete option
            {
                             SelectFixedItemList* popoverContent = [[SelectFixedItemList alloc] initWithNibName:@"SelectFixedItemList" bundle:nil];
                popoverContent.label = @"Effectiveness";
                [popoverContent setupData:item entityKeyString:@"Effectiveness" entityselectListType:0 entitySelectListString:@"EffectivenessTemplate" parentQueue:notifyQueue ];
                //resize the popover view shown
                //in the current view to the view's size
                popoverContent.contentSizeForViewInPopover =
                CGSizeMake(500, 500);
                UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
                toolsNavController.view.frame = CGRectMake(0.0, -10.0, 320.0, 100);
                toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentModalViewController:toolsNavController animated:YES];

            }
        }
        if (section == SECTIONHAZARDRECOMMENDEDCONTROLS )
        {
            NSManagedObject *item = [recommendControls objectAtIndex:row];
            if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
            {
                
                [self AddPopover:@"ControlTemplate" entityKey:@"RecommendedControls" forRowAtIndexPath:indexPath selectListType:SELECTLISTTEMPLATE title:@"Recommended Control"];
                
                
            }
        
        }
        if (section == SECTIONHAZARDPHOTOS )
        {
            NSManagedObject *item = [photos objectAtIndex:row];
            if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
            {
                
                ChooseImage *popoverContent = [[ChooseImage alloc] initWithNibName:@"ChooseImage" bundle:nil];
                
                
                
                //resize the popover view shown
                //in the current view to the view's size
                popoverContent.contentSizeForViewInPopover =
                CGSizeMake(380, 544);
                
                //create a popover controller
                [popoverContent setupData:hazardItem entityKeyString: @"Photos"  queue:notifyQueue];
                
                popoverContent.label = @"Add Photo";
                //present the popover view non-modal with a
                //refrence to the toolbar button which was pressed
                
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
                navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
                //     navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.08627450980392 green:0.15686274509804 blue:0.20392156862745 alpha:1];
                
                //     navigationController.view.frame = CGRectMake(0.0, -10.0, 300, 504);
                navigationController.title = @"Select Photo";
                //release the popover content
                //[popoverContent release];
                [self presentModalViewController:navigationController animated:YES];
                //release the popover content
                
                
                
                //release the popover content
                
                
                
                
            }
       
        }



}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSUInteger row = [indexPath row];
    NSUInteger section = [indexPath section];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (section == SECTIONHAZARDRISKS )
    {
     	NSManagedObject *item = [risks objectAtIndex:row];
        if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
        {
            
            [self AddPopover:@"RiskTemplate" entityKey:@"Risks" forRowAtIndexPath:indexPath selectListType:SELECTLISTTEMPLATE title:@"Risk"];
            
            
        }
        else //else show delete option
        {
            [hazardItem removeRisksObject:item];
            
            [tableView reloadData];
            ;
        }
    }
    else
        if (section == SECTIONHAZARDRISKGROUP )
        {
            NSManagedObject *item = [riskgroups objectAtIndex:row];
            if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
            {
                
                [self AddPopover:@"RiskGroupTemplate" entityKey:@"RiskGroups" forRowAtIndexPath:indexPath selectListType:SELECTLISTTEMPLATE title:@"Risk Group"];
                
                
            }
            else //else show delete option
            {
                [hazardItem removeRiskGroupsObject:item];
                
                [tableView reloadData];
                ;
            }
        }
        else
            if (section == SECTIONHAZARDEXISTINGCONTROLS )
            {
                NSManagedObject *item = [existingControls objectAtIndex:row];
                if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
                {
                    
                    [self AddPopover:@"ControlTemplate" entityKey:@"ExistingControls" forRowAtIndexPath:indexPath selectListType:SELECTLISTTEMPLATE title:@"Existing Control"];
                    
                    
                }
                else //else show delete option
                {
                    [hazardItem removeExistingControlsObject:item];
                    
                    [tableView reloadData];
                    ;
                }
            }
    if (section == SECTIONHAZARDRECOMMENDEDCONTROLS )
    {
        NSManagedObject *item = [recommendControls objectAtIndex:row];
        if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
        {
            
            [self AddPopover:@"ControlTemplate" entityKey:@"RecommendedControls" forRowAtIndexPath:indexPath selectListType:SELECTLISTTEMPLATE title:@"Recommended Control"];
            
            
        }
        else //else show delete option
        {
            [hazardItem removeRecommendedControlsObject:item];
            
            [tableView reloadData];
            ;
        }
    }
    if (section == SECTIONHAZARDPHOTOS )
    {
        NSManagedObject *item = [photos objectAtIndex:row];
        if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
        {
            
            ChooseImage *popoverContent = [[ChooseImage alloc] initWithNibName:@"ChooseImage" bundle:nil];

            
            
            //resize the popover view shown
            //in the current view to the view's size
            popoverContent.contentSizeForViewInPopover =
            CGSizeMake(380, 544);
            
            //create a popover controller
           [popoverContent setupData:hazardItem entityKeyString: @"Photos"  queue:notifyQueue];
            
            popoverContent.label = @"Add Photo";
            //present the popover view non-modal with a
            //refrence to the toolbar button which was pressed
            
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
            navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
       //     navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.08627450980392 green:0.15686274509804 blue:0.20392156862745 alpha:1];
            
       //     navigationController.view.frame = CGRectMake(0.0, -10.0, 300, 504);
            navigationController.title = @"Select Photo";
            //release the popover content
            //[popoverContent release];
            [self presentModalViewController:navigationController animated:YES];
            //release the popover content
            
            
            
            //release the popover content
            
            
            
            
        }
        else //else show delete option
        {
            [hazardItem removePhotosObject:item];
            
            [self._tableView reloadData];
            ;
        }
    }
    
}
-(void)AddPopover:(NSString*)listName entityKey:(NSString*) entityKey forRowAtIndexPath:(NSIndexPath *)indexPath selectListType:(int)selectListType  title:(NSString*) title

{
    
    UITableViewCell *cell = [self._tableView cellForRowAtIndexPath:indexPath];
    
    if([popoverController isPopoverVisible])
    {
        //close the popover view if toolbar button was touched
        //again and popover is already visible
        
        [popoverController dismissPopoverAnimated:YES];
        return;
    }
    SelectItemItem* popoverContent = [[SelectItemItem alloc]
                                      init];
    popoverContent.parentController  = self;
    [popoverContent setupData:hazardItem entityKeyString:entityKey entityselectListType:SELECTLISTTEMPLATE entitySelectList:listName parentQueue:notifyQueue];
    popoverContent.label = title;
    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.contentSizeForViewInPopover =
    CGSizeMake(500, 500);
    
    UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent] ;
    toolsNavController.view.frame = CGRectMake(0.0, -10.0, 520.0, 100);
    
    //create a popover controller
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
    navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.08627450980392 green:0.15686274509804 blue:0.20392156862745 alpha:1];
    
    navigationController.view.frame = CGRectMake(0.0, -10.0, 300, 504);
    //release the popover content
    //[popoverContent release];
    [self presentModalViewController:navigationController animated:YES];
    //release the popover content
    
    
    
    
}
@end
