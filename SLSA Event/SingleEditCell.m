//
//  SingleEditCell.m
//  AussieIpad
//
//  Created by Julian robertshawe on 12/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SingleEditCell.h"
#import "EditSingleLineText.h"
#import "EventAppDelegate.h"
#import "EnterValue.h"
@implementation SingleEditCell
@synthesize cellDetailText;
@synthesize celllabelText;
@synthesize updateButton;
@synthesize parentController;
@synthesize cellbiglabelText;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString  entitySelectList:(NSString*) entitySelectList entitySelectListType:(int) entitySelectListType parentQueue:(NSString*) parentQueue
{ 
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    selectListType = entitySelectListType;
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    notifyQueue =  [[NSString alloc] initWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
    
    if (parentQueue != nil)
    {
        parentViewQueue = [[NSString alloc] initWithString:parentQueue];
    }
     entity = entityObject;
    
    if (entityKeyString != nil)
    {
        entityKey= [[NSString alloc] initWithString:entityKeyString];
        if (entitySelectList != nil)
        {
            selectList= [[NSString alloc] initWithString:entitySelectList];
            selectlistTitle = [[NSString alloc] initWithString:entityLabelString];
        }
       
       cellDetailText.text = [entity valueForKey:entityKey];
        
    }
    celllabelText.text = entityLabelString;
    entityLabel = entityLabelString;
    if (cellbiglabelText != nil)
    {
        if (cellDetailText.text == nil || [cellDetailText.text length] == 0)
        {
            cellbiglabelText.text = entityLabelString;
            celllabelText.hidden = YES;
            if  ([delegate isFieldInError:entityKey] == YES)
                      cellbiglabelText.textColor = [UIColor redColor];
              
        }
        else
        {
             cellbiglabelText.hidden = YES;
             celllabelText.hidden = NO;
        }
    }
    else
    {
        cellbiglabelText.hidden = YES;
        celllabelText.hidden = NO;
    }
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:entityKey
     object:nil];  
    [updateButton addTarget:self action:@selector(UpdateButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)handleNotification:(NSNotification *)pNotification
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    cellDetailText.text = [entity valueForKey:entityKey];
    if (entityKey != nil)
    {
                
        cellDetailText.text = [entity valueForKey:entityKey];
        
    }
    celllabelText.text = entityLabel;
    if (cellbiglabelText != nil)
    {
        if (cellDetailText.text == nil || [cellDetailText.text length] == 0)
        {
            cellbiglabelText.hidden = NO;
            cellbiglabelText.text = entityLabel;
            celllabelText.hidden = YES;
            if  ([delegate isFieldInError:entityKey] == YES)
                cellbiglabelText.textColor = [UIColor redColor];
            
        }
        else
        {
            cellbiglabelText.hidden = YES;
            celllabelText.hidden = NO;
        }
    }
    else
    {
        cellbiglabelText.hidden = YES;
        celllabelText.hidden = NO;
    }

    if([popoverController isPopoverVisible])
    {
        //close the popover view         
        [popoverController dismissPopoverAnimated:YES];
        
        
    }
    if(parentController)
        [parentController dismissModalViewControllerAnimated:YES];
    if (parentViewQueue != nil)
        [[NSNotificationCenter defaultCenter] postNotificationName:parentViewQueue object:nil userInfo:nil];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected == YES)
        [self UpdateButtonPressed:nil];
}
-(IBAction)UpdateButtonPressed:(id)sender
{
    if([popoverController isPopoverVisible])
    {
        //close the popover view if toolbar button was touched
        //again and popover is already visible
        
        [popoverController dismissPopoverAnimated:YES];
        return;
    }
    
    UIButton *button = (UIButton*) sender;    
    
    
    if (selectListType == 0)
    {
             EnterValue* popoverContent = [[EnterValue alloc] initWithNibName:@"EnterValue" bundle:nil];
        
        [popoverContent setupData:entity entityKeyString:entityKey entityLabel:entityLabel];
        //    [popoverContent setupData:entity entityKeyString:entityKey entityLabelString:celllabelText.text parent:nil];
            
        //    popoverContent.notifyQueue = notifyQueue;
            //resize the popover view shown
            //in the current view to the view's size
            UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
     //       toolsNavController.view.frame = CGRectMake(0.0, -10.0, 320.0, 100);
            
     
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;

        [parentController presentModalViewController:toolsNavController animated:YES];
    
        //release the popover content
    }   
    else if (selectListType == 1)
    {
        SelectItemItem *popoverContent = [[SelectItemItem alloc] initWithNibName:@"SelectItemItem" bundle:nil];

        [popoverContent setupData:entity entityKeyString:entityKey entityselectListType:selectListType entitySelectList:selectList parentQueue:entityKey];
        popoverContent.label = celllabelText.text;
        popoverContent.notifyQueue = entityKey;
        //resize the popover view shown
        //in the current view to the view's size
        popoverContent.contentSizeForViewInPopover =
        CGSizeMake(500, 500);
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.08627450980392 green:0.15686274509804 blue:0.20392156862745 alpha:1];
        
    //    navigationController.view.frame = CGRectMake(0.0, -10.0, 500.0, 300);
        
        
        //create a popover controller
        
        
        [parentController presentModalViewController:navigationController animated:YES];
        
        
        //release the popover content
    } 
         else if (selectListType == 3)
     {
         SelectInterval* popoverContent = [[SelectInterval alloc]
                                       init];
        [popoverContent setupData:entity entityKeyString:entityKey entityLabelString:celllabelText.text parent:nil];
         
         popoverContent.notifyQueue = entityKey;
         //resize the popover view shown
         //in the current view to the view's size
         popoverContent.contentSizeForViewInPopover =
         CGSizeMake(390, 240);
         UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent] ;
         toolsNavController.view.frame = CGRectMake(0.0, -10.0, 290, 100);
         
         //create a popover controller
         popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:toolsNavController];
         //present the popover view non-modal with a
         //refrence to the toolbar button which was pressed
         
         CGRect buttonFrameInDetailView = button.frame;
         
         [popoverController presentPopoverFromRect:buttonFrameInDetailView
                                            inView:self permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
         
         //release the popover content
     }
     else if (selectListType == 4)
     {
         SelectFixedItemList* popoverContent = [[SelectFixedItemList alloc]
                                           init];
         [popoverContent setupData:entity entityKeyString:entityKey entityselectListType:selectListType entitySelectListString:selectList parentQueue:notifyQueue];
         
         
         //resize the popover view shown
         //in the current view to the view's size
         popoverContent.contentSizeForViewInPopover =
         CGSizeMake(390, 330);
         UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent] ;
         toolsNavController.view.frame = CGRectMake(0.0, -10.0, 290, 100);
         
         //create a popover controller
         popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:toolsNavController];
         //present the popover view non-modal with a
         //refrence to the toolbar button which was pressed
         
         CGRect buttonFrameInDetailView = button.frame;
         
         [popoverController presentPopoverFromRect:buttonFrameInDetailView
                                            inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
         
         //release the popover content
     }
   
}

@end
