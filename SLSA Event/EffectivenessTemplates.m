//
//  EffectivenessTemplates.m
//  SLSARiskAssessment
//
//  Created by Julian on 16/03/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "EffectivenessTemplates.h"
#import "SLSARiskAssessmentAppDelegate.h"
#import "EffectivenessTemplate.h"
@implementation EffectivenessTemplates

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
       [recordkey release];
    [super dealloc];
}
-(void)setupData:(NSString*) entityObject entityKeyString:(NSString*) entityKeyString
{
    SLSARiskAssessmentAppDelegate *delegate = (SLSARiskAssessmentAppDelegate* )[[UIApplication sharedApplication] delegate];
	
    if (entityObject != nil)
    {
        NSURL* tempURL = [[NSURL alloc] initWithString:entityObject];
        selectedObject = [delegate objectWithURI:tempURL];
        [tempURL release];
    }
    
    recordkey = [[NSString alloc] initWithString:entityKeyString];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Select Effectiveness";
	
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    SLSARiskAssessmentAppDelegate *delegate = (SLSARiskAssessmentAppDelegate* )[[UIApplication sharedApplication] delegate];
	
	itemArray = [delegate GetEffectivenessTemplate];
	[self.tableView reloadData];
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemArray count];
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    int row = [indexPath row];
	SLSARiskAssessmentAppDelegate *delegate = (SLSARiskAssessmentAppDelegate* )[[UIApplication sharedApplication] delegate];
	NSArray *tempcontrolsArray = [delegate GetEffectivenessTemplate];
    EffectivenessTemplate  *control = [tempcontrolsArray objectAtIndex:row ];
    cell.textLabel.text =  [[control valueForKey:@"Description"] description];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	int row = [indexPath row];
	SLSARiskAssessmentAppDelegate *delegate = (SLSARiskAssessmentAppDelegate* )[[UIApplication sharedApplication] delegate];
	NSArray *tempArray = [delegate GetEffectivenessTemplate];
    EffectivenessTemplate  *effectivenessTemplate = [tempArray objectAtIndex:row ];
    
    int ivalue = [[effectivenessTemplate Order] intValue];
    Control *control = (Control*)selectedObject;
    [control setValue:[NSNumber numberWithInt:ivalue]  forKey:recordkey];
   [self dismissModalViewControllerAnimated:YES];
    
}

@end
