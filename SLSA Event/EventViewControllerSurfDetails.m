//
//  EventViewControllerSurfDetails.m
//  SLSA Event
//
//  Created by Julian robertshawe on 4/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerSurfDetails.h"
#import "events.h"
#import "AssessmentTemplates.h"
#import "SelectInterval.h"
@interface EventViewControllerSurfDetails ()

@end

@implementation EventViewControllerSurfDetails

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad {
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonImage = [UIImage imageNamed:@"but_done_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
  //  self.navigationItem.rightBarButtonItem = backButtonItem;
    
    
   if (selectedObject != nil)
	{
        
		if ([selectedObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
		{
			newRecord = NO;
		}
		else
		{
            Assessment* assessmentObject = (Assessment *)[NSEntityDescription insertNewObjectForEntityForName:@"Assessment" inManagedObjectContext:[delegate managedObjectContext]];
            [assessmentObject setValue:[NSDate date] forKey:@"AssessmentDate"];
            
			newRecord = YES;
            [assessmentObject setValue:governmentArea.ContactName forKey:@"Assessor"];
            
		}
	}
	else
	{
        selectedObject = (Assessment *)[NSEntityDescription insertNewObjectForEntityForName:@"Assessment" inManagedObjectContext:[delegate managedObjectContext]];
        [selectedObject setValue:[NSDate date] forKey:@"AssessmentDate"];
        
        [selectedObject setValue:governmentArea.ContactName forKey:@"Assessor"];
		newRecord = YES;
	}
    [delegate saveContext];
       [delegate.fieldinError removeAllObjects];
    [self._tableView setEditing:YES animated:YES];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"refreshView"
     object:nil];
    
}
-(void)handleNotification:(NSNotification *)pNotification
{
    
    [self._tableView reloadData];
}

- (void) didTapBackButton:(id)sender {
    
    if ([self checkValid] == YES)
    {
        [self._tableView reloadData];
        return;
    };
    selectedObject.surfChecked = [NSNumber numberWithBool: YES];
    
     EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    [delegate saveContext];
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite
{
    
    localSiteObject = localSite;
    if ([entityObject isKindOfClass:[Assessment class]])
    {
        selectedObject = entityObject;
    }
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    
    return NO;
}

-(BOOL) checkValid
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    bool notValid = NO;
    return notValid;
    if (selectedObject.CurrentSwellSize == nil)
    {
        [delegate.fieldinError addObject:@"CurrentSwellSize"];
        notValid = YES;
    }
    if (selectedObject.CurrentSwellTime == nil)
    {
        [delegate.fieldinError addObject:@"CurrentSwellTime"];
        notValid = YES;
    }
    if (selectedObject.CurrentTide == nil)
    {
        [delegate.fieldinError addObject:@"CurrentTide"];
        notValid = YES;
    }
    if (selectedObject.CurrentWaveType == nil)
    {
        [delegate.fieldinError addObject:@"CurrentWaveType"];
        notValid = YES;
    }
    
    if (selectedObject.PredictionSwellTime == nil)
    {
        [delegate.fieldinError addObject:@"PredictionSwellTime"];
        notValid = YES;
    }
    if (selectedObject.PredictionLowTide == nil)
    {
        [delegate.fieldinError addObject:@"PredictionLowTide"];
        notValid = YES;
    }
    if (selectedObject.PredictionHighTide == nil)
    {
        [delegate.fieldinError addObject:@"PredictionHighTide"];
        notValid = YES;
    }
    if (selectedObject.PredictionWaveType == nil)
    {
        [delegate.fieldinError addObject:@"PredictionWaveType"];
        notValid = YES;
    }
    if (selectedObject.PredictionSwellSize == nil)
    {
        [delegate.fieldinError addObject:@"PredictionSwellSize"];
        notValid = YES;
    }
    [self._tableView reloadData];
    
    return notValid;

    
    
    
    
      
}
- (void)cancelRequest
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
	if (newRecord == YES)
		[[delegate managedObjectContext] deleteObject:selectedObject];
	[[self navigationController] popViewControllerAnimated:YES];
}
- (void)saveObject
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
	
	if (selectedObject != nil)
	{
		if ([selectedObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
		{
            
			if (objectString != nil)
			{
                
				if ([selectedObject isKindOfClass:[LocalSite class]]) //It is a risk object so update data
				{
                    [localSiteObject addAssessmentsObject:selectedObject];
					
                    
				}
				
			}
		}
	}
	[delegate saveContext];
	
	[[self navigationController] popViewControllerAnimated:YES];
	
	
	
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)] ;
	tableView.sectionHeaderHeight = headerView.frame.size.height;
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, headerView.frame.size.width - 20, 22)] ;
	label.text = [self tableView:tableView titleForHeaderInSection:section];
	label.font = [UIFont boldSystemFontOfSize:16.0];
	label.shadowOffset = CGSizeMake(0, 1);
	label.shadowColor = [UIColor blackColor];
	label.backgroundColor = [UIColor clearColor];
    
	label.textColor = [UIColor whiteColor];
    
	[headerView addSubview:label];
	return headerView;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath   ///what edit mode are we entering
{
	
	if (self._tableView.editing == YES)
	{
		
		NSUInteger row = [indexPath row];
		NSUInteger section = [indexPath section];
		if (section == EVENTS)
		{
			NSManagedObject *item = [eventItems objectAtIndex:row];
			if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
			{
				return UITableViewCellEditingStyleInsert;
				
			}
			else //else show delete option
			{
				return UITableViewCellEditingStyleDelete;
			}
			
		}
    }
    return UITableViewCellEditingStyleNone;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self._tableView reloadData];

}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav3.png",delegate.imagePrefix];
    
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    
    [hv addSubview:image]; tableView.tableHeaderView = hv;

    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int sectionRowCount;
	
    switch (section) {
		case DETAILS:
			sectionRowCount = 4;
			break;
		case EVENTS:
			sectionRowCount = [eventItems count];
			
			break;
        case CURRENTWEATHER:
    		sectionRowCount = 4;
	        
            break;
            
        case PREDICTIONWEATHER:
    		sectionRowCount = 4;
	        
            break;
        case CURRENTSURF:
    		sectionRowCount = 4;
	        
            break;
        case PREDICTIONSURF:
    		sectionRowCount = 5;
	        
            break;
            
		default:
			sectionRowCount = 0;
			break;
	}
	
    return sectionRowCount;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
  	NSString *headerTitle = @"";
	
    switch (section) {
        case DETAILS:
            headerTitle =  @"Details";
            
            break;
        case CURRENTWEATHER:
            headerTitle =  @"Current Weather";
            
            break;
            
        case PREDICTIONWEATHER:
            headerTitle =  @"Prediction Weather";
            
            break;
        case CURRENTSURF:
            headerTitle =  @"Current Surf";
            
            break;
        case PREDICTIONSURF:
            headerTitle =  @"Predicted Surf";
            
            break;
            
        case EVENTS:
            if (eventItems == nil)
                eventItems = [[NSMutableArray alloc] init];
            else
                [eventItems removeAllObjects];
            
            [eventItems addObjectsFromArray:[selectedObject.Events allObjects]];
            [ eventItems insertObject:@"Add Activity"  atIndex:0];    //add temp line to allow adding items
            headerTitle =  @"Activity";
            break;
        default:
            headerTitle = @"";
            break;
    }
    
    
    return headerTitle;
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {;
    
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
    static NSString *CellIdentifier = @"Cell";
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
		
		
		//	UILabel *labelField  = [[UILabel alloc] initWithFrame:CGRectMake(13,10, 280  , 25)];
		//	labelField.tag = 11;
		//	labelField.textColor = [UIColor lightGrayColor];
		//	[cell.contentView addSubview:labelField];
		//	[labelField release];
		//	cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
		//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        //  if (section == EVENTS)
        //	{
        //		NSManagedObject *item = [eventItems objectAtIndex:row];
        //		if ([item isKindOfClass:[NSString class]])
        //		{
        //			NSString* labelText = (NSString*) item;
        //			cell.textLabel.text  = labelText;
        //			cell.editingAccessoryType = UITableViewCellAccessoryNone;
        //		}
        //		else
        //		{
        //			Events   *control = (Events* )item;
        //			cell.textLabel.text  = [[control valueForKey:@"Description"] description];
        //			cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
        //		}
        //
        //	}
        
        
        
	}
    /*	if (assessmentObject != nil)
     {
     if ([assessmentObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
     {
     UILabel *cell.textLabel  = (UILabel *)[cell viewWithTag:11];
     UILabel *label12  = (UILabel *)[cell viewWithTag:12];
     Assessment* item = (Assessment*) assessmentObject;
     conditionField.text = [[item valueForKey:@"Conditions"] description];
     temperatureField.text = [[item valueForKey:@"temperature"] description];
     NSDate * assementDate = [item valueForKey:@"AssessmentDate"];
     label12.text =  [formatter stringFromDate:assementDate];
     if (item.Description != nil)
     {
     cell.textLabel.text = [[item valueForKey:@"Description"] description];
     cell.textLabel.textColor = [UIColor blackColor];
     }
     
     else
     {
     cell.textLabel.text = @"Assessment Type";
     cell.textLabel.textColor = [UIColor blackColor];
     }
     
     
     
     }
     }
     */
    cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    
    if (sectionRow == 0)
    {
        
     	if (selectedObject.CurrentSwellSize != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"CurrentSwellSize"] description];
            cell.detailTextLabel.text = @"Swell Size";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Swell Size";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"CurrentSwellSize"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 1)
    {
        
     	if (selectedObject.CurrentWaveType != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"CurrentWaveType"] description];
            cell.detailTextLabel.text = @"Wave Type";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Wave Type";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"CurrentWaveType"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 2)
    {
        
     	if (selectedObject.CurrentTide != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"CurrentTide"] description];
            cell.detailTextLabel.text = @"Tide";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Tide";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"CurrentTide"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 3)
    {
        
     	if (selectedObject.CurrentSwellTime != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"CurrentSwellTime"] description];
            cell.detailTextLabel.text = @"Swell Period";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Swell Period";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"CurrentSwellTime"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 10)
    {
        
     	if (selectedObject.PredictionSwellSize != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"PredictionSwellSize"] description];
            cell.detailTextLabel.text = @"Swell Size";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Swell Size";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"PredictionSwellSize"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 11)
    {
        
     	if (selectedObject.PredictionWaveType != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"PredictionWaveType"] description];
            cell.detailTextLabel.text = @"Wave Type";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Wave Type";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"PredictionWaveType"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 12)
    {
        
     	if (selectedObject.PredictionLowTide != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"PredictionLowTide"] description];
            cell.detailTextLabel.text = @"Low Tide Time";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Low Tide Time";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"PredictionLowTide"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 13)
    {
        
     	if (selectedObject.PredictionHighTide != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"PredictionHighTide"] description];
            cell.detailTextLabel.text = @"High Tide Time";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"High Tide Time";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"PredictionHighTide"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 14)
    {
        
     	if (selectedObject.PredictionSwellTime != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"PredictionSwellTime"] description];
            cell.detailTextLabel.text = @"Swell Period";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Swell Period";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"PredictionSwellTime"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 2)
    {
        cell.editingAccessoryType = UITableViewCellAccessoryNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
		cell.textLabel.textColor = [UIColor blueColor];
    	NSDate * assementDate = [selectedObject valueForKey:@"AssessmentDate"];
        cell.textLabel.text =  [formatter stringFromDate:assementDate];
        
        
    }
    
      
    
    // Configure the cell...
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	NSUInteger section = [indexPath section];
	if (section == EVENTS )  //if add items then indent
		return YES;
	
	return NO;
}
/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	if (section == EVENTS)
	{
		NSManagedObject *item = [eventItems objectAtIndex:row];
		if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
		{
            Events*  events = [[Events alloc] initWithNibName: @"Events" bundle:nil];
            [events setupData:selectedObject];
            
            [[self navigationController] pushViewController:events animated:YES];
          	
		}
		else //else show delete option
		{
            [selectedObject removeEventsObject:item];
            [self._tableView reloadData];
			;
		}
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
    SwellSizeTemplates* swellSizeTemplates;
    WaveTypeTemplates* waveTypeTemplates;
    TideTemplates* tideTemplates;
    SelectTime * selectTime;
    EnterValue*  enterValue;
    SelectInterval* selectInterval;
   if (sectionRow  == 0)
	{
		swellSizeTemplates = [[SwellSizeTemplates alloc] initWithNibName: @"SwellSizeTemplates" bundle:nil];
        [swellSizeTemplates setupData:selectedObject entityKeyString:@"CurrentSwellSize"];
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:swellSizeTemplates] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
         toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:toolsNavController animated:YES];
        
		
	}
    else if (sectionRow  == 1)
	{
		waveTypeTemplates = [[WaveTypeTemplates alloc] initWithNibName: @"WaveTypeTemplates" bundle:nil];
        [waveTypeTemplates setupData:selectedObject entityKeyString:@"CurrentWaveType"];
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:waveTypeTemplates] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
          toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
         toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:toolsNavController animated:YES];
		
		
	}
    else if (sectionRow  == 2)
	{
        tideTemplates = [[TideTemplates alloc] initWithNibName: @"TideTemplates" bundle:nil];
        [tideTemplates setupData:selectedObject entityKeyString:@"CurrentTide"];
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:tideTemplates] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
          toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentModalViewController:toolsNavController animated:YES];
        
		
	}
    else if (sectionRow  == 3)
	{
		selectInterval = [[SelectInterval alloc] initWithNibName: @"SelectInterval" bundle:nil];
        [selectInterval setupData:selectedObject entityKeyString:@"CurrentSwellTime"  entityLabelString:@"Current Swell Time" parent:@"refreshView"];
        
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:selectInterval] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
          toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentModalViewController:toolsNavController animated:YES];
        
		
	}
    else if (sectionRow  == 10)
	{
		swellSizeTemplates = [[SwellSizeTemplates alloc] initWithNibName: @"SwellSizeTemplates" bundle:nil];
        [swellSizeTemplates setupData:selectedObject entityKeyString:@"PredictionSwellSize"];
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:swellSizeTemplates] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
          toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentModalViewController:toolsNavController animated:YES];
		
		
	}
    else if (sectionRow  == 11)
	{
		waveTypeTemplates = [[WaveTypeTemplates alloc] initWithNibName: @"WaveTypeTemplates" bundle:nil];
        [waveTypeTemplates setupData:selectedObject entityKeyString:@"PredictionWaveType"];
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:waveTypeTemplates] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
          toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:toolsNavController animated:YES];
        
		
	}
    else if (sectionRow  == 12)
	{
		selectTime = [[SelectTime alloc] initWithNibName: @"SelectTime" bundle:nil];
        [selectTime setupData:selectedObject entityKeyString:@"PredictionLowTide" entityKeyStringValue:@"PredictionLowTideValue"];
        
	    
        selectTime.titleText = @"Low Tide Time";
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:selectTime] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
          toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:toolsNavController animated:YES];
        
		
	}
    else if (sectionRow  == 13)
	{
		selectTime = [[SelectTime alloc] initWithNibName: @"SelectTime" bundle:nil];
        [selectTime setupData:selectedObject entityKeyString:@"PredictionHighTide" entityKeyStringValue:@"PredictionHighTideValue"];
	    
        selectTime.titleText = @"High Tide Time";
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:selectTime] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
          toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentModalViewController:toolsNavController animated:YES];
		
	}
    else if (sectionRow  == 14)
	{
		selectInterval = [[SelectInterval alloc] initWithNibName: @"SelectInterval" bundle:nil];
        [selectInterval setupData:selectedObject entityKeyString:@"PredictionSwellTime"  entityLabelString:@"Prediction Swell Time" parent:@"refreshView"];
        
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:selectInterval] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
          toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:toolsNavController animated:YES];
        
		
	}
	    
}






@end

