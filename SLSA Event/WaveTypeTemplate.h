//
//  WaveTypeTemplate.h
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface WaveTypeTemplate : NSManagedObject

@property (nonatomic, retain) NSNumber * Order;
@property (nonatomic, retain) NSString * Description;

@end
