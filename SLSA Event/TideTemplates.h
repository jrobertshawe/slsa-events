//
//  TideTemplates.h
//  SLSARiskAssessment
//
//  Created by Julian on 22/03/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

@interface TideTemplates : UITableViewController {
    
    NSArray* itemArray;   
    NSManagedObject *selectedObject;
    NSString *recordkey;
}
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString;
@end