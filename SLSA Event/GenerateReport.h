//
//  GenerateReport.h
//  SLSARiskAssessment
//
//  Created by Julian on 17/12/10.
//  Copyright 2010 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Hazard.h"
#import "Assessment.h"
#import "LocalSite.h"
#import "Control.h"
#import "Event.h"
#define RIGHTMARGIN 832
#define LEFTMARGIN 20
#define HAZARDCOLUMN 78
#define PHOTOCOLUMN 151
#define RISKCOLUMN 222
#define RISKSCORECOLUMN 287
#define CONSEQCOLUMN 341
#define RISKSCOREMIDDLECOLUMN 417
#define EXISTINGCONTROLSCOLUMN 547
#define RISKRATINGCOLUMN 590
#define PROPOSEDCONTROLSCOLUMN 760
#define RESIDUALRATINGCOLUMN 832
#define PAGEWIDTH 842
#define PAGEHEIGHT 595
#define LINEHEIGHT 15
#define TITLEROWHEIGHT 50
#define RIGHTMARGININDENT 450
#define EVENTROWHEIGHT 40
#define EVENTROWHEIGHT 40
#define FONTSIZE 8
#define LARGEFONTSIZE 11
@interface GenerateReport : UIViewController <MFMailComposeViewControllerDelegate> {
	NSString									*fileName;
	NSString									*kmlfileName;
	IBOutlet UIWebView							*webView;
	NSArray *reportObjects;
	NSMutableString *kmlMapString;
	Assessment* assessment;
    LocalSite* localSite;
}
@property (nonatomic, retain) UIWebView			*webView;
@property (nonatomic, retain) Assessment* assessment;
-(void)displayComposerSheet ;
- (int)WriteHeaderBlock:(CGContextRef) context StartyPostion:(int)startyPostion;
- (int)WriteHeaderBlock2:(CGContextRef) context StartyPostion:(int)startyPostion;

-(void)CreatePDFFile:(CGRect) pageRect;
- (void)DrawStringCentered:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(const char *) str textBold:(bool)textBold  fontsizing:(int)fontSizing;
- (void)DrawStringRightAlign:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(const char *) str textBold:(bool)textBold;
- (void)DrawStringLeftAlign:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(const char *) str textBold:(bool)textBold fontsizing:(int)fontSizing;
- (void)DrawNSStringRightAlign:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString* ) str textBold:(bool)textBold Width:(int) width;
- (int )DrawNSStringLeftAlignWrap:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString*) str textBold:(bool)textBold  Width:(int) width;
- (void)DrawNSStringLeftAlign:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString*) str textBold:(bool)textBold fontsizing:(int)fontSizing;
- (void)DrawNSStringCentered:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString*) str textBold:(bool)textBold;
- (void)drawRectGrey:(CGContextRef) context drawrect:(CGRect)rect;
- (void)drawRectRed:(CGContextRef) context drawrect:(CGRect)rect;
- (void)drawRectOrange:(CGContextRef) context drawrect:(CGRect)rect;
- (void)drawRectGreen:(CGContextRef) context drawrect:(CGRect)rect;
- (void)drawRectYellow:(CGContextRef) context drawrect:(CGRect)rect;
- (int)WriteHazardLine:(CGContextRef) context HazardObject:(Hazard* )hazard StartyPostion:(int)startyPostion;
-(int)HeightForStringDrawing:(NSString *)myString FontSize:(int) fontSize RectWidth:(int) myWidth;
- (void)DrawNSStringLeftAlignRect:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString* ) str textBold:(bool)textBold;
-(void)DrawLogo;
- (void)DrawNSStringCenteredWhite:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString*) str textBold:(bool)textBold;
- (void)DrawNSStringCenteredBig:(CGContextRef) gc xPos:(float) xPos yPos:(float) yPos str:(NSString*) str textBold:(bool)textBold;
@end
