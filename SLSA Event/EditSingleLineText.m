//
//  EditSingleLineText.m
//  SLSA Risk Assessment
//
//  Created by Julian on 20/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "EditSingleLineText.h"

#import "EventAppDelegate.h"
@implementation EditSingleLineText
@synthesize textField;

@synthesize notifyQueue;
@synthesize labelField; 
@synthesize isTemplate;
@synthesize notesField;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    isTemplate = NO;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *navBar;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        navBar = @"ipad_img_blue-header.png";
    }
    else {
        navBar = @"iphone_img_blueheader.png";
    }
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:navBar];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
    if (entity != nil)
        {   
            NSString *stringValue = nil;
            stringValue = [entity valueForKey:entityKey];
            if (stringValue != nil)
                textField.text = stringValue;
            
              if ([entityKey isEqualToString:@"Notes"] == YES)  
                  {
                      textField.hidden  = YES;
                       notesField.hidden  = NO;
                  }
            
            
            if (([entityKey isEqualToString:@"PhoneNumber"] == YES) || ([entityKey isEqualToString:@"CurrentWeatherTemp"] == YES) || ([entityKey isEqualToString:@"PredictionWeatherTemp"] == YES))
                [textField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
            else
                [textField setKeyboardType:UIKeyboardTypeAlphabet];
            
        }
        

     [textField becomeFirstResponder];
  
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(DoneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.rightBarButtonItem = backButtonItem;

    
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
//    textField.center = CGPointMake(self.view.center.x, textField.center.y);
}
- (void) didTapBackButton:(id)sender {
    
    
    
    
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString parent:(NSString*) parentString
{
    
    if (parentString != nil && [parentString length] > 0)
    {
        entityParentString = [[NSString alloc] initWithString:parentString];
           
    }
    entityKey = [[NSString alloc] initWithString:entityKeyString];;
    self.navigationItem.title = entityLabelString;
    entity = entityObject;
         if (entity != nil)
        {   
            NSString *stringValue = nil;
                    stringValue = [entity valueForKey:entityKey];
                if (stringValue != nil)
                    textField.text = stringValue;
                [textField setKeyboardType:UIKeyboardTypeAlphabet];
          
        }
    
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(IBAction)DoneButtonPressed
{
    NSManagedObject*       entityParent = nil;
  
    if ([textField.text length] == 0)
        return;
    
    bool dataisvalid = YES;
   
  
 
    NSString* entityParentName = nil;
   
    if (entityParent != nil)
    {
        NSEntityDescription *nsd = [entityParent entity];
        entityParentName = [nsd name];
    }
    if (entity != nil)
    {
        if ([entityKey isEqualToString:@"Notes"] == YES)
            [entity setValue:notesField.text forKey:entityKey];
        else
            [entity setValue:textField.text forKey:entityKey];
        [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];
      
        
        
        
    }

    [self dismissModalViewControllerAnimated:YES];
   
}
@end
