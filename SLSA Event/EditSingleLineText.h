//
//  EditSingleLineText.h
//  SLSA Risk Assessment
//
//  Created by Julian on 20/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

@interface EditSingleLineText : UIViewController {
    NSManagedObject*       entity;
    NSString*       entityParentString; 
    
    NSString*               entityKey;
    NSString*               label; 
    NSString*               notifyQueue;  
    IBOutlet UITextField *  textField; 
    IBOutlet UILabel *      labelField;
    NSObject*               parentView;
    BOOL                    isTemplate;
}
@property   BOOL                    isTemplate;
@property (nonatomic,retain)  NSString						*notifyQueue;
@property (nonatomic,retain)  IBOutlet UITextField                  *textField;
 @property (nonatomic,retain)  IBOutlet UITextView *  notesField;
@property (nonatomic,retain)  IBOutlet UILabel                  *labelField;
-(IBAction)DoneButtonPressed;
-(void)setupData:(NSString*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString parent:(NSString*) parentString;
@end
