//
//  LocationCell.h
//  SLSA Risk Assessment
//
//  Created by Julian on 9/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "GetCurrerntLocation.h"
@interface LocationCell : UITableViewCell <UINavigationControllerDelegate> {
    IBOutlet UILabel *latitude;
     IBOutlet UILabel *longitude;
    IBOutlet UILabel *GPSlatitude;
    IBOutlet UILabel *GPSlongitude;
    
    IBOutlet UILabel *accuracyLabel;
    GetCurrerntLocation *locationController;
    float accuracy;
	NSString *longField;
	NSString *latField;
    NSManagedObject* entity;
    UIPopoverController *popoverController;
    IBOutlet UIButton *updateButton;
     IBOutlet UIButton *gpsButton;
}
@property (nonatomic,retain)  IBOutlet UIButton *updateButton;
@property (nonatomic,retain)  IBOutlet UIButton *gpsButton;
@property (nonatomic,retain)  UILabel						*accuracyLabel;
@property (nonatomic,retain)  UILabel						*latitude;
@property (nonatomic,retain)  UILabel						*longitude;
@property (nonatomic,retain)  UILabel						*GPSlatitude;
@property (nonatomic,retain)  UILabel						*GPSlongitude;
-(void)setupData:(NSManagedObject*) entityObject;
- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;
- (IBAction)GPSMark:(id)sender;
-(IBAction)DropPin:(id)sender;
@end
