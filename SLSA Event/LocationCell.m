//
//  LocationCell.m
//  SLSA Risk Assessment
//
//  Created by Julian on 9/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "LocationCell.h"
#import "SelectLocation.h"
#import "EventAppDelegate.h"
@implementation LocationCell
@synthesize latitude;
@synthesize longitude;
@synthesize accuracyLabel;
@synthesize GPSlatitude;
@synthesize GPSlongitude,updateButton,gpsButton;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)setupData:(NSManagedObject*) entityObject
{
    entity = entityObject;
     locationController = [[GetCurrerntLocation alloc] init];
	locationController.delegate = self;
	[locationController.locationManager startUpdatingLocation];
  	GPSlatitude.text = [entity valueForKey:@"Latitute"];
    GPSlongitude.text  = [entity valueForKey:@"Longitude"];
    
    accuracy = 0;
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"LocationCell"
     object:nil];  
      [updateButton addTarget:self action:@selector(DropPin:) forControlEvents:UIControlEventTouchUpInside];
    [gpsButton addTarget:self action:@selector(GPSMark:) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)handleNotification:(NSNotification *)pNotification
{
    if([popoverController isPopoverVisible])
    {
        //close the popover view         
        [popoverController dismissPopoverAnimated:YES];
        latitude.text = [entity valueForKey:@"Latitute"];
        longitude.text  = [entity valueForKey:@"Longitude"];
        GPSlongitude.text =  longitude.text;
        GPSlatitude.text  = latitude.text;

        //  cellDetailText.text = [item valueForKey:entityColumn];
        
    }
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(IBAction)DropPin:(id)sender
{
    UIButton *button = (UIButton*) sender;
    CGRect buttonFrameInDetailView = button.frame;
    
	SelectLocation* popoverContent = [[SelectLocation alloc]
                                 init];
    
    
    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.contentSizeForViewInPopover =
    CGSizeMake(600, 600);
    
    //create a popover controller
    popoverController = [[UIPopoverController alloc]
                         initWithContentViewController:popoverContent];
    popoverContent.entity = entity;
    popoverContent.notifyQueue = @"LocationCell";
    
    //present the popover view non-modal with a
    //refrence to the toolbar button which was pressed
    UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent] ;
    toolsNavController.view.frame = CGRectMake(0.0, -10.0, 600.0, 600);
    
    //create a popover controller
    popoverController = [[UIPopoverController alloc]
                         initWithContentViewController:toolsNavController];
    
    [popoverController presentPopoverFromRect:buttonFrameInDetailView
                                       inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    
   

}
-(IBAction)GPSMark:(id)sender
{
    GPSlongitude.text =  longField;
    GPSlatitude.text  = latField;
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    [entity setValue:longField forKey:@"Longitude"];
    [entity setValue:latField forKey:@"Latitute"];
    [delegate saveContext];
     
}
- (void)locationUpdate:(CLLocation *)location
{
	
    
        accuracy = location.horizontalAccuracy;
    if (accuracy < 10)
           [[NSNotificationCenter defaultCenter] postNotificationName:@"GPSOn" object:nil userInfo:nil];
    else
           [[NSNotificationCenter defaultCenter] postNotificationName:@"GPSOff" object:nil userInfo:nil];
        longField = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
        latField  = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
        accuracyLabel.text = [NSString stringWithFormat:@"+/-%gm",accuracy];
        
        
        //	   [self.tableView reloadData];

    
	//gpsField.text  = [NSString stringWithFormat:@"lat=%f long=%f h=%f v=%f ",location.coordinate.latitude,location.coordinate.longitude,location.horizontalAccuracy,location.verticalAccuracy];
	
	
}
- (void)locationError:(NSError *)error {
}
@end
