//
//  Photo.m
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "Photo.h"
#import "Hazard.h"


@implementation Photo

@dynamic newAttribute;
@dynamic Name;
@dynamic Hazard;

@end
