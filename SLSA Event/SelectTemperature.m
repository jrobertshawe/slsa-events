//
//  SelectTemperature.m
//  AussieIpad
//
//  Created by Julian robertshawe on 15/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SelectTemperature.h"

@implementation SelectTemperature
@synthesize notifyQueue;
@synthesize labelField; 
@synthesize intervalSelectPicker;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    intervals = [[NSMutableArray alloc] init];
    int pos = 10;
    NSString* currentValue = label;
    NSString* tempstring;    
    for (int i = 10; i < 50; i++)
    {
        tempstring = [NSString stringWithFormat:@"%d",i];
        [intervals addObject:tempstring];
        if (currentValue != nil)
         if ([currentValue isEqualToString:tempstring] == YES)
             pos = i-10;
    }
    
    [intervalSelectPicker reloadAllComponents];
    [intervalSelectPicker selectRow:pos inComponent:0 animated:YES];
    [self tempChanged];
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [intervals count];
    
}
-(IBAction)tempChanged
{
    NSUInteger selectedRow = [intervalSelectPicker selectedRowInComponent:0];
    
    NSString* value = [NSString stringWithFormat:@"%@", [intervals objectAtIndex:selectedRow]];
    
    
    labelField.text =  [NSString stringWithFormat:@"%@ degrees", [intervals objectAtIndex:selectedRow]];
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    
    UILabel *retval = (id)view;
    if (!retval) {
        retval= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 5.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)] ;
    }
    retval.text = [NSString stringWithFormat: @"%@", [intervals objectAtIndex:row]];
    
    retval.font = [UIFont systemFontOfSize:40];
    return retval;
}

-(void) pickerView:(UIPickerView *) thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSInteger selectedRow = row;
    NSString* value = [NSString stringWithFormat:@"%@", [intervals objectAtIndex:selectedRow]];
    labelField.text =  [NSString stringWithFormat:@"%@ degrees", [intervals objectAtIndex:selectedRow]];
    
}
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    
    return 55;
}
- (void)viewWillAppear:(BOOL)animated
{
  //  NSUserDefaults *defaultsPicker = [NSUserDefaults standardUserDefaults];
	//int selIndex = [[defaultsPicker objectForKey:@"picker"] intValue];

}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void) didTapBackButton:(id)sender {
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    NSUInteger selectedRow = [intervalSelectPicker selectedRowInComponent:0];
    
    NSString* value = [NSString stringWithFormat:@"%@", [intervals objectAtIndex:selectedRow]];
    [entity setValue:[NSString stringWithFormat:@"%@",value] forKey:entityKey];
    [delegate saveContext];
       
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshView" object:nil userInfo:nil];
    [self dismissModalViewControllerAnimated:YES];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString parent:(NSString*) parentString
{
    entityKey = [[NSString alloc] initWithString:entityKeyString];;
    notifyQueue= [[NSString alloc] initWithString:parentString];;
    self.navigationItem.title = entityLabelString;
    
        entity = entityObject;
        if (entity != nil)
        {   
            NSString *stringValue = nil;
            stringValue = [entity valueForKey:entityKey];
            if (stringValue != nil)
            {
                labelField.text = stringValue;
                label =   [[NSString alloc] initWithString:stringValue];
            }
            
        }
        
    }

-(IBAction)DoneButtonPressed
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    NSUInteger selectedRow = [intervalSelectPicker selectedRowInComponent:0];
    
    NSString* value = [NSString stringWithFormat:@"%@", [intervals objectAtIndex:selectedRow]];    
          [entity setValue:[NSString stringWithFormat:@"%@",value] forKey:entityKey];
        [delegate saveContext];
     [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];
    
}

@end
