//
//  SingleLineListItemCell.h
//  SLSA Risk Assessment
//
//  Created by Julian on 24/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface SingleLineListItemCell : UITableViewCell {
    IBOutlet UILabel *cellDetailText;
    IBOutlet UILabel *cellDateText;
    IBOutlet UILabel *cellStatusText;
    IBOutlet UILabel *cellDescriptionText;
}
@property (nonatomic,retain)  UILabel						*cellDescriptionText;
@property (nonatomic,retain)  UILabel						*cellDetailText;
@property (nonatomic,retain)  UILabel						*cellStatusText;
@property (nonatomic,retain)  UILabel						*cellDateText;
-(void)setupData:(NSManagedObject*) entityObject;
@end
