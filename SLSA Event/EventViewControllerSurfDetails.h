//
//  EventViewControllerSurfDetails.h
//  SLSA Event
//
//  Created by Julian robertshawe on 4/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

#define HEADER_FIELD_TAG_1 0
#define HEADER_FIELD_TAG_2 1


///Table Cell Indexes and Identifiers
#define DETAILS 2
#define CURRENTWEATHER 3
#define CURRENTSURF 0
#define PREDICTIONWEATHER 4
#define PREDICTIONSURF 1
#define EVENTS 5

@interface EventViewControllerSurfDetails : UIViewController
{
	IBOutlet UITextField			*conditionField;
    IBOutlet UITextField		*temperatureField;
	IBOutlet UITextField			*descriptionField;
	NSString *objectString;
    bool newRecord;
    NSMutableArray					*eventItems;
    GovernmentArea* governmentArea;
    LocalSite* localSiteObject;
    
    Assessment* selectedObject;
}
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (nonatomic, retain) GovernmentArea* governmentArea;
-(void)handleNotification:(NSNotification *)pNotification;
- (void)cancelRequest;
- (void)saveObject;
-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite;
@end