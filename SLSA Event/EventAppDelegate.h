//
//  EventAppDelegate.h
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "GetCurrerntLocation.h"
#import "HazardTemplate.h"
#import "GovernmentArea.h"
#import "LocalSite.h"
#import "Event.h"
#import "Assessment.h"
#import "WindDirectionTemplate.h"
#import "Hazard.h"
#import "Assessment.h"
#import "WindSpeedTemplates.h"
#import "WaveTypeTemplates.h"
#import "WindDirectionTemplates.h"
#import "EnterValue.h"
#import "SwellSizeTemplates.h"
#import "CloudCover.h"
#import "TideTemplates.h"
#import "SelectTime.h"
#import "EventTemplate.h"
#import "AssessmentTemplate.h"
#import "WindSpeedTemplate.h"
#import "SwellSizeTemplate.h"
#import "WaveTypeTemplate.h"
#import "CloudTemplate.h"
#import "TideTemplate.h"
#import "Photo.h"
#import "risk.h"
#import "RiskGroup.h"
#import "Control.h"
#import "EffectivenessTemplate.h"
#import "UINavigationController+KeyboardDismiss.h"
#define AS(A,B)    [(A) stringByAppendingString:(B)]
static inline double radians (double degrees) {return degrees * M_PI/180;}
@interface EventAppDelegate : UIResponder <UIApplicationDelegate>
{
    @private
    NSManagedObjectContext *managedObjectContext_;
    NSManagedObjectModel *managedObjectModel_;
    NSPersistentStoreCoordinator *persistentStoreCoordinator_;
    float longField;
	float latField;
    float accuracy;
    GetCurrerntLocation *locationController;
    NSMutableArray* fieldinError;
}
@property  float longField;
@property  float latField;
@property  float accuracy;
@property  int header_height;
@property  int header_length;
@property (strong, nonatomic) UIViewController* areaViewController;
@property (strong, nonatomic) NSString* imagePrefix;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray *fieldinError;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
- (NSString *)applicationDocumentsDirectory;
- (void)saveContext;
-(NSArray*) GetHazardTemplates;
-(void) AddHazardTemplate:(NSString*) description  DisplayOrder:(int)order;
-(NSArray*) GetAreas;
-(void)AddAreas:(GovernmentArea *) governmentArea;
-(GovernmentArea*) AddGovernmentArea:(NSString*) name Description:(NSString*)description PhoneNumber:(NSString*)phoneNumber ContactName:(NSString*)contactName Address:(NSString*)address;
-(void) DeleteRecord:(NSManagedObject *)record;
-(NSArray*) GetRisks;
-(NSArray*) GetRiskGroups;
-(NSArray*) GetControls;
-(NSArray*) GetAssessmentTemplates;
-(NSArray*) GetLocalGovermentArea;
-(NSArray*) GetEventTemlates;
-(NSArray*) GetWindDirectionTemplates;
-(NSArray*) GetWindSpeedTemplates;
-(NSArray*) GetSwellSizeTemplates;
-(NSArray*) GetWaveTypeTemplates;
-(NSArray*) GetEffectivenessTemplate;
-(NSArray*) GetCloudTemplates;
-(NSArray*) GetTideTemplates;
-(void) AddTemplate:(NSString*) name  templateTable:(NSString*)templateTable;
-(NSManagedObject*) AddRisk:(NSString*) name ParentRecord:(NSManagedObject*)parentRecord;
-(NSManagedObject*) AddRiskGroup:(NSString*) name ParentRecord:(NSManagedObject*)parentRecord;
-(void) AddRecommendControl:(NSString*) description  ParentRecord:(NSManagedObject*)parentRecord;
-(void) AddExistingControl:(NSString*) description  ParentRecord:(NSManagedObject*)parentRecord;
-(NSArray*) GetDataArray:(NSString*) tableName;
-(NSManagedObject*) AddPhoto:(NSString*) name hazard:(Hazard*)hazard;
-(void) AddEvent:(NSString*) description  ParentRecord:(NSString *)parentRecord;
-(void) AddEventTemplate:(NSString*) description DisplayOrder:(int)order;
-(Assessment*) CreateAssessment;
-(void) AddAssessmentTemplate:(NSString*) description  DisplayOrder:(int)order;
- (UIImage*)imageWithImage:(UIImage*)sourceImage scaledToSizeWithSameAspectRatio:(CGSize)targetSize;
-(void) AddRiskTemplate:(NSString*) description;
-(void) AddRiskGroupTemplate:(NSString*) description;
-(void) AddEffectivenessTemplate:(NSString*) description DisplayOrder:(int)order;
-(bool)isFieldInError:(NSString*)FieldToCheck;
-(void)CopyAssessment:(NSManagedObject *)parentRecord AssessmentRecord:(NSManagedObject *)record;
-(UIImage*)scaledToSize:(UIImage*)sourceImage newSize:(CGSize)newSize;
@end
