//
//  SelectionValues.m
//  AussieIpad
//
//  Created by Julian robertshawe on 19/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SelectionValues.h"


@implementation SelectionValues

@dynamic display_order;
@dynamic itemDescription;
@dynamic itemKey;
@dynamic itemValue;
@dynamic timestamp;

@end
