//
//  Events.h
//  SLSARiskAssessment
//
//  Created by Julian on 15/03/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

@interface Events : UIViewController <UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate> {
  	NSMutableArray* itemArray;
	
	IBOutlet UITableView		*_tableView;
	NSManagedObject *selectedObject;
	NSMutableArray* selectedItems;
}
-(void)setupData:(NSManagedObject*) entityObject;
@property (nonatomic, retain) UITableView		*_tableView;
@property (strong, nonatomic) UITextField *currentTextField;
@end