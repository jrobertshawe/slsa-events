//
//  SelectInterval.m
//  AussieIpad
//
//  Created by Julian robertshawe on 18/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SelectInterval.h"

@implementation SelectInterval
@synthesize notifyQueue;
@synthesize labelField; 
@synthesize intervalSelectPicker;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
      self.navigationItem.leftBarButtonItem = backButtonItem;
    intervals = [[NSMutableArray alloc] init];
       int pos = 0;
    for (int i = 0; i < 60; i++)
    {
        [intervals addObject:[NSString stringWithFormat:@"%d",i]];
        if ([stringValue isEqualToString:[NSString stringWithFormat:@"%d",i] ] == YES)
            pos = i;
            
    }

    [intervalSelectPicker reloadAllComponents];
    [intervalSelectPicker selectRow:pos inComponent:0 animated:YES];
    NSUInteger selectedRow = [intervalSelectPicker selectedRowInComponent:0];
    
    labelField.text =  [NSString stringWithFormat:@"%@ seconds", [intervals objectAtIndex:selectedRow]];
    

    
    
   self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
    
}
- (void) didTapBackButton:(id)sender {
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    NSUInteger selectedRow = [intervalSelectPicker selectedRowInComponent:0];
    
    NSString* value = [NSString stringWithFormat:@"%@", [intervals objectAtIndex:selectedRow]];
    
    [entity setValue:[NSString stringWithFormat:@"%@",value] forKey:entityKey];
    
    [delegate saveContext];    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshView" object:nil userInfo:nil];
    [self dismissModalViewControllerAnimated:YES];
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [intervals count];
    
}
-(void) pickerView:(UIPickerView *) thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSInteger selectedRow = row;
    labelField.text =  [NSString stringWithFormat:@"%@ seconds", [intervals objectAtIndex:selectedRow]];
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    
    UILabel *retval = (id)view;
    if (!retval) {
        retval= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 5.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)] ;
    }
    retval.text = [NSString stringWithFormat: @"%@", [intervals objectAtIndex:row]];
    
    retval.font = [UIFont systemFontOfSize:40];
    return retval;
}


- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    
    return 55;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString parent:(NSString*) parentString
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    entity  = entityObject;
    intervals = [[NSMutableArray alloc] init];
    for (int i = 0; i < 60; i++)
    {
        [intervals addObject:[NSString stringWithFormat:@"%d",i]];
    }

    
    if (parentString != nil && [parentString length] > 0)
    {
        entityParentString = [[NSString alloc] initWithString:parentString];
        
    }
    entityKey = [[NSString alloc] initWithString:entityKeyString];;
    self.navigationItem.title = entityLabelString;
    
        if (entity != nil)
        {   
            stringValue = [entity valueForKey:entityKey];
            
        }
        
    
}
-(IBAction)DoneButtonPressed
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    NSUInteger selectedRow = [intervalSelectPicker selectedRowInComponent:0];
    
      NSString* value = [NSString stringWithFormat:@"%@", [intervals objectAtIndex:selectedRow]];    
    
       [entity setValue:[NSString stringWithFormat:@"%@",value] forKey:entityKey];
            [delegate saveContext];
    [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];
    
}

@end
