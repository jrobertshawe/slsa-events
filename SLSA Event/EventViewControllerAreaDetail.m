//
//  EventViewControllerAreaDetail.m
//  SLSA Event
//
//  Created by Julian robertshawe on 13/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerAreaDetail.h"
#import "SingleEditCell.h"
@interface EventViewControllerAreaDetail ()

@end

@implementation EventViewControllerAreaDetail
@synthesize _tableView;
@synthesize keyboardSize;
@synthesize currentTextField;
@synthesize addressSection;
@synthesize nameField;
@synthesize idField;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];



// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
// self.navigationItem.rightBarButtonItem = self.editButtonItem;
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav2.png",delegate.imagePrefix];
    
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    
    [hv addSubview:image];
    //  hv.backgroundColor = [UIColor whiteColor];
    _tableView.tableHeaderView = hv;
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
     [delegate.fieldinError removeAllObjects];
 
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"EventViewControllerAreaDetail"
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleRefreshNotification:)
     name:@"refreshView"
     object:nil];
}
-(void)handleRefreshNotification:(NSNotification *)pNotification
{
    
    [self._tableView reloadData];
}
-(void)handleNotification:(NSNotification *)pNotification
{
    
   [self._tableView reloadData];
}
- (void) didTapBackButton:(id)sender {
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];

    if ([object.Name length] >0 )
    {
        [delegate saveContext];
        if(self.navigationController.viewControllers.count > 1) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else
    {
        [delegate.fieldinError addObject:@"Name"];
        [_tableView reloadData];
    }

}

-(void)setupData:(NSManagedObject*) entityObject

{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
	
    if ([entityObject isKindOfClass:[GovernmentArea class]]) //It is a risk
    {
        LocalSite* localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[delegate managedObjectContext]];
        GovernmentArea* item = (GovernmentArea*) entityObject;
        [item addSitesObject:localSite];
        object = localSite;
    }
    else
        object = entityObject;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// custom rotation code based on interfaceOrientation here...
	return NO;
}
- (void)cancelRequest
{
	[[self navigationController] popViewControllerAnimated:YES];
}
- (void)insertNewObject
{
	 EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
	NSString* name = nameField.text;
	NSString* idfield = idField.text;
	             
		if ([object isKindOfClass:[LocalSite class]]) //It is a risk object so update data
		{
			LocalSite* localSite = (LocalSite*) object;
			[localSite setValue:name forKey:@"Name"];
			[localSite setValue:idfield forKey:@"BeachKey"];
		}
	else
	{
        LocalSite* localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[delegate managedObjectContext]];
        
        [localSite setValue:name forKey:@"Name"];
        [localSite setValue:idfield forKey:@"BeachKey"];
        GovernmentArea* item = (GovernmentArea*) object;
        [item addSitesObject:localSite];
        
   	}
	[delegate saveContext];
    
    [[self navigationController] popViewControllerAnimated:YES];
    
}


/*
 - (void)viewWillAppear:(BOOL)animated {
 [super viewWillAppear:animated];
 }
 */
/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    addressSection = 0;
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 2;
}
- (IBAction)textFieldDoneEditing:(id)sender {
    
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
	
    UITableViewCell *tblcell;;
  	
		
		if (sectionRow == DETAIL_SECTION_LINE1_ROW )
		{
            SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SingleEditCell"];
            if (cell == nil) {
                cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
                
            }
            
            [cell setupData:object entityKeyString:@"Name" entityLabelString:@"Name" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerAreaDetail"];
            cell.parentController = self;
            tblcell = cell;

			
		}
		else if (sectionRow == DETAIL_SECTION_LINE2_ROW )
		{
            
            
            SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SingleEditCell"];
            if (cell == nil) {
                cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
                
            }
            
            [cell setupData:object entityKeyString:@"BeachKey" entityLabelString:@"Area ID (Optional)" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerAreaDetail"];
            cell.parentController = self;
            tblcell = cell;
        }
	  
	
		
  
        tblcell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
     tblcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return tblcell;
}



#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	/*
	 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
	 [self.navigationController pushViewController:detailViewController animated:YES];
	 [detailViewController release];
	 */
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    _tableView = nil;
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}




@end
