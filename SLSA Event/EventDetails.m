//
//  EventDetails.m
//  SLSARiskAssessment
//
//  Created by Julian on 13/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "EventDetails.h"
#import "Event.h"
#import "EventTemplate.h"

@implementation EventDetails
@synthesize descriptionField;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)setupData:(NSString*) entityObject 

{
    
    
    objectString = [[NSString alloc] initWithString:entityObject];
    
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)cancelRequest
{
	[[self navigationController] popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelRequest)];
    self.navigationItem.leftBarButtonItem = cancelButton;

    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(insertNewObject)];
    self.navigationItem.rightBarButtonItem = doneButton;

	self.navigationItem.title = @"Add";
	if (objectString != nil)
	{
      
		if ([selectedObject isKindOfClass:[Event class]]) //It is a risk object so update data
		{
			self.navigationItem.title = @"Update";
		}
	}

self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                 [UIColor whiteColor], UITextAttributeTextColor,
                                                                 [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                 [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                 
                                                                 nil];


}
- (void)insertNewObject
{
	EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
	NSString* description = descriptionField.text;
	if (objectString != nil)
	{
      
		if ([selectedObject isKindOfClass:[Event class]]) //It is a risk object so update data
		{
			Event* riskItem = (Event*) selectedObject;
			[riskItem setValue:description forKey:@"name"];
		}
		if ([selectedObject isKindOfClass:[EventTemplate class]]) //It is a risk object so update data
		{
			EventTemplate* riskItem = (EventTemplate*) selectedObject;
			[riskItem setValue:description forKey:@"name"];
		}
	}
	else
	{
		
		[delegate AddEventTemplate:description  DisplayOrder:99 ];
	}
	[delegate saveContext];
	
	[[self navigationController] popViewControllerAnimated:YES];
	
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

     return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
		UITextField *textField  = [[UITextField alloc] initWithFrame:CGRectMake(10,0, 280  , 44)];
		textField.clearButtonMode = UITextFieldViewModeAlways;
		//	[textField addTarget:self action:@selector(textFieldDoneEditing:) forControlEvents:UIControlEventEditingDidEndOnExit];
		textField.textAlignment = UITextAlignmentLeft;
		textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
		textField.keyboardType = UIKeyboardTypeDefault;
		textField.returnKeyType = UIReturnKeyDefault;
		
		if (sectionRow == 0)
		{
			textField.tag = 2;
			textField.placeholder = @"Description";
			descriptionField = textField;
			if (objectString != nil)
			{
                

				if ([selectedObject isKindOfClass:[Event class]]) //It is a risk object so update data
				{
					Event* riskItem = (Event*) selectedObject;
					descriptionField.text = [[riskItem valueForKey:@"name"] description];
				}
				if ([selectedObject isKindOfClass:[EventTemplate class]]) //It is a risk object so update data
				{
					EventTemplate* riskItem = (EventTemplate*) selectedObject;
					descriptionField.text = [[riskItem valueForKey:@"name"] description];
				}
			}
		}
		[cell.contentView addSubview:textField];
		
    }
    
    // Configure the cell...
    
    return cell;}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate



@end
