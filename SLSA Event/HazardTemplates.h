//
//  HazardTemplates.h
//  SLSARiskAssessment
//
//  Created by Julian on 14/12/10.
//  Copyright 2010 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EventAppDelegate.h"
@interface HazardTemplates : UIViewController {
@private
	bool isEditing;	
	NSMutableArray* itemArray;
	
	IBOutlet UITableView		*_tableView;
    NSManagedObject* object;

	NSMutableArray* selectedItems;
}
@property (nonatomic, retain) UITableView		*_tableView;

-(void)setupData:(NSManagedObject*) entityObject;
@end