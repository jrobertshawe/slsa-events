//
//  SingleLineListControlCell.m
//  SLSA Risk Assessment
//
//  Created by Julian on 9/06/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "SingleLineListControlCell.h"
#import "SelectFixedItemList.h"
#import "EventAppDelegate.h"
@implementation SingleLineListControlCell
@synthesize cellDetailText;
@synthesize cellEffectText;
@synthesize updateButton;
@synthesize parentController;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString   entitySelectListString:(NSString*) entitySelectListString  parentQueue:(NSString*) parentQueue
{
    if ([entityObject isKindOfClass:[NSString class]] == NO)
    {
    
        entity = entityObject  ;
        entitySelectList = [[NSString alloc] initWithString:entitySelectListString];
        entityKey = [[NSString alloc] initWithString:entityKeyString];
        if (entitySelectListString != nil)
            entitySelectList = [[NSString alloc] initWithString:entitySelectListString];
        notifyQueue =  [[NSString alloc] initWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
        cellDetailText.text = [entity valueForKey:@"Description"];
        cellEffectText.text = [entity valueForKey:@"Effectiveness"];
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(handleNotification:)
         name:notifyQueue
         object:nil];
         updateButton.hidden = NO;
        cellDetailText.textColor = [UIColor blackColor];
        
    }
    else
    {
        cellDetailText.text = @"Add control";
        updateButton.hidden = YES;
        cellDetailText.textColor = [UIColor lightGrayColor];
        
    }
     [updateButton addTarget:self action:@selector(UpdateButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)handleNotification:(NSNotification *)pNotification
{
     cellEffectText.text = [entity valueForKey:@"Effectiveness"];
    [parentController dismissModalViewControllerAnimated:YES];
    
}


-(IBAction)UpdateButtonPressed:(id)sender
{

    if (entity == nil)
        return;
    
    
    
    UIButton *button = (UIButton*) sender;
    SelectFixedItemList* popoverContent = [[SelectFixedItemList alloc] initWithNibName:@"SelectFixedItemList" bundle:nil];
    popoverContent.label = @"Effectiveness";
    [popoverContent setupData:entity entityKeyString:entityKey entityselectListType:0 entitySelectListString:entitySelectList parentQueue:notifyQueue ];
    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.contentSizeForViewInPopover =
    CGSizeMake(500, 500);
    UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
    toolsNavController.view.frame = CGRectMake(0.0, -10.0, 320.0, 100);
      toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
      [parentController presentModalViewController:toolsNavController animated:YES];
    
    //release the popover content
      
    
    
    
    
}
@end
