//
//  EventViewControllerEvent.m
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerArea.h"
#import "SingleEditCell.h"
///Table Cell Indexes and Identifiers
#define DETAILS 0
#define DETAIL_SECTION_LINE1_ROW 0
#define DETAIL_SECTION_LINE1_TAG 10
#define DETAIL_SECTION_LINE1_PLACEHOLDER @"Name"
#define DETAIL_SECTION_LINE2_ROW 1
#define DETAIL_SECTION_LINE2_TAG 11
#define DETAIL_SECTION_LINE2_PLACEHOLDER @"Description (Optional)"
#define DETAIL_SECTION_LINE3_ROW 2
#define DETAIL_SECTION_LINE3_TAG 12
#define DETAIL_SECTION_LINE3_PLACEHOLDER @"Contact Name"
#define DETAIL_SECTION_LINE4_ROW 3
#define DETAIL_SECTION_LINE4_TAG 13
#define DETAIL_SECTION_LINE4_PLACEHOLDER @"Email Address"
#define DETAIL_SECTION_LINE5_ROW 4
#define DETAIL_SECTION_LINE5_TAG 14
#define DETAIL_SECTION_LINE5_PLACEHOLDER @"Phone Number"
#define DETAIL_SECTION_LINE6_ROW 5
#define DETAIL_SECTION_LINE6_TAG 15
#define DETAIL_SECTION_LINE7_PLACEHOLDER @"Description (Optional)"



@interface EventViewControllerArea ()

@end

@implementation EventViewControllerArea
@synthesize _tableView;
@synthesize keyboardSize;
@synthesize nameField;
@synthesize currentTextField;
@synthesize addressSection;
@synthesize contactNameField;
@synthesize descriptionField;
@synthesize addressField;
@synthesize phoneNumberField;
@synthesize footerView;
-(void)setupData:(GovernmentArea*) entityObject
{
    area = entityObject;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
     NSString *navBar;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        navBar = @"ipad_img_blue-header.png";
    }
    else {
        navBar = @"iphone_img_blueheader.png";
    }
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:navBar];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;

    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonImage = [UIImage imageNamed:@"but_edit_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
   // self.navigationItem.RightBarButtonItem = rightButtonItem;
    
    
    
    Items = [[NSMutableArray alloc] init];
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav1.png",delegate.imagePrefix];
    
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    [hv addSubview:image];
    //  hv.backgroundColor = [UIColor whiteColor];
    _tableView.tableHeaderView = hv;
    if (footerView != nil)
        _tableView.tableFooterView = footerView;


}
- (void)EditMode {
	if (_tableView.isEditing == YES)
	{
		UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage* buttonImage = [UIImage imageNamed:@"but_edit_off.png"];
        [backButton setImage:buttonImage forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
        backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
        self.navigationItem.RightBarButtonItem = rightButtonItem;
		[self._tableView setEditing:NO animated:YES];
	
	}
	
	else
	{
		UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage* buttonImage = [UIImage imageNamed:@"but_done_off.png"];
        [backButton setImage:buttonImage forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
        backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
        self.navigationItem.RightBarButtonItem = rightButtonItem;
        [self._tableView setEditing:YES animated:YES];

		
	}
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"EventViewControllerArea"
     object:nil];

}

- (void) didTapBackButton:(id)sender {
    
    
    
    
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)handleNotification:(NSNotification *)pNotification
{
 
    [self dismissModalViewControllerAnimated:YES];
    [self._tableView reloadData];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
- (void) viewWillAppear:(BOOL)animated
{
    [_tableView reloadData];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}
#pragma mark -
#pragma mark Table view data source
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)] ;
	tableView.sectionHeaderHeight = headerView.frame.size.height;
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, headerView.frame.size.width - 20, 22)] ;
	label.text = [self tableView:tableView titleForHeaderInSection:section];
	label.font = [UIFont boldSystemFontOfSize:16.0];
	label.shadowOffset = CGSizeMake(0, 1);
	label.shadowColor = [UIColor blackColor];
	label.backgroundColor = [UIColor clearColor];
    
	label.textColor = [UIColor whiteColor];
    
	[headerView addSubview:label];
	return headerView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    [Items removeAllObjects];
    NSSortDescriptor* descriptor;
    descriptor = [[NSSortDescriptor alloc] initWithKey:@"Name" ascending:YES];
    [Items addObjectsFromArray:[[area.Sites allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]]];
    
      [ Items insertObject:@"Add Area" atIndex:0];   //add temp 
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 1)
       return [Items count];
    else return 1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    NSUInteger section = [indexPath section];
    NSUInteger sectionRow = (section * 10) + row;
    static NSString *CellIdentifier = @"AreaCell";
    UITableViewCell *tblcell;
    if ([indexPath section] == 1)
    {
        tblcell = [tableView dequeueReusableCellWithIdentifier:@"AREA"];
        if (tblcell == nil) {
            tblcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
        }
         [self configureCell:tblcell atIndexPath:indexPath];
    }
    else
    {
        if (sectionRow == DETAILS)
        {
              tblcell = [tableView dequeueReusableCellWithIdentifier:@"AREA"];
            if (tblcell == nil) {
                tblcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
            }
            tblcell.textLabel.text = @"Details";
            tblcell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
        }
        else    if (sectionRow == DETAIL_SECTION_LINE2_ROW)
        {
            SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SingleEditCell"];
            if (cell == nil) {
                cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
                
            }
            
            [cell setupData:area entityKeyString:@"Description" entityLabelString:@"Description" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerArea"];
            cell.parentController = self;
              tblcell = cell;
            
        }
        else    if (sectionRow == DETAIL_SECTION_LINE3_ROW)
        {
            SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SingleEditCell"];
            if (cell == nil) {
                cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
                
            }
            
            [cell setupData:area entityKeyString:@"ContactName" entityLabelString:@"Contact Name" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerArea"];
            cell.parentController = self;
              tblcell = cell;
            
        }
        else    if (sectionRow == DETAIL_SECTION_LINE4_ROW)
        {
            SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SingleEditCell"];
            if (cell == nil) {
                cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
                
            }
            
            [cell setupData:area entityKeyString:@"Address" entityLabelString:@"Email Address" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerArea"];
            cell.parentController = self;
              tblcell = cell;
            
        }
        else    if (sectionRow == DETAIL_SECTION_LINE5_ROW)
        {
            SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
                
            }
            
            [cell setupData:area entityKeyString:@"PhoneNumber" entityLabelString:@"Phone Number" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerArea"];
            cell.parentController = self;
              tblcell = cell;
            
        }
       
    }
    // Configure the cell.
   tblcell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return tblcell;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *titleString =@"";
	NSSortDescriptor * descriptor;
	switch ( section)
    {
        case 0:
            titleString =  @"Venue";
            break;
        case 1:
            titleString =  @"Areas";
            break;

    }
    return titleString;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    if ([indexPath section] == 1)
    {
        NSObject *managedObject = [Items objectAtIndex:[indexPath row]];
        if ([managedObject isKindOfClass:[NSString class]])
        {
            NSString* stringObject = (NSString*)managedObject;
            cell.textLabel.text = stringObject;
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.accessoryType =    UITableViewCellAccessoryNone;
        }
        else
        {
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.text = [[managedObject valueForKey:@"Name"] description];
                cell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else
    {
        NSUInteger row = [indexPath row];
        NSUInteger section = [indexPath section];
        NSUInteger sectionRow = (section * 10) + row;
    
        if (cell != nil) {
                UITextField *textField  = [[UITextField alloc] initWithFrame:CGRectMake(10,0, 280  , 44)];
                textField.clearButtonMode = UITextFieldViewModeAlways;
                //	[textField addTarget:self action:@selector(textFieldDoneEditing:) forControlEvents:UIControlEventEditingDidEndOnExit];
                textField.textAlignment = UITextAlignmentLeft;
                textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                textField.keyboardType = UIKeyboardTypeDefault;
                textField.returnKeyType = UIReturnKeyNext;
                
                if (sectionRow == DETAIL_SECTION_LINE1_ROW )
                {
                    if (area.Name != nil)
                    {
                        cell.textLabel.text = area.Name ;
                        cell.textLabel.textColor = [UIColor blackColor];
                        cell.detailTextLabel.text = @"Name";
                    }
                    
                    else
                    {
                        cell.textLabel.text = @"Name";
                        cell.textLabel.textColor = [UIColor lightGrayColor];
                        cell.detailTextLabel.text = nil;
                    }
                    
                }
                else if (sectionRow == DETAIL_SECTION_LINE2_ROW )
                {
                    if (area.Description != nil)
                    {
                        cell.textLabel.text = area.Description ;
                        cell.textLabel.textColor = [UIColor blackColor];
                        cell.detailTextLabel.text = @"Description (Optional)";
                    }
                    
                    else
                    {
                        cell.textLabel.text = @"Description (Optional)";
                        cell.textLabel.textColor = [UIColor lightGrayColor];
                        cell.detailTextLabel.text = nil;
                    }

                  
                }
                else if (sectionRow == DETAIL_SECTION_LINE3_ROW )
                {
                    if (area.ContactName != nil)
                    {
                        cell.textLabel.text = area.ContactName ;
                        cell.textLabel.textColor = [UIColor blackColor];
                        cell.detailTextLabel.text = @"Contact Name";
                    }
                    
                    else
                    {
                        cell.textLabel.text = @"Contact Name";
                        cell.textLabel.textColor = [UIColor lightGrayColor];
                        cell.detailTextLabel.text = nil;
                    }

                                }
                else if (sectionRow == DETAIL_SECTION_LINE4_ROW )
                {
                    if (area.Address != nil)
                    {
                        cell.textLabel.text = area.Address ;
                        cell.textLabel.textColor = [UIColor blackColor];
                        cell.detailTextLabel.text = @"Email Address";
                    }
                    
                    else
                    {
                        cell.textLabel.text = @"Email Address";
                        cell.textLabel.textColor = [UIColor lightGrayColor];
                        cell.detailTextLabel.text = nil;
                    }
                }
                else if (sectionRow == DETAIL_SECTION_LINE5_ROW )
                {
                    if (area.PhoneNumber != nil)
                    {
                        cell.textLabel.text = area.PhoneNumber ;
                        cell.textLabel.textColor = [UIColor blackColor];
                        cell.detailTextLabel.text = @"Phone Number";
                    }
                    
                    else
                    {
                        cell.textLabel.text = @"Phone Number";
                        cell.textLabel.textColor = [UIColor lightGrayColor];
                        cell.detailTextLabel.text = nil;
                    }
                }
                [textField setDelegate:self];
                [cell.contentView addSubview:textField];
            
            
        }
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // Configure the cell...
    }
}
- (BOOL)fieldIsInHeader:(UITextField *)textField {
    
    if ((textField.tag == HEADER_FIELD_TAG_1) || (textField.tag == HEADER_FIELD_TAG_2)) {
        return YES;
    }
    return NO;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    currentTextField = textField;
    [self scrollIntoView:textField];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 15)
        textField.tag = 9;
    [[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
    [self scrollIntoView:textField];
    return YES;
}
- (void)scrollIntoView:(UITextField *)textField {
    if ((keyboardSize.height != 0) && (![self fieldIsInHeader:textField])) {
        
        [self._tableView scrollToRowAtIndexPath:[self indexPathForField:textField] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}
///
/// Only use this function for rows in the table SECTIONS not the HEADER
///
- (NSIndexPath *)indexPathForField:(UITextField *)theTextField {
    
    //set a default value
    NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:addressSection];
    
    if (theTextField.tag == DETAIL_SECTION_LINE1_TAG) {
        currentIndexPath = [NSIndexPath indexPathForRow:DETAIL_SECTION_LINE1_ROW inSection:addressSection];
    } else if (theTextField.tag == DETAIL_SECTION_LINE2_TAG) {
        currentIndexPath = [NSIndexPath indexPathForRow:DETAIL_SECTION_LINE2_ROW inSection:addressSection];
    } else if (theTextField.tag == DETAIL_SECTION_LINE3_TAG) {
        currentIndexPath = [NSIndexPath indexPathForRow:DETAIL_SECTION_LINE3_ROW inSection:addressSection];
    } else if (theTextField.tag == DETAIL_SECTION_LINE4_TAG) {
        currentIndexPath = [NSIndexPath indexPathForRow:DETAIL_SECTION_LINE4_ROW inSection:addressSection];
    } else if (theTextField.tag == DETAIL_SECTION_LINE5_TAG) {
        currentIndexPath = [NSIndexPath indexPathForRow:DETAIL_SECTION_LINE5_ROW inSection:addressSection];
    }
    
    return currentIndexPath;
}

#pragma mark - Keyboard Delegate

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)deregisterFortKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWillShow:(NSNotification*)notification
{
    
    NSDictionary* info = [notification userInfo];
    keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
    //
    [UIView animateWithDuration:[[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]
                          delay:0
                        options:[[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]
                     animations:^{
                         //                         UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
                         //                         self._tableView.contentInset = contentInsets;
                         //                         self._tableView.scrollIndicatorInsets = contentInsets;
                         CGRect frame = self._tableView.frame;
                         frame.size.height -= keyboardSize.height;
                         self._tableView.frame = frame;
                     }
                     completion:^(BOOL done){
                         if ([self fieldIsInHeader:currentTextField]) {
                             return;
                         } else {
                             [self scrollIntoView:currentTextField];
                         }
                         
                     }
     ];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    //Clear the keyboard size variable
    keyboardSize.height = 0;
    keyboardSize.width = 0;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self._tableView.contentInset = contentInsets;
    self._tableView.scrollIndicatorInsets = contentInsets;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        return UITableViewCellEditingStyleDelete;
    }
    else
        return UITableViewCellEditingStyleNone;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        return YES;
    }
    else
        return NO;
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        [delegate.managedObjectContext deleteObject:managedObject];
        
        [delegate saveContext];
        [tableView reloadData];
	}
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([indexPath section] == 0)
    {
        [self performSegueWithIdentifier:@"EventDetails" sender: nil];
        return;
    }
    
    if ([managedObject isKindOfClass:[LocalSite class]]) //It is a risk object so update data
	{
        [self performSegueWithIdentifier:@"Assessments" sender: nil];
    }
	else
	{
         [self performSegueWithIdentifier:@"AreaDetail" sender: nil];
    }
    
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"EventDetails"] == YES)
    {
        EventViewControllerEventDetail *ev = (EventViewControllerEventDetail *)[segue destinationViewController];
        
        [ev setupData:area];
        return;
    }
    NSManagedObject *item = [Items objectAtIndex:[self._tableView.indexPathForSelectedRow row]];
    EventViewControllerAssessment *ev = (EventViewControllerAssessment *)[segue destinationViewController];
    if ([item isKindOfClass:[NSString class]]) //It is
        [ev setupData:area];
    else
         [ev setupData:item];
    
}
@end
