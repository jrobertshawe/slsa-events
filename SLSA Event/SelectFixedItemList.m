//
//  SelectFixedItemList.m
//  SLSA Risk Assessment
//
//  Created by Julian on 6/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "SelectFixedItemList.h"
#import "EventAppDelegate.h"
@implementation SelectFixedItemList
@synthesize               label; 

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(IBAction)DoneButtonPressed
{
     [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityselectListType:(int) entityselectListType  entitySelectListString:(NSString*) entitySelectListString  parentQueue:(NSString*) parentQueue
{
    entity = entityObject;
    
    entityKey = [[NSString alloc] initWithString:entityKeyString];
    if (entitySelectListString != nil)
        entitySelectList = [[NSString alloc] initWithString:entitySelectListString];
    selectListType = entityselectListType;
    notifyQueue = [[NSString alloc] initWithString:parentQueue];

}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    items = [[NSMutableArray alloc] init];
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DoneButtonPressed)];
   // self.navigationItem.rightBarButtonItem = rightBarButtonItem;

    self.navigationItem.title = label;
  
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iphone_img_background.png"]];
    [tempImageView setFrame:self.tableView.frame];
    
    self.tableView.backgroundView = tempImageView;

    [items addObjectsFromArray:[delegate GetDataArray:entitySelectList]];
    NSString *navBar;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        navBar = @"ipad_img_blue-header.png";
    }
    else {
        navBar = @"iphone_img_blueheader.png";
    }
    
    UIImage *image = [UIImage imageNamed:navBar];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
   
    return [items count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

            return 68.0;

    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     
    
    static NSString *CellIdentifier = @"Cell";
     NSString* currentValue =   [entity valueForKey:entityKey];   
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    NSUInteger row = [indexPath row];
    NSManagedObject* item = [items objectAtIndex:row];
    if ([item isKindOfClass:[NSString class]])
    {
        cell.textLabel.text  = (NSString*) item;
    }
    else
    {
        cell.textLabel.text =  [item valueForKey:@"Description"];
    }
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];

    NSUInteger row = [indexPath row];
    NSManagedObject* item = [items objectAtIndex:row];
  
    
    [entity setValue:[item valueForKey:@"Description"] forKey:entityKey];
    
    
       [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];

    [delegate saveContext];

}

@end
