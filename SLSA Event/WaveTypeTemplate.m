//
//  WaveTypeTemplate.m
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "WaveTypeTemplate.h"


@implementation WaveTypeTemplate

@dynamic Order;
@dynamic Description;

@end
