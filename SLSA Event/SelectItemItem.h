//
//  SelectItemItem.h
//  SLSA Risk Assessment
//
//  Created by Julian on 20/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditSingleLineText.h"
#define SELECTLISTCONTACT 0
#define SELECTLISTTEMPLATE 1
#define SELECTLISTFIXED 2

@interface SelectItemItem : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate >
{
    NSManagedObject*            selectedObject;
    NSString*               	entityKey;
    NSString*                   selectList;
    IBOutlet UITableView*       tableview;
    IBOutlet UIBarButtonItem*   rightButton;
    NSMutableArray              *items;
    IBOutlet UIView             *selectView;
    IBOutlet UIView             *contactDetailsView;
    IBOutlet UITextField        *name;
    IBOutlet UITextField        *company;
    IBOutlet UITextField        *phone;
    IBOutlet UITextField        *position;
    IBOutlet UITextField        *address;
     UIPopoverController *popoverController;  
        NSString*               label; 
      NSString*                 notifyQueue; 
    int                        selectListType;
}
@property  int                        selectListType;
@property (nonatomic,retain)  UITextField                           *name;
@property (nonatomic,retain)  UITextField                           *company;
@property (nonatomic,retain)  UITextField                           *phone;
@property (nonatomic,retain)  UITextField                           *position;
@property (nonatomic,retain)  UITextField                           *address;
@property (nonatomic,retain)  UIView                           *selectView;
@property (nonatomic,retain)  UIView                           *contactDetailsView;
@property (nonatomic,retain)  UITableView                           *tableview;
@property (nonatomic,retain)  UIBarButtonItem                              *rightButton;
 @property (nonatomic,retain)     NSString*               label; 
 @property (nonatomic,retain)     NSString*               notifyQueue; 
-(IBAction)DoneButtonPressed;
-(IBAction)EditButtonPressed;
-(IBAction)ContactDoneButtonPressed:(id)sender;
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityselectListType:(int) entityselectListType  entitySelectList:(NSString*) entitySelectList  parentQueue:(NSString*) parentQueue;
@property (nonatomic,retain)  IBOutlet UIViewController *parentController;
@property CGSize keyboardSize;

@property (strong, nonatomic) UITextField *currentTextField;
- (void)registerForKeyboardNotifications;
- (BOOL)fieldIsInHeader:(UITextField *)textField;
@end
