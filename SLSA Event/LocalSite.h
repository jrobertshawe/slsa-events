//
//  LocalSite.h
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Assessment, GovernmentArea;

@interface LocalSite : NSManagedObject

@property (nonatomic, retain) NSNumber * Longitude;
@property (nonatomic, retain) NSNumber * Latitute;
@property (nonatomic, retain) NSString * Name;
@property (nonatomic, retain) NSString * BeachKey;
@property (nonatomic, retain) NSSet *Assessments;
@property (nonatomic, retain) GovernmentArea *GovernmentArea;
@end

@interface LocalSite (CoreDataGeneratedAccessors)

- (void)addAssessmentsObject:(Assessment *)value;
- (void)removeAssessmentsObject:(Assessment *)value;
- (void)addAssessments:(NSSet *)values;
- (void)removeAssessments:(NSSet *)values;

@end
