//
//  EffectivenessTemplates.h
//  SLSARiskAssessment
//
//  Created by Julian on 16/03/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EffectivenessTemplates : UITableViewController {
    NSArray* itemArray;   
    NSManagedObject *selectedObject;
    NSString *recordkey;
}
-(void)setupData:(NSString*) entityObject entityKeyString:(NSString*) entityKeyString;
@end