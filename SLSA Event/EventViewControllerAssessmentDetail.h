//
//  EventViewControllerAssessmentDetail.h
//  SLSA Event
//
//  Created by Julian robertshawe on 20/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"
#define HEADER_FIELD_TAG_1 0
#define HEADER_FIELD_TAG_2 1


///Table Cell Indexes and Identifiers
#define DETAILS 0
#define CURRENTWEATHER 1
#define CURRENTSURF 3
#define PREDICTIONWEATHER 2
#define PREDICTIONSURF 4
#define EVENTS 5

#define DETAIL_SECTION_LINE1_ROW 0
#define DETAIL_SECTION_LINE1_TAG 10
#define DETAIL_SECTION_LINE1_PLACEHOLDER @"Name"
#define DETAIL_SECTION_LINE2_ROW 1
#define DETAIL_SECTION_LINE2_TAG 11
#define DETAIL_SECTION_LINE2_PLACEHOLDER @"Description (Optional)"
#define DETAIL_SECTION_LINE3_ROW 2
#define DETAIL_SECTION_LINE3_TAG 12
#define DETAIL_SECTION_LINE3_PLACEHOLDER @"Contact Name"
#define DETAIL_SECTION_LINE4_ROW 3
#define DETAIL_SECTION_LINE4_TAG 13
#define DETAIL_SECTION_LINE4_PLACEHOLDER @"Email Address"
#define DETAIL_SECTION_LINE5_ROW 4
#define DETAIL_SECTION_LINE5_TAG 14
#define DETAIL_SECTION_LINE5_PLACEHOLDER @"Phone Number"
#define DETAIL_SECTION_LINE6_ROW 5
#define DETAIL_SECTION_LINE6_TAG 15
#define DETAIL_SECTION_LINE7_PLACEHOLDER @"Description (Optional)"

@interface EventViewControllerAssessmentDetail : UIViewController
{
	IBOutlet UITextField			*conditionField;
    IBOutlet UITextField		*temperatureField;
	IBOutlet UITextField			*descriptionField;
	NSString *objectString;
    bool newRecord;
    NSMutableArray					*eventItems;
    GovernmentArea* governmentArea;
    LocalSite* localSiteObject;
    
    Assessment* selectedObject;
}
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (nonatomic, retain) GovernmentArea* governmentArea;
@property (nonatomic, retain) IBOutlet UITextField *conditionField;
@property (nonatomic, retain) IBOutlet UITextField *temperatureField;
@property (nonatomic, retain) IBOutlet UITextField *descriptionField;
- (void)cancelRequest;
- (void)saveObject;
-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite;
-(void)handleNotification:(NSNotification *)pNotification;

@end