//
//  Hazard.h
//  SLSA Event
//
//  Created by Julian robertshawe on 24/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Assessment, Control, Photo, Risk, RiskGroup;

@interface Hazard : NSManagedObject

@property (nonatomic, retain) NSString * RiskScoreConsequence;
@property (nonatomic, retain) NSString * Description;
@property (nonatomic, retain) NSString * RiskScore;
@property (nonatomic, retain) NSString * Latitute;
@property (nonatomic, retain) NSString * ResidualRiskScoreConsequence;
@property (nonatomic, retain) NSString * ResidualRiskScoreLikelihood;
@property (nonatomic, retain) NSString * ResidualRiskScore;
@property (nonatomic, retain) NSString * Longitude;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * RiskScoreLikelihood;
@property (nonatomic, retain) NSSet *RiskGroups;
@property (nonatomic, retain) NSSet *ExistingControls;
@property (nonatomic, retain) NSSet *Risks;
@property (nonatomic, retain) NSSet *Photos;
@property (nonatomic, retain) Assessment *Assessment;
@property (nonatomic, retain) NSSet *RecommendedControls;
@end

@interface Hazard (CoreDataGeneratedAccessors)

- (void)addRiskGroupsObject:(RiskGroup *)value;
- (void)removeRiskGroupsObject:(RiskGroup *)value;
- (void)addRiskGroups:(NSSet *)values;
- (void)removeRiskGroups:(NSSet *)values;

- (void)addExistingControlsObject:(Control *)value;
- (void)removeExistingControlsObject:(Control *)value;
- (void)addExistingControls:(NSSet *)values;
- (void)removeExistingControls:(NSSet *)values;

- (void)addRisksObject:(Risk *)value;
- (void)removeRisksObject:(Risk *)value;
- (void)addRisks:(NSSet *)values;
- (void)removeRisks:(NSSet *)values;

- (void)addPhotosObject:(Photo *)value;
- (void)removePhotosObject:(Photo *)value;
- (void)addPhotos:(NSSet *)values;
- (void)removePhotos:(NSSet *)values;

- (void)addRecommendedControlsObject:(Control *)value;
- (void)removeRecommendedControlsObject:(Control *)value;
- (void)addRecommendedControls:(NSSet *)values;
- (void)removeRecommendedControls:(NSSet *)values;

@end
