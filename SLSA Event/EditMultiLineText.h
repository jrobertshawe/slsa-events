//
//  EditMultiLineText.h
//  SLSA Risk Assessment
//
//  Created by Julian on 21/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface EditMultiLineText : UIViewController {
    NSString*       entityString; 
    
    NSString*               entityKey;
    NSString*               label; 
    NSString*               notifyQueue;  
   IBOutlet UITextView *  textField;
    
}
@property (nonatomic,retain)  IBOutlet UITextView                  *textField;

-(IBAction)DoneButtonPressed;
-(IBAction)CancelButtonPressed;
-(void)setupData:(NSString*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString notifyQueueString:(NSString*) notifyQueueString;
@end
