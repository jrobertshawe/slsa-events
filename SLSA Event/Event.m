//
//  Event.m
//  SLSA Event
//
//  Created by Julian robertshawe on 18/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "Event.h"
#import "Assessment.h"


@implementation Event

@dynamic name;
@dynamic Assessment;

@end
