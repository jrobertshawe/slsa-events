//
//  EventViewControllerAssessmentMenu.m
//  SLSA Event
//
//  Created by Julian robertshawe on 3/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerAssessmentMenu.h"

@interface EventViewControllerAssessmentMenu ()

@end

@implementation EventViewControllerAssessmentMenu
@synthesize weatherGreenTick;
@synthesize hazardsGreenTick;
@synthesize activityGreenTick;
@synthesize surfGreenTick;
@synthesize footerView;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite
{
    assessment = entityObject;
    localSiteObject = localSite;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonImage = [UIImage imageNamed:@"but_report_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(CreateReport:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.rightBarButtonItem = backButtonItem;

    self.navigationItem.rightBarButtonItem.enabled = NO;
   
  
}
- (void) didTapBackButton:(id)sender {
    
    
    
    
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void) viewWillAppear:(BOOL)animated
{
     self.navigationItem.rightBarButtonItem.enabled = NO;
    bool weather = NO;
    bool surf = false;
    bool activity = NO;
    bool hazard = NO;
    
    weatherGreenTick.image = [UIImage imageNamed:@"SLSA Selections Unselected.png"];
    activityGreenTick.image = [UIImage imageNamed:@"SLSA Selections Unselected.png"];
    hazardsGreenTick.image = [UIImage imageNamed:@"SLSA Selections Unselected.png"];
    surfGreenTick.image = [UIImage imageNamed:@"SLSA Selections Unselected.png"];
    if (assessment.weatherChecked != nil)
        if ([assessment.weatherChecked boolValue] == YES)
            if (weatherGreenTick != nil)
            {
                weatherGreenTick.image = [UIImage imageNamed:@"SLSA Selections Selected.png"];
                weather = YES;
            }
    if (assessment.surfChecked != nil)
        if ([assessment.surfChecked boolValue] == YES)
            if (surfGreenTick != nil)
                surfGreenTick.image = [UIImage imageNamed:@"SLSA Selections Selected.png"];
    if (assessment.activityChecked != nil)
        if ([assessment.activityChecked boolValue] == YES)
            if (activityGreenTick != nil)
            {
                activityGreenTick.image = [UIImage imageNamed:@"SLSA Selections Selected.png"];
                activity = YES;
            }
    if (assessment.hazardscheck != nil)
      if ([assessment.hazardscheck boolValue] == YES)
        if (hazardsGreenTick != nil)
        {
            hazardsGreenTick.image = [UIImage imageNamed:@"SLSA Selections Selected.png"];
            hazard = YES;
        }
    if ( weather== YES && activity == YES && hazard == YES)
        self.navigationItem.rightBarButtonItem.enabled = YES;
    
}
- (IBAction)CreateReport:(id)sender 
{
	    GenerateReport*  generateReport = [[GenerateReport alloc] initWithNibName: @"GenerateReport" bundle:nil];
		generateReport.assessment = assessment;
		[[self navigationController] pushViewController:generateReport animated:YES];
	
	
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iphone_img_background.png"]];
    [tableView setBackgroundView:bg];
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav3.png",delegate.imagePrefix];
    
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    
    [hv addSubview:image];
    //  hv.backgroundColor = [UIColor whiteColor];
    tableView.tableHeaderView = hv;
    if (footerView != nil)
        tableView.tableFooterView = footerView;
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    if (section == 0)
        return 1;
    else
      return 4;
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AssessmentDetails"] == YES)
    {
        EventViewControllerAssessmentDetail *ev = (EventViewControllerAssessmentDetail *)[segue destinationViewController];
        
        [ev setupData:assessment localSite:localSiteObject];
    }
    else if ([segue.identifier isEqualToString:@"Weather"] == YES)
        
    {
        EventViewControllerWeather *ev = (EventViewControllerWeather *)[segue destinationViewController];
        
        [ev setupData:assessment localSite:localSiteObject];
        
    }
    else if ([segue.identifier isEqualToString:@"SurfDetails"] == YES)
        
    {
        EventViewControllerSurfDetails *ev = (EventViewControllerSurfDetails *)[segue destinationViewController];
        
        [ev setupData:assessment localSite:localSiteObject];
        
    }
    else if ([segue.identifier isEqualToString:@"Events"] == YES)
        
    {
        EventViewControllerEvents *ev = (EventViewControllerEvents *)[segue destinationViewController];
        
        [ev setupData:assessment localSite:localSiteObject];
        
    }
    else if ([segue.identifier isEqualToString:@"Hazard"] == YES)
        
    {
        EventViewControllerHazards *ev = (EventViewControllerHazards *)[segue destinationViewController];
        
        [ev setupData:assessment localSite:localSiteObject];
        
    }
}

@end
