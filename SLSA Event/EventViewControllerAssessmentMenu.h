//
//  EventViewControllerAssessmentMenu.h
//  SLSA Event
//
//  Created by Julian robertshawe on 3/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"
#import "EventViewControllerAssessmentDetail.h"
#import "EventViewControllerWeather.h"
#import "EventViewControllerSurfDetails.h"
#import "EventViewControllerEvents.h"
#import "EventViewControllerHazards.h"
#import "GenerateReport.h"
@interface EventViewControllerAssessmentMenu : UITableViewController
{
    Assessment* assessment;
     LocalSite* localSiteObject;
}
@property (nonatomic, retain) IBOutlet UIView		*footerView;
@property (nonatomic, retain) IBOutlet UIImageView *weatherGreenTick;
@property (nonatomic, retain) IBOutlet UIImageView *activityGreenTick;
@property (nonatomic, retain) IBOutlet UIImageView *surfGreenTick;
@property (nonatomic, retain) IBOutlet UIImageView *hazardsGreenTick;
-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite;
@end
