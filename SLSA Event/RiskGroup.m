//
//  RiskGroup.m
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "RiskGroup.h"
#import "Hazard.h"


@implementation RiskGroup

@dynamic Description;
@dynamic Hazard;

@end
