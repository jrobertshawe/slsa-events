//
//  SingleImageCell.h
//  SLSA Risk Assessment
//
//  Created by Julian on 21/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Hazard.h"

@interface SingleImageCell : UITableViewCell {
      UIPopoverController *popoverController;
    UIViewController *viewController;
    NSManagedObject*       entity;
    
    NSString*               entityKey;
    IBOutlet UIImageView             *mapThumbnail;
        IBOutlet UILabel *celllabelText;

    IBOutlet UIButton *resend;
bool isCameraImage;
     bool isRiskImage;
     bool isImageList;
    NSString *queueName;
  

    IBOutlet UIButton *updateButton;
    UIViewController* parentController;
    
}
@property (nonatomic,retain)  IBOutlet UIViewController *parentController;
@property (nonatomic,retain)  IBOutlet UIButton *updateButton;
@property (nonatomic,retain)  UILabel						*celllabelText;
@property (nonatomic,retain) IBOutlet UIButton						*resend;
@property (nonatomic,retain)  NSString						*entityKey;
@property (nonatomic,retain)  UIImageView						*mapThumbnail;

-(void)handleNotification:(NSNotification *)pNotification;
-(void)updateImage;
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString cameraImage:(bool)cameraImage riskImage:(bool)riskImage imageList:(bool)imageList;
-(IBAction)UpdateButtonPressed:(id)sender;
@end
