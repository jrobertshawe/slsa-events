//
//  EventViewControllerAssessmentDetail.m
//  SLSA Event
//
//  Created by Julian robertshawe on 20/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerAssessmentDetail.h"
#import "events.h"
#import "AssessmentTemplates.h"
#import "SingleEditCell.h"	
#import "SingleLineListItemCell.h"
@interface EventViewControllerAssessmentDetail ()

@end

@implementation EventViewControllerAssessmentDetail

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad {
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
		
   
    
    
   // self.navigationItem.title = @"Add Assessment";
	if (selectedObject != nil)
	{
        
		if ([selectedObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
		{
	//		self.navigationItem.title = @"Update Assessment";
			newRecord = NO;
		}
		else
		{
           Assessment* assessmentObject = (Assessment *)[NSEntityDescription insertNewObjectForEntityForName:@"Assessment" inManagedObjectContext:[delegate managedObjectContext]];
            [assessmentObject setValue:[NSDate date] forKey:@"AssessmentDate"];

			newRecord = YES;
            [assessmentObject setValue:governmentArea.ContactName forKey:@"Assessor"];
            
		}
	}
	else
	{
        selectedObject = (Assessment *)[NSEntityDescription insertNewObjectForEntityForName:@"Assessment" inManagedObjectContext:[delegate managedObjectContext]];
        [selectedObject setValue:[NSDate date] forKey:@"AssessmentDate"];

        [selectedObject setValue:governmentArea.ContactName forKey:@"Assessor"];
		newRecord = YES;
	}
    [delegate saveContext];
    [self._tableView setEditing:YES animated:YES];
    [    self.navigationItem.rightBarButtonItem setEnabled:NO];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"refreshView"
     object:nil];
}
-(void)handleNotification:(NSNotification *)pNotification
{
    
    [self._tableView reloadData];
}
- (void) didTapBackButton:(id)sender {
    
    if ([[localSiteObject.Assessments allObjects] containsObject:selectedObject] == NO)
        [localSiteObject addAssessmentsObject:selectedObject];
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
  
    [delegate saveContext];
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite
{
    
    localSiteObject = localSite;
    if ([entityObject isKindOfClass:[Assessment class]])
    {
        selectedObject = entityObject;
    }
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    
    return NO;
}

-(void) checkValid
{
     
    [    self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    if (selectedObject.Description == nil)
        return;
    
    if (selectedObject.Assessor == nil)
        return;
    if (selectedObject.CurrentCloud == nil)
        return;
    //   if (item.CurrentSwellSize == nil)
    //       return;
    //    if (item.CurrentSwellTime == nil)
    //       return;
    //   if (item.CurrentTide == nil)
    //     return;
    //  if (item.CurrentWaveType == nil)
    //      return;
    if (selectedObject.CurrentWeatherTemp == nil)
        return;
    if (selectedObject.CurrentWindDirection == nil)
        return;
    if (selectedObject.CurrentWindSpeed == nil)
        return;
    
    if (selectedObject.PredictionCloud == nil)
        return;
    //  if (item.PredictionSwellSize == nil)
    //       return;
    //   if (item.PredictionSwellTime == nil)
    //       return;
    //   if (item.PredictionLowTide == nil)
    //       return;
    //    if (item.PredictionHighTide == nil)
    //        return;
    //   if (item.PredictionWaveType == nil)
    //       return;
    if (selectedObject.PredictionWeatherTemp == nil)
        return;
    if (selectedObject.PredictionWindDirection == nil)
        return;
    if (selectedObject.PredictionWindSpeed == nil)
        return;
    
    
    if([selectedObject.Events count] == 0)
        return;
    
    [    self.navigationItem.rightBarButtonItem setEnabled:YES];
    
}
- (void)cancelRequest
{
   EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
     
	if (newRecord == YES)
		[[delegate managedObjectContext] deleteObject:selectedObject];
	[[self navigationController] popViewControllerAnimated:YES];
}
- (void)saveObject
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];

	
	if (selectedObject != nil)
	{
		if ([selectedObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
		{
            
			if (objectString != nil)
			{
                
				if ([selectedObject isKindOfClass:[LocalSite class]]) //It is a risk object so update data
				{
                    [localSiteObject addAssessmentsObject:selectedObject];
					
			
				}
				
			}
		}
	}
	[delegate saveContext];
	
	[[self navigationController] popViewControllerAnimated:YES];
	
	
	
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)] ;
	tableView.sectionHeaderHeight = headerView.frame.size.height;
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, headerView.frame.size.width - 20, 22)] ;
	label.text = [self tableView:tableView titleForHeaderInSection:section];
	label.font = [UIFont boldSystemFontOfSize:16.0];
	label.shadowOffset = CGSizeMake(0, 1);
	label.shadowColor = [UIColor blackColor];
	label.backgroundColor = [UIColor clearColor];
    
	label.textColor = [UIColor whiteColor];
    
	[headerView addSubview:label];
	return headerView;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath   ///what edit mode are we entering
{
	
	if (self._tableView.editing == YES)
	{
		
		NSUInteger row = [indexPath row];
		NSUInteger section = [indexPath section];
		if (section == EVENTS)
		{
			NSManagedObject *item = [eventItems objectAtIndex:row];
			if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
			{
				return UITableViewCellEditingStyleInsert;
				
			}
			else //else show delete option
			{
				return UITableViewCellEditingStyleDelete;
			}
			
		}
    }
    return UITableViewCellEditingStyleNone;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self._tableView reloadData];
    [self checkValid];
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav3.png",delegate.imagePrefix];
    
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    
    [hv addSubview:image];    //  hv.backgroundColor = [UIColor whiteColor];
    tableView.tableHeaderView = hv;
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int sectionRowCount;
	
    switch (section) {
		case DETAILS:
			sectionRowCount = 3;
			break;
		case EVENTS:
			sectionRowCount = [eventItems count];
			
			break;
        case CURRENTWEATHER:
    		sectionRowCount = 4;
	        
            break;
            
        case PREDICTIONWEATHER:
    		sectionRowCount = 4;
	        
            break;
        case CURRENTSURF:
    		sectionRowCount = 4;
	        
            break;
        case PREDICTIONSURF:
    		sectionRowCount = 5;
	        
            break;
            
		default:
			sectionRowCount = 0;
			break;
	}
	
    return sectionRowCount;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
  	NSString *headerTitle = @"";
	
    switch (section) {
        case DETAILS:
            headerTitle =  @"Details";
            
            break;
        case CURRENTWEATHER:
            headerTitle =  @"Current Weather";
            
            break;
            
        case PREDICTIONWEATHER:
            headerTitle =  @"Prediction Weather";
            
            break;
        case CURRENTSURF:
            headerTitle =  @"Current Surf";
            
            break;
        case PREDICTIONSURF:
            headerTitle =  @"Prediction Surf";
            
            break;
            
        case EVENTS:
            if (eventItems == nil)
                eventItems = [[NSMutableArray alloc] init];
            else
                [eventItems removeAllObjects];
            
            [eventItems addObjectsFromArray:[selectedObject.Events allObjects]];
            [ eventItems insertObject:@"Add Activity"  atIndex:0];    //add temp line to allow adding items
            headerTitle =  @"Activity";
            break;
        default:
            headerTitle = @"";
            break;
    }
    
    
    return headerTitle;
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {;
   
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
    static NSString *CellIdentifier = @"Cell";
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
    UITableViewCell *tblcell;
     static NSString *SingleLineListItemCellIdentifier = @"SingleLineListItemCell";
    if (sectionRow == 0)
    {
        SingleLineListItemCell *cell = [tableView dequeueReusableCellWithIdentifier:SingleLineListItemCellIdentifier];
        if (cell == nil) {
            cell = [[SingleLineListItemCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                 reuseIdentifier:SingleLineListItemCellIdentifier];
            
        }
        [cell setupData:selectedObject];
        tblcell = cell;
    }
    if (sectionRow == 1)
    {
        SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SingleEditCell"];
        if (cell == nil) {
            cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                         reuseIdentifier:CellIdentifier];
            
        }
        
        [cell setupData:selectedObject entityKeyString:@"Assessor" entityLabelString:@"Assessor" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerAssessmentDetail"];
        cell.parentController = self;
        tblcell = cell;
        
    }

    if (sectionRow == 2)
    {
        
        SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SingleEditCell"];
        if (cell == nil) {
            cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                         reuseIdentifier:CellIdentifier];
            
        }
        
        [cell setupData:selectedObject entityKeyString:@"Notes" entityLabelString:@"Notes" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerAssessmentDetail"];
        cell.parentController = self;
        tblcell = cell;

    }
       
    
    // Configure the cell...
   tblcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return tblcell;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	NSUInteger section = [indexPath section];
	if (section == EVENTS )  //if add items then indent
		return YES;
	
	return NO;
}
/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
		NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	if (section == EVENTS)
	{
		NSManagedObject *item = [eventItems objectAtIndex:row];
		if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
		{
            Events*  events = [[Events alloc] initWithNibName: @"Events" bundle:nil];
            [events setupData:selectedObject];
            
            [[self navigationController] pushViewController:events animated:YES];
          	
		}
		else //else show delete option
		{
            [selectedObject removeEventsObject:item];
            [self._tableView reloadData];
			;
		}
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
    WindDirectionTemplates*  windDirectionTemplates;
    WindSpeedTemplates*  windSpeedTemplates;
    SwellSizeTemplates* swellSizeTemplates;
    WaveTypeTemplates* waveTypeTemplates;
    TideTemplates* tideTemplates;
    SelectTime * selectTime;
    CloudCover * cloudCover;
    EnterValue*  enterValue;
    if (sectionRow  == 0)
	{
		AssessmentTemplates*  assessmentTemplates = [[AssessmentTemplates alloc] initWithNibName: @"AssessmentTemplates" bundle:nil];
        [assessmentTemplates setupData:selectedObject];
		
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:assessmentTemplates] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 300, 504);
        
        toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentModalViewController:toolsNavController animated:YES];
        
		
	}
    else if (sectionRow  == 1)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
        [enterValue setupData:selectedObject entityKeyString:@"Assessor"  entityLabel:@"Assessor"];
	    enterValue.promptText  = @"First name Last name";
        enterValue.titleText = @"Your Name";
        enterValue.editkeyboardtype = UIKeyboardTypeAlphabet;
        
		[[self navigationController] pushViewController:enterValue animated:YES];
	
		
	}
    else if (sectionRow  == 3)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
        [enterValue setupData:selectedObject entityKeyString:@"Notes" entityLabel:@"Notes"];
        enterValue.promptText  = @"";
        enterValue.titleText = @"Assessment Notes";
        enterValue.editkeyboardtype = UIKeyboardTypeAlphabet;
        enterValue.isNotes = YES;
		[[self navigationController] pushViewController:enterValue animated:YES];
	
		
	}
    else if (sectionRow  == 11)
	{
		windDirectionTemplates = [[WindDirectionTemplates alloc] initWithNibName: @"WindDirectionTemplates" bundle:nil];
        [windDirectionTemplates setupData:selectedObject entityKeyString:@"CurrentWindDirection"];
		[[self navigationController] pushViewController:windDirectionTemplates animated:YES];
	
		
	}
    else if (sectionRow  == 12)
	{
		windSpeedTemplates = [[WindSpeedTemplates alloc] initWithNibName: @"WindSpeedTemplates" bundle:nil];
        [windSpeedTemplates setupData:selectedObject entityKeyString:@"CurrentWindSpeed"];
		[[self navigationController] pushViewController:windSpeedTemplates animated:YES];
	
		
	}
    else if (sectionRow  == 13)
	{
		cloudCover = [[CloudCover alloc] initWithNibName: @"CloudCover" bundle:nil];
        [cloudCover setupData:selectedObject entityKeyString:@"CurrentCloud"];
		[[self navigationController] pushViewController:cloudCover animated:YES];
	
		
	}
    else if (sectionRow  == 10)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
		[enterValue setupData:selectedObject entityKeyString:@"CurrentWeatherTemp"  entityLabel:@"Current Weather"];
        enterValue.promptText  = @"Enter temperature as number";
        enterValue.titleText  = @"Air Temperature";
        enterValue.editkeyboardtype = UIKeyboardTypeDecimalPad;
		[[self navigationController] pushViewController:enterValue animated:YES];
	
		
	}
    else if (sectionRow  == 20)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
		[enterValue setupData:selectedObject entityKeyString:@"PredictionWeatherTemp" entityLabel:@"Prediction Weather"];        enterValue.editkeyboardtype = UIKeyboardTypeDecimalPad;
        enterValue.promptText  = @"Enter temperature as number";
        enterValue.titleText  = @"Air Temperature";
		[[self navigationController] pushViewController:enterValue animated:YES];
	
		
	}
    else if (sectionRow  == 21)
	{
		windDirectionTemplates = [[WindDirectionTemplates alloc] initWithNibName: @"WindDirectionTemplates" bundle:nil];
        [windDirectionTemplates setupData:selectedObject entityKeyString:@"PredictionWindDirection"];
        
	    [[self navigationController] pushViewController:windDirectionTemplates animated:YES];
	
	}
    else if (sectionRow  == 22)
	{
		windSpeedTemplates = [[WindSpeedTemplates alloc] initWithNibName: @"WindSpeedTemplates" bundle:nil];
        [windSpeedTemplates setupData:selectedObject entityKeyString:@"PredictionWindSpeed"];
        
		[[self navigationController] pushViewController:windSpeedTemplates animated:YES];
	
		
	}
    else if (sectionRow  == 23)
	{
		cloudCover = [[CloudCover alloc] initWithNibName: @"CloudCover" bundle:nil];
        [cloudCover setupData:selectedObject entityKeyString:@"PredictionCloud"];
		[[self navigationController] pushViewController:cloudCover animated:YES];
		
		
	}
    else if (sectionRow  == 30)
	{
		swellSizeTemplates = [[SwellSizeTemplates alloc] initWithNibName: @"SwellSizeTemplates" bundle:nil];
        [swellSizeTemplates setupData:selectedObject entityKeyString:@"CurrentSwellSize"];
		[[self navigationController] pushViewController:swellSizeTemplates animated:YES];
	
		
	}
    else if (sectionRow  == 31)
	{
		waveTypeTemplates = [[WaveTypeTemplates alloc] initWithNibName: @"WaveTypeTemplates" bundle:nil];
        [waveTypeTemplates setupData:selectedObject entityKeyString:@"CurrentWaveType"];
		[[self navigationController] pushViewController:waveTypeTemplates animated:YES];
		
		
	}
    else if (sectionRow  == 32)
	{
        tideTemplates = [[TideTemplates alloc] initWithNibName: @"TideTemplates" bundle:nil];
        [tideTemplates setupData:selectedObject entityKeyString:@"CurrentTide"];
		[[self navigationController] pushViewController:tideTemplates animated:YES];
	
		
	}
    else if (sectionRow  == 33)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
		[enterValue setupData:selectedObject entityKeyString:@"CurrentSwellTime" entityLabel:@"Current Swell Time"]; 
        enterValue.promptText  = @"in seconds";
        enterValue.titleText = @"Swell Period";
        enterValue.editkeyboardtype = UIKeyboardTypeDecimalPad;
		[[self navigationController] pushViewController:enterValue animated:YES];
	
		
	}
    else if (sectionRow  == 40)
	{
		swellSizeTemplates = [[SwellSizeTemplates alloc] initWithNibName: @"SwellSizeTemplates" bundle:nil];
        [swellSizeTemplates setupData:selectedObject entityKeyString:@"PredictionSwellSize"];
		[[self navigationController] pushViewController:swellSizeTemplates animated:YES];
		
		
	}
    else if (sectionRow  == 41)
	{
		waveTypeTemplates = [[WaveTypeTemplates alloc] initWithNibName: @"WaveTypeTemplates" bundle:nil];
        [waveTypeTemplates setupData:selectedObject entityKeyString:@"PredictionWaveType"];
		[[self navigationController] pushViewController:waveTypeTemplates animated:YES];
	
		
	}
    else if (sectionRow  == 42)
	{
		selectTime = [[SelectTime alloc] initWithNibName: @"SelectTime" bundle:nil];
        [selectTime setupData:selectedObject entityKeyString:@"PredictionLowTide" entityKeyStringValue:@"PredictionLowTideValue"];
        
	    
        selectTime.titleText = @"Low Tide Time";
        [[self navigationController] pushViewController:selectTime animated:YES];
	
		
	}
    else if (sectionRow  == 43)
	{
		selectTime = [[SelectTime alloc] initWithNibName: @"SelectTime" bundle:nil];
        [selectTime setupData:selectedObject entityKeyString:@"PredictionHighTide" entityKeyStringValue:@"PredictionHighTideValue"];
	    
        selectTime.titleText = @"High Tide Time";
        [[self navigationController] pushViewController:selectTime animated:YES];
		
	}
    else if (sectionRow  == 44)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
		[enterValue setupData:selectedObject entityKeyString:@"PredictionSwellTime"  entityLabel:@"Prediction Swell Time"]; 
        enterValue.promptText  = @"in seconds";
        enterValue.titleText = @"Swell Period";
        enterValue.editkeyboardtype = UIKeyboardTypeDecimalPad;
        
		[[self navigationController] pushViewController:enterValue animated:YES];
	
		
	}
	if (section  == EVENTS)
	{
        NSManagedObject *item = [eventItems objectAtIndex:row];
		if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
		{
            Events*  events = [[Events alloc] initWithNibName: @"Events" bundle:nil];
            [events setupData:selectedObject];
            [[self navigationController] pushViewController:events animated:YES];
 
		}
	}
    
}






@end

