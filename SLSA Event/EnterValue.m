//
//  EnterValue.m
//  SLSARiskAssessment
//
//  Created by Julian on 16/03/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "EnterValue.h"
#import "Assessment.h"
#import "EventAppDelegate.h"
@implementation EnterValue
@synthesize nameField;
@synthesize promptText;
@synthesize titleText;
@synthesize isNotes;
@synthesize  editkeyboardtype;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        editkeyboardtype  = UIKeyboardTypeDefault;
        isNotes = NO;
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([recordkey isEqualToString:@"Notes"] == YES)
        isNotes = YES;

    self.navigationItem.title = titleText;
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iphone_img_background.png"]];
    [tempImageView setFrame:self.tableView.frame];
    
    self.tableView.backgroundView = tempImageView;
    
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
    self.title = titleText;
    
}
- (void) didTapBackButton:(id)sender {
    if (isNotes == NO)
        [selectedObject setValue:nameField.text forKey:recordkey];
    else
        [selectedObject setValue:nameView.text forKey:recordkey];
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    [delegate saveContext];
         [[NSNotificationCenter defaultCenter] postNotificationName:recordkey object:nil userInfo:nil];
    
   [self dismissModalViewControllerAnimated:YES];
}

-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityLabel:(NSString*) entityLabel
{
  
    if (entityObject != nil)
    {
        selectedObject =  entityObject;
    }
    titleText = entityLabel;
    recordkey = [[NSString alloc] initWithString:entityKeyString];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    if (isNotes == NO)
        nameField.text  =  [selectedObject valueForKey:recordkey];
    else
        nameView.text =  [selectedObject valueForKey:recordkey];

    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    
      
     
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	 if (isNotes == YES)
         return 185;
    else
        return 44;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    int editFieldWidth;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        editFieldWidth =460;
    }
    else
    {
        editFieldWidth =280;
    }
    if (isNotes == YES)
    {
        UITextView *textField = [[UITextView alloc] initWithFrame:CGRectMake(5,5, editFieldWidth  , 170)];
		textField.tag = 1;
		[cell.contentView addSubview:textField];
		nameView = textField;
         textField.keyboardType = editkeyboardtype;
	     [textField becomeFirstResponder];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;

    }
    else
    {
        
      
        
        
        UITextField *textField  = [[UITextField alloc] initWithFrame:CGRectMake(10,0, editFieldWidth  , 44)];
        textField.clearButtonMode = UITextFieldViewModeAlways;
        //	[textField addTarget:self action:@selector(textFieldDoneEditing:) forControlEvents:UIControlEventEditingDidEndOnExit];
        textField.textAlignment = UITextAlignmentLeft;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        textField.keyboardType = editkeyboardtype;
        textField.returnKeyType = UIReturnKeyDefault;
        textField.tag = 1;
        textField.placeholder = promptText;
            [textField becomeFirstResponder];
        nameField = textField;
        [cell.contentView addSubview:textField];
       }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
