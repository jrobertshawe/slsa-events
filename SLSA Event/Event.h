//
//  Event.h
//  SLSA Event
//
//  Created by Julian robertshawe on 18/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Assessment;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Assessment *Assessment;

@end
