//
//  EventViewControllerHome.m
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerHome.h"

@interface EventViewControllerHome ()

@end

@implementation EventViewControllerHome
@synthesize _tableView;
@synthesize footerView;
@synthesize footerLabel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    [super viewDidLoad];
    isEditing = NO;
    NSString *navBar;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        navBar = @"ipad_img_blue-header.png";
    }
    else {
        navBar = @"iphone_img_blueheader.png";
    }
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:navBar];
       [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_edit_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
 //   self.navigationItem.RightBarButtonItem = rightButtonItem;
    
    
    
   Items = [[NSMutableArray alloc] init];
	isEditing = NO;
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav1.png",delegate.imagePrefix];
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    [hv addSubview:image];
    
  //  hv.backgroundColor = [UIColor whiteColor];
    self._tableView.autoresizesSubviews = YES;
    _tableView.tableHeaderView = hv;
    
    
    CGSize cgsize = [footerLabel.text sizeWithFont:footerLabel.font constrainedToSize:CGSizeMake(footerLabel.frame.size.width,MAXFLOAT) lineBreakMode:footerLabel.lineBreakMode];
   // self.footerView.frame = CGRectMake(0, 200, self._tableView.bounds.size.width, cgsize.height+500);
    
    // set up the table's footer view based on our UIView 'myFooterView' outlet
 //   CGRect newFrame = CGRectMake(0.0, 0.0, self._tableView.bounds.size.width, self.footerView.frame.size.height);
  //  self.footerView.backgroundColor = [UIColor clearColor];
 //   self.footerView.frame = newFrame;
   // self._tableView.tableFooterView = self.footerView; // note this will override U
     hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 000, self._tableView.bounds.size.width-40, 470)];
    label.text = @"Thank you for using the Event Risk Management tool.    \nThis tool will assist you through the risk management process to eliminate or minimise health and safety risk, involving the following steps: \n1. Identifying hazards - identify what could cause harm    \n2. Assess the risk - understand the nature of the harm that could be caused by the hazard    \n3. Review control measures - evaluate the effectiveness of existing control measures    \nYour progress through the assessment process is identified at the top of the screen.    \nOn this screen you may choose an existing venue to assess, or create a new one by selecting 'Add Venue'.";
    self.footerView.backgroundColor = [UIColor clearColor];
    hv.backgroundColor  = [UIColor clearColor];
     label.backgroundColor  = [UIColor clearColor];
    label.textColor =  [UIColor lightGrayColor];
    label.numberOfLines = 60;
    [hv addSubview:label];
     CGRect newFrame = CGRectMake(0.0, 0.0, self._tableView.bounds.size.width,500);
     hv.frame = newFrame;
    _tableView.tableFooterView = hv;
    
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
    
}
- (void)EditMode {
	if (isEditing == YES)
	{
		UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage* buttonImage = [UIImage imageNamed:@"but_edit_off.png"];
        [backButton setImage:buttonImage forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
        backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
        self.navigationItem.RightBarButtonItem = rightButtonItem;
		[self._tableView setEditing:NO animated:YES];
		isEditing = NO;
	}
	
	else
	{
		UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage* buttonImage = [UIImage imageNamed:@"but_done_off.png"];
        [backButton setImage:buttonImage forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
        backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
        self.navigationItem.RightBarButtonItem = rightButtonItem;
			[self._tableView setEditing:YES animated:YES];
		isEditing = YES;
		
	}
	
	return;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
- (void) viewWillAppear:(BOOL)animated
{
    [_tableView reloadData];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}
#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    [Items removeAllObjects];
	[Items addObjectsFromArray:[delegate GetAreas]];
     [ Items insertObject:@"Add Venue"  atIndex:0];   //add temp line to 
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [Items count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    }
    
    // Configure the cell.
    [self configureCell:cell atIndexPath:indexPath];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSString class]])
    {
        NSString* stringObject = (NSString*)managedObject;
        cell.textLabel.text = stringObject;
        cell.textLabel.textColor = [UIColor lightGrayColor];
           cell.accessoryType =    UITableViewCellAccessoryNone;
        
    }
    else
    {
        cell.textLabel.text = [[managedObject valueForKey:@"Name"] description];
        cell.textLabel.textColor = [UIColor blackColor];
            cell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
        
  //      cell.detailTextLabel.text = [[managedObject valueForKey:@"Description"] description];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSString class]])
    {
         [self performSegueWithIdentifier:@"EventDetail" sender: nil];
    }
    else 	if (isEditing == YES)
	{
         [self performSegueWithIdentifier:@"EventDetail" sender: nil];
		}
	else
	{
         [self performSegueWithIdentifier:@"Area" sender: nil];
    }
    
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
    return UITableViewCellEditingStyleDelete;
    }
    else
        return UITableViewCellEditingStyleNone;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        return YES;
    }
    else
        return NO;

}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        [delegate.managedObjectContext deleteObject:managedObject];
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        [delegate saveContext];
	}
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if (self._tableView.indexPathForSelectedRow != nil)
    {
         if ([segue.identifier isEqualToString:@"Area"] == YES)
         {
                NSManagedObject *item = [Items objectAtIndex:[self._tableView.indexPathForSelectedRow row]];
                EventViewControllerArea *ev = (EventViewControllerArea *)[segue destinationViewController];
                
                [ev setupData:item];
         }
    }
  
}
@end
