//
//  SelectLocation.m
//  SLSA Risk Assessment
//
//  Created by Julian on 13/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "SelectLocation.h"
#import "EventAppDelegate.h"
@implementation SelectLocation
@synthesize mapView;
@synthesize entity;
@synthesize entityKey;
@synthesize locationManager,currentLocation;
@synthesize latitude;
@synthesize longitude;
@synthesize selectedObject;
@synthesize notifyQueue;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    
       
    [super viewDidLoad];

    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DoneButtonPressed)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;

    
    
       
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
- (IBAction)changeType:(id)sender
{
	if(mapType.selectedSegmentIndex==0){
		mapView.mapType=MKMapTypeStandard;
	}
	else if (mapType.selectedSegmentIndex==1){
		mapView.mapType=MKMapTypeSatellite;
	}
	else if (mapType.selectedSegmentIndex==2){
		mapView.mapType=MKMapTypeHybrid;
	}
}
- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{
    for(MKAnnotationView *annotationView in views)
	{
        
        MKCoordinateRegion region;
        MKCoordinateSpan span;
        
        span.latitudeDelta=0.001;
        span.longitudeDelta=0.001;
        
        CLLocationCoordinate2D location=annotationView.annotation.coordinate;
        
        location = annotationView.annotation.coordinate;
        
        region.span=span;
        region.center=location;
        
        [mv setRegion:region animated:TRUE];
        [mv regionThatFits:region];
        
    }
}
- (void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];

    CLLocationCoordinate2D theCoordinate;
    
    if ( [entity valueForKey:@"Latitute"] == nil)
    {
        theCoordinate.latitude = delegate.latField;
        theCoordinate.longitude = [delegate longField];
        latitude =  delegate.latField;
        longitude =  delegate.longField;
        
	}
    else
    {
        
        theCoordinate.latitude = [[entity valueForKey:@"Latitute"] floatValue];
        theCoordinate.longitude = [[entity valueForKey:@"Longitude"] floatValue];
        latitude =  [[entity valueForKey:@"Latitute"] floatValue];
        longitude =  [[entity valueForKey:@"Longitude"] floatValue];
    }

    
    
    if(currentLocation == nil) 
	{
		
		currentLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
	}
    
	[mapView setRegion:MKCoordinateRegionMake(currentLocation.coordinate, MKCoordinateSpanMake(0.05f, 0.05f))];	
	[mapView setShowsUserLocation:NO];
    
   	
	if(currentLocation == nil) 
	{
		
		currentLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
	}
	
	[mapView setRegion:MKCoordinateRegionMake(currentLocation.coordinate, MKCoordinateSpanMake(0.05f, 0.05f))];	
	[mapView setShowsUserLocation:NO];
	
	
}

- (void)viewWillDisappear:(BOOL)animated {
	
	[super viewWillDisappear:animated];
	
	// NOTE: This is optional, DDAnnotationCoordinateDidChangeNotification only fired in iPhone OS 3, not in iOS 4.
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"DDAnnotationCoordinateDidChangeNotification" object:nil];	
}

-(IBAction)DoneButtonPressed
{
    NSString* longitudeString = [NSString stringWithFormat:@"%f",longitude];
     NSString* latitudeString  = [NSString stringWithFormat:@"%f",latitude];
    
    
    
    [entity setValue:longitudeString forKey:@"longitude"];
    [entity setValue:latitudeString forKey:@"Latitute"];
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];
}
#pragma mark -
#pragma mark DDAnnotationCoordinateDidChangeNotification

// NOTE: DDAnnotationCoordinateDidChangeNotification won't fire in iOS 4, use -mapView:annotationView:didChangeDragState:fromOldState: instead.
- (void)coordinateChanged_:(NSNotification *)notification {
	
    //	DDAnnotation *annotation = notification.object;
    //	annotation.subtitle = [NSString	stringWithFormat:@"%f %f", annotation.coordinate.latitude, annotation.coordinate.longitude];
    //	latitude = annotation.coordinate.latitude;
    //	longitude = annotation.coordinate.longitude;
	
    //	[selectedObject setValue:[NSNumber numberWithDouble:annotation.coordinate.longitude] forKey:@"Longitude"];
    //	[selectedObject setValue:[NSNumber numberWithDouble:annotation.coordinate.latitude] forKey:@"Latitute"];
    
}

#pragma mark -
#pragma mark MKMapViewDelegate





@end
