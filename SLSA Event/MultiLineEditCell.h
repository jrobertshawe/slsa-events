//
//  MultiLineEditCell.h
//  SLSA Risk Assessment
//
//  Created by Julian on 21/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EditMultiLineText.h"

@interface MultiLineEditCell : UITableViewCell {
    
    IBOutlet UILabel *celllabelText;
    IBOutlet UILabel *cellDetailText;
    NSString*               entityString;
    
    NSString*               entityKey;
    UIPopoverController *popoverController;
    IBOutlet UIButton *updateButton;
}
@property (nonatomic,retain)  IBOutlet UIButton *updateButton;
@property (nonatomic,retain)  UILabel						*celllabelText;
@property (nonatomic,retain)  UILabel						*cellDetailText;
-(IBAction)UpdateButtonPressed:(id)sender;
-(void)handleNotification:(NSNotification *)pNotification;
-(void)setupData:(NSString*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString;
@end
