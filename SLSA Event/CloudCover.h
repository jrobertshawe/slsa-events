//
//  CloudCover.h
//  SLSARiskAssessment
//
//  Created by Julian on 18/03/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

@interface CloudCover : UITableViewController {
@private
    NSArray* itemArray;   
  NSString *recordkey;
    NSManagedObject* selectedObject;
}
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString;
@end
