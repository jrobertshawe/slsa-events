//
//  WindDirectionTemplate.m
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "WindDirectionTemplate.h"


@implementation WindDirectionTemplate

@dynamic Order;
@dynamic Description;

@end
