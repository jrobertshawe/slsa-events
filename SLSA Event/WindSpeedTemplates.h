//
//  WindSpeedTemplates.h
//  SLSARiskAssessment
//
//  Created by Julian on 16/03/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

@interface WindSpeedTemplates : UITableViewController {
@private
    NSArray* itemArray;   
    NSManagedObject *selectedObject;
    NSString *recordkey;
}

-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString;
@end
