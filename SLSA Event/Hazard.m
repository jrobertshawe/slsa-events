//
//  Hazard.m
//  SLSA Event
//
//  Created by Julian robertshawe on 24/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "Hazard.h"
#import "Assessment.h"
#import "Control.h"
#import "Photo.h"
#import "Risk.h"
#import "RiskGroup.h"


@implementation Hazard

@dynamic RiskScoreConsequence;
@dynamic Description;
@dynamic RiskScore;
@dynamic Latitute;
@dynamic ResidualRiskScoreConsequence;
@dynamic ResidualRiskScoreLikelihood;
@dynamic ResidualRiskScore;
@dynamic Longitude;
@dynamic status;
@dynamic RiskScoreLikelihood;
@dynamic RiskGroups;
@dynamic ExistingControls;
@dynamic Risks;
@dynamic Photos;
@dynamic Assessment;
@dynamic RecommendedControls;

@end
