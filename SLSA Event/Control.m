//
//  Control.m
//  SLSA Event
//
//  Created by Julian robertshawe on 7/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "Control.h"
#import "Hazard.h"


@implementation Control

@dynamic Description;
@dynamic Effectiveness;
@dynamic display_order;
@dynamic HazardRecommendend;
@dynamic ExistingHazard;

@end
