//
//  Control.h
//  SLSA Event
//
//  Created by Julian robertshawe on 7/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Hazard;

@interface Control : NSManagedObject

@property (nonatomic, retain) NSString * Description;
@property (nonatomic, retain) NSString * Effectiveness;
@property (nonatomic, retain) NSNumber * display_order;
@property (nonatomic, retain) Hazard *HazardRecommendend;
@property (nonatomic, retain) Hazard *ExistingHazard;

@end
