//
//  AssessmentDetails.h
//  SLSARiskAssessment
//
//  Created by Julian on 2/12/10.
//  Copyright 2010 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GovernmentArea.h"
#import "EventAppDelegate.h"
@interface	AssessmentDetails: UIViewController< UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate > {
	IBOutlet UITextField			*conditionField;
		IBOutlet UITextField		*temperatureField;
	IBOutlet UITextField			*descriptionField;
	NSString *objectString;
    NSString						*assessmenturi;
	bool newRecord;
    NSMutableArray					*eventItems;
    GovernmentArea* governmentArea;
    NSManagedObject*                selectedObject;
    Assessment*                     item;
    LocalSite*                     localsite;
    
}
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (nonatomic, retain) GovernmentArea* governmentArea;
@property (nonatomic, retain) IBOutlet UITextField *conditionField;
@property (nonatomic, retain) IBOutlet UITextField *temperatureField;
@property (nonatomic, retain) IBOutlet UITextField *descriptionField;
- (void)cancelRequest;
- (void)saveObject;
-(void)setupData:(NSString*) entityObject;
@end
