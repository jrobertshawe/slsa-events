//
//  SelectionValues.h
//  AussieIpad
//
//  Created by Julian robertshawe on 19/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SelectionValues : NSManagedObject

@property (nonatomic, retain) NSNumber * display_order;
@property (nonatomic, retain) NSString * itemDescription;
@property (nonatomic, retain) NSString * itemKey;
@property (nonatomic, retain) NSString * itemValue;
@property (nonatomic, retain) NSString * timestamp;

@end
