//
//  EventViewControllerEvents.h
//  SLSA Event
//
//  Created by Julian robertshawe on 4/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

#define HEADER_FIELD_TAG_1 0
#define HEADER_FIELD_TAG_2 1


///Table Cell Indexes and Identifiers
#define DETAILS 2
#define CURRENTWEATHER 5
#define CURRENTSURF 3
#define PREDICTIONWEATHER 1
#define PREDICTIONSURF 4
#define EVENTS 0

@interface EventViewControllerEvents : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITextField			*conditionField;
    IBOutlet UITextField		*temperatureField;
	IBOutlet UITextField			*descriptionField;
	NSString *objectString;
    bool newRecord;
    NSMutableArray					*eventItems;
    GovernmentArea* governmentArea;
    LocalSite* localSiteObject;
    
    Assessment* selectedObject;
}
@property (nonatomic, retain) IBOutlet UIView		*footerView;
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (nonatomic, retain) GovernmentArea* governmentArea;
@property (nonatomic, retain) IBOutlet UITextField *conditionField;
@property (nonatomic, retain) IBOutlet UITextField *temperatureField;
@property (nonatomic, retain) IBOutlet UITextField *descriptionField;
- (void)cancelRequest;
- (void)saveObject;
-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite;
@end