//
//  EventViewControllerEvents.m
//  SLSA Event
//
//  Created by Julian robertshawe on 4/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerEvents.h"
#import "events.h"
#import "AssessmentTemplates.h"
@interface EventViewControllerEvents ()

@end

@implementation EventViewControllerEvents
@synthesize footerView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad {
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonImage = [UIImage imageNamed:@"but_done_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //self.navigationItem.rightBarButtonItem = backButtonItem;
    
    
    // self.navigationItem.title = @"Add Assessment";
	if (selectedObject != nil)
	{
        
		if ([selectedObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
		{
            //		self.navigationItem.title = @"Update Assessment";
			newRecord = NO;
		}
		else
		{
            Assessment* assessmentObject = (Assessment *)[NSEntityDescription insertNewObjectForEntityForName:@"Assessment" inManagedObjectContext:[delegate managedObjectContext]];
            [assessmentObject setValue:[NSDate date] forKey:@"AssessmentDate"];
            
			newRecord = YES;
            [assessmentObject setValue:governmentArea.ContactName forKey:@"Assessor"];
            
		}
	}
	else
	{
        selectedObject = (Assessment *)[NSEntityDescription insertNewObjectForEntityForName:@"Assessment" inManagedObjectContext:[delegate managedObjectContext]];
        [selectedObject setValue:[NSDate date] forKey:@"AssessmentDate"];
        
        [selectedObject setValue:governmentArea.ContactName forKey:@"Assessor"];
		newRecord = YES;
	}
    [delegate saveContext];
 
    self.title = @"Activity";
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"refreshView"
     object:nil];
    
}
-(void)handleNotification:(NSNotification *)pNotification
{
    [self dismissModalViewControllerAnimated:YES];
    [self._tableView reloadData];
}
- (void) didTapBackButton:(id)sender {
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    selectedObject.activityChecked = [NSNumber numberWithBool: YES];
    
    [delegate saveContext];
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite
{
    
    localSiteObject = localSite;
    if ([entityObject isKindOfClass:[Assessment class]])
    {
        selectedObject = entityObject;
    }
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    
    return NO;
}

- (void)cancelRequest
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
	if (newRecord == YES)
		[[delegate managedObjectContext] deleteObject:selectedObject];
	[[self navigationController] popViewControllerAnimated:YES];
}
- (void)saveObject
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
	
	if (selectedObject != nil)
	{
		if ([selectedObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
		{
            
			if (objectString != nil)
			{
                
				if ([selectedObject isKindOfClass:[LocalSite class]]) //It is a risk object so update data
				{
                    [localSiteObject addAssessmentsObject:selectedObject];
					
                    
				}
				
			}
		}
	}
	[delegate saveContext];
	
	[[self navigationController] popViewControllerAnimated:YES];
	
	
	
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)] ;
	tableView.sectionHeaderHeight = headerView.frame.size.height;
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, headerView.frame.size.width - 20, 22)] ;
	label.text = [self tableView:tableView titleForHeaderInSection:section];
	label.font = [UIFont boldSystemFontOfSize:16.0];
	label.shadowOffset = CGSizeMake(0, 1);
	label.shadowColor = [UIColor blackColor];
	label.backgroundColor = [UIColor clearColor];
    
	label.textColor = [UIColor whiteColor];
    
	[headerView addSubview:label];
	return headerView;
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self._tableView reloadData];

}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav3.png",delegate.imagePrefix];
    
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    
    [hv addSubview:image];
    tableView.tableHeaderView = hv;
    hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,500)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 000, self._tableView.bounds.size.width-40, 220)];
    label.text = @"Add an activity by pressing the button. You can select multiple activities.    \nOnce activities are added, hit the Back button to go back to the Assessment screen to detail the hazards.";
    self.footerView.backgroundColor = [UIColor clearColor];
    hv.backgroundColor  = [UIColor clearColor];
    label.backgroundColor  = [UIColor clearColor];
    label.textColor =  [UIColor lightGrayColor];
    label.numberOfLines = 40;
    [hv addSubview:label];
    CGRect newFrame = CGRectMake(0.0, 0.0, self._tableView.bounds.size.width,700);
    hv.frame = newFrame;
    self._tableView.tableFooterView = hv;
  
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int sectionRowCount;
	
   			sectionRowCount = [eventItems count];
		
    return sectionRowCount;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
  	NSString *headerTitle = @"";
	
          if (eventItems == nil)
                eventItems = [[NSMutableArray alloc] init];
            else
                [eventItems removeAllObjects];
            
            [eventItems addObjectsFromArray:[selectedObject.Events allObjects]];
            [ eventItems insertObject:@"Add Activity"  atIndex:0];    //add temp line to allow adding items
            headerTitle =  @"Activity";
         
    return headerTitle;
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {;
    
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
    static NSString *CellIdentifier = @"Cell";
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
		
		

        
        
        
	}


    cell.editingAccessoryType = UITableViewCellSelectionStyleNone;
    cell.accessoryType =  UITableViewCellSelectionStyleNone;;
    
        
    
    if (section == EVENTS)
    {
        UITextField *textField = (UITextField *)[cell viewWithTag:1];
        if (textField != nil)
            textField.hidden = YES;
        NSManagedObject *item = [eventItems objectAtIndex:row];
        if ([item isKindOfClass:[NSString class]])
        {
            NSString* labelText = (NSString*) item;
            cell.textLabel.text  = labelText;
            cell.textLabel.textColor = [UIColor lightGrayColor];
            
            cell.editingAccessoryType = UITableViewCellAccessoryNone;
        }
        else
        {
            cell.textLabel.textColor = [UIColor blackColor];
            
            Event *event = (Event* )item;
            cell.textLabel.text  = [[event valueForKey:@"Name"] description];
            cell.editingAccessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    
    // Configure the cell...
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	NSUInteger section = [indexPath section];
	if (section == EVENTS )  //if add items then indent
		return YES;
	
	return NO;
}



- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    NSManagedObject *managedObject = [eventItems objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        return UITableViewCellEditingStyleDelete;
    }
    else
        return UITableViewCellEditingStyleNone;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *managedObject = [eventItems objectAtIndex:[indexPath row]];
    
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        return YES;
    }
    else
        return NO;
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSManagedObject *managedObject = [eventItems objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        [delegate.managedObjectContext deleteObject:managedObject];
        
          [delegate saveContext];
        [tableView reloadData];
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
  
	if (section  == EVENTS)
	{
        NSManagedObject *item = [eventItems objectAtIndex:row];
		if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
		{
            Events*  events = [[Events alloc] initWithNibName: @"Events" bundle:nil];
            [events setupData:selectedObject];
            
            UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:events] ;
            toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
            NSString *navBar;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                navBar = @"ipad_img_blue-header.png";
            }
            else {
                navBar = @"iphone_img_blueheader.png";
            }
            
            UIImage *image = [UIImage imageNamed:navBar];
            [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
            
            
            toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
            
            
            [self presentModalViewController:toolsNavController animated:YES];
            
		}
	}
    
}






@end

