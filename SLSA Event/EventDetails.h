//
//  EventDetails.h
//  SLSARiskAssessment
//
//  Created by Julian on 13/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

@interface EventDetails : UITableViewController {
	IBOutlet UITextField			*descriptionField;
    NSString* objectString;
    NSManagedObject* selectedObject;
}
@property (nonatomic, retain) IBOutlet UITextField *descriptionField;
- (void)cancelRequest;
- (void)insertNewObject;
-(void)setupData:(NSString*) entityObject;
@end
