//
//  HazardTemplates.m
//  SLSARiskAssessment
//
//  Created by Julian on 14/12/10.
//  Copyright 2010 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "HazardTemplates.h"
#import "HazardTemplateDetails.h"

#import "HazardTemplate.h"

@interface HazardTemplates ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation HazardTemplates
@synthesize _tableView;


#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.navigationItem.title = @"Hazard Type";
	
	
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_edit_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
  //  self.navigationItem.rightBarButtonItem = backButtonItem;
	isEditing = NO;
	selectedItems = [[NSMutableArray alloc] init];
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
    itemArray = [[NSMutableArray alloc] init];
	
}
-(void)setupData:(NSManagedObject*) entityObject 

{
    
    
    object = entityObject;
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return NO;
}

// Implement viewWillAppear: to do additional setup before the view is presented.
- (void)viewWillAppear:(BOOL)animated {
	EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
	
	
	[self._tableView reloadData];
    [super viewWillAppear:animated];
}

- (void)insertNewObject {
	HazardTemplateDetails*  hazardTemplateDetails = [[HazardTemplateDetails alloc] initWithNibName: @"HazardTemplateDetails" bundle:nil];
    [hazardTemplateDetails setupData:nil];
	[[self navigationController] pushViewController:hazardTemplateDetails animated:YES];
	return;
	
}
- (void)EditMode {
	if (self._tableView.isEditing == YES)
	{
		[self._tableView setEditing:NO animated:YES];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage* buttonImage = [UIImage imageNamed:@"but_edit_off.png"];
        [backButton setImage:buttonImage forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
        backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
        self.navigationItem.rightBarButtonItem = backButtonItem;

	}
	
	else
	{
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage* buttonImage = [UIImage imageNamed:@"but_done_off.png"];
        [backButton setImage:buttonImage forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
        backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
        self.navigationItem.rightBarButtonItem = backButtonItem;
		[self._tableView setEditing:YES animated:YES];
        
		
	}
	
	return;
}

- (void)viewDidAppear:(BOOL)animated {
	
	[super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];

	
}
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    int row = [indexPath row];
	EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
	NSArray *tempcontrolsArray = [delegate GetHazardTemplates];
    HazardTemplate  *control = [tempcontrolsArray objectAtIndex:row ];
    cell.textLabel.text =  [[control valueForKey:@"Description"] description];
	if ([selectedItems containsObject:control] == YES)
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	else
		cell.accessoryType = UITableViewCellAccessoryNone;
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
}


#pragma mark -
#pragma mark Add a new object



#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iphone_img_background.png"]];
    [tempImageView setFrame:tableView.frame];
    
    tableView.backgroundView = tempImageView;
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    [itemArray removeAllObjects];
   
    [itemArray addObjectsFromArray:[delegate GetHazardTemplates]];
    [ itemArray insertObject:@"Add new hazard" atIndex:0];   //add temp line to allow adding items
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [itemArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    // Configure the cell.
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}



/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		int row = [indexPath row];
		EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
		NSArray *temprisksArray = [delegate GetHazardTemplates];
		HazardTemplate  *control = [temprisksArray objectAtIndex:row ];
		[delegate DeleteRecord:control];
		itemArray = [delegate GetHazardTemplates];
		[self._tableView reloadData];
	}   
	else if (editingStyle == UITableViewCellEditingStyleInsert) {
		// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}   
	
}





#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	int row = [indexPath row];
	EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
	NSArray *tempArray = [delegate GetHazardTemplates];
    HazardTemplate  *control = [tempArray objectAtIndex:row ];
    NSString * tempObjectString = [[[control objectID] URIRepresentation] absoluteString];
    
	if (isEditing == YES)
	{
		HazardTemplateDetails*  hazardTemplateDetails = [[HazardTemplateDetails alloc] initWithNibName: @"HazardTemplateDetails" bundle:nil];
        [hazardTemplateDetails setupData:tempObjectString];
		
		[[self navigationController] pushViewController:hazardTemplateDetails animated:YES];

	}
	else
	{
        
        
        if ([object isKindOfClass:[Hazard class]]) //It is a risk object so update data
        {
            NSString* description = control.Description;
            [object setValue:description forKey:@"Description"];
        }
        else
        {
            Hazard* hazardObject = (Hazard *)[NSEntityDescription insertNewObjectForEntityForName:@"Hazard" inManagedObjectContext:[delegate managedObjectContext]];
            [hazardObject setValue:control.Description forKey:@"Description"];
            Assessment* assessment  = (Assessment*)object;
            [assessment addHazzardsObject:hazardObject];
            
        }

        
        
        
        
	       [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshView" object:nil userInfo:nil];

        [self dismissModalViewControllerAnimated:YES];
		
	}
	
}


#pragma mark -
#pragma mark Fetched results controller

#pragma mark -
#pragma mark Fetched results controller delegate


/*
 // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed. 
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
 // In the simplest, most efficient, case, reload the table view.
 [self.tableView reloadData];
 }
 */


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}





@end


