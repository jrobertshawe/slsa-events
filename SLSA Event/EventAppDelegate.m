//
//  EventAppDelegate.m
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventAppDelegate.h"

@implementation EventAppDelegate
@synthesize latField;
@synthesize longField;
@synthesize accuracy;
@synthesize fieldinError;
@synthesize imagePrefix;
@synthesize header_height;
@synthesize header_length;
@synthesize areaViewController;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if ([self.GetHazardTemplates count] == 0)
	{
		[self AddHazardTemplate:@"Craft" DisplayOrder:0];
		[self AddHazardTemplate:@"Boats" DisplayOrder:1];
		[self AddHazardTemplate:@"Dangerous Surf Conditions affecting provision of water safety" DisplayOrder:2];
        [self AddHazardTemplate:@"Course length" DisplayOrder:5];
        [self AddHazardTemplate:@"Width of surf zone (wide)" DisplayOrder:6];
        [self AddHazardTemplate:@"Longshore Drift / strong currents" DisplayOrder:6];
        [self AddHazardTemplate:@"Water visibility / turbidity" DisplayOrder:7];
        [self AddHazardTemplate:@"Dangerous Waves (Size)" DisplayOrder:8];
        [self AddHazardTemplate:@"Dangerous Waves (type)" DisplayOrder:10];
        [self AddHazardTemplate:@"Shore dump" DisplayOrder:11];
        [self AddHazardTemplate:@"Shallow water" DisplayOrder:12];
        [self AddHazardTemplate:@"Stinger/Marine creatures" DisplayOrder:13];
        [self AddHazardTemplate:@"Rocks" DisplayOrder:14];
        [self AddHazardTemplate:@"Predatory Sea Life" DisplayOrder:15];
        [self AddHazardTemplate:@"Pollution" DisplayOrder:16];
        [self AddHazardTemplate:@"Extreme Heat" DisplayOrder:17];
        [self AddHazardTemplate:@"Manmade structures" DisplayOrder:18];
        [self AddHazardTemplate:@"Lightning" DisplayOrder:19];
         [self AddHazardTemplate:@"Machinery/Vehicle Conflicting" DisplayOrder:20];
             
        
        
	}
    if ([self.GetEffectivenessTemplate count] == 0)
	{
		[self AddEffectivenessTemplate:@"Inadequate" DisplayOrder:4];
		[self AddEffectivenessTemplate:@"Poor" DisplayOrder:3];
		[self AddEffectivenessTemplate:@"Moderate" DisplayOrder:2];
		[self AddEffectivenessTemplate:@"Good" DisplayOrder:1];
		[self AddEffectivenessTemplate:@"Excellent" DisplayOrder:0];
		
		
	}
	if ([self.GetAssessmentTemplates count] == 0)
	{
		[self AddAssessmentTemplate:@"Pre competition" DisplayOrder:0];
		[self AddAssessmentTemplate:@"Mid morning" DisplayOrder:1];
		[self AddAssessmentTemplate:@"Lunch" DisplayOrder:2];
		[self AddAssessmentTemplate:@"Mid Afternoon" DisplayOrder:3];
		[self AddAssessmentTemplate:@"On demand" DisplayOrder:4];
	}
    if ([self.GetWindDirectionTemplates count] == 0)
	{
		[self AddWindDirectionTemplate:@"North" DisplayOrder:0];
		[self AddWindDirectionTemplate:@"NorthEast" DisplayOrder:1];
		[self AddWindDirectionTemplate:@"East" DisplayOrder:2];
		[self AddWindDirectionTemplate:@"SouthEast" DisplayOrder:3];
		[self AddWindDirectionTemplate:@"South" DisplayOrder:4];
		[self AddWindDirectionTemplate:@"SouthWest" DisplayOrder:5];
		[self AddWindDirectionTemplate:@"West" DisplayOrder:6];
		[self AddWindDirectionTemplate:@"NorthWest" DisplayOrder:7];
		
		
	}
    if ([self.GetWindSpeedTemplates count] == 0)
	{
        [self AddWindSpeedTemplate:@"Nil (0 Kts)" DisplayOrder:0];
		[self AddWindSpeedTemplate:@"Slight (=< 10 kts)" DisplayOrder:2 ];
		[self AddWindSpeedTemplate:@"Moderate (11 - 16 kts)" DisplayOrder:3 ];
		[self AddWindSpeedTemplate:@"Fresh (17 - 21 kts)" DisplayOrder:4 ];
		[self AddWindSpeedTemplate:@"Strong (22 - 33 kts)" DisplayOrder:5 ];
		[self AddWindSpeedTemplate:@"Gale/Storm(34kts+)" DisplayOrder:6 ];
		
		
	}
    if ([self.GetCloudTemplates count] == 0)
	{
        [self AddCloudTemplate:@"Fine" DisplayOrder:0];
		[self AddCloudTemplate:@"Overcast" DisplayOrder:2 ];
		[self AddCloudTemplate:@"Rain" DisplayOrder:3 ];
		[self AddCloudTemplate:@"Windy" DisplayOrder:4 ];
		
		
	}
    if ([self.GetSwellSizeTemplates count] == 0)
	{
        [self AddSwellSizeTemplates:@"0m-0.5m" DisplayOrder:0];
		[self AddSwellSizeTemplates:@"0.5m - 1.5m" DisplayOrder:2 ];
		[self AddSwellSizeTemplates:@"1.5m - 2.5m" DisplayOrder:3 ];
		[self AddSwellSizeTemplates:@"2.5m+" DisplayOrder:4 ];
		
		
	}
    if ([self.GetWaveTypeTemplates count] == 0)
	{
        [self AddWaveTypeTemplates:@"surging" DisplayOrder:0];
		[self AddWaveTypeTemplates:@"spilling" DisplayOrder:2 ];
		[self AddWaveTypeTemplates:@"plunging" DisplayOrder:3 ];
		
		
	}
    if ([self.GetTideTemplates count] == 0)
	{
        [self AddTideTemplate:@"High" DisplayOrder:0];
		[self AddTideTemplate:@"Falling" DisplayOrder:2 ];
		[self AddTideTemplate:@"Low" DisplayOrder:3 ];
		[self AddTideTemplate:@"Rising" DisplayOrder:3 ];
		
	}
    if ([self.GetEventTemlates count] == 0)
	{
		[self AddEventTemplate:@"Boats" DisplayOrder:0];
		[self AddEventTemplate:@"Skis" DisplayOrder:1];
		[self AddEventTemplate:@"Boards" DisplayOrder:2];
		[self AddEventTemplate:@"Swim" DisplayOrder:3];
		[self AddEventTemplate:@"Tube" DisplayOrder:4];
		[self AddEventTemplate:@"Belt" DisplayOrder:5];
		[self AddEventTemplate:@"Flags" DisplayOrder:6];
		[self AddEventTemplate:@"Sprint" DisplayOrder:7];
		[self AddEventTemplate:@"M/Past" DisplayOrder:8];
		[self AddEventTemplate:@"R & R" DisplayOrder:9];
        [self AddEventTemplate:@"Lifesaving" DisplayOrder:10];
        [self AddEventTemplate:@"Swim Course" DisplayOrder:11];
        [self AddEventTemplate:@"Road Course" DisplayOrder:12];
        [self AddEventTemplate:@"Beach Course" DisplayOrder:13];
         [self AddEventTemplate:@"Back of beach" DisplayOrder:14];
         [self AddEventTemplate:@"Pool" DisplayOrder:15];
		
        
		
	}
    if ([[self GetDataArray:@"RiskTemplate"] count] == 0)
	{
		[self AddRiskTemplate:@"Collision / injury"];
		[self AddRiskTemplate:@"Drowning death"];
		[self AddRiskTemplate:@"Injury"];
        [self AddRiskTemplate:@"Fatigue"];
        
        [self AddRiskTemplate:@"Larger area under supervision"];
		[self AddRiskTemplate:@"Increased race duration"];
		[self AddRiskTemplate:@"Increased exposure to hazard"];
		[self AddRiskTemplate:@"Difficulty of supervision"];
		[self AddRiskTemplate:@"Impedance to SAR"];
        
		
	}
    

    
    if ([[self GetDataArray:@"RiskGroupTemplate"] count] == 0)
	{
		[self AddRiskGroupTemplate:@"Under 15 Men" ];
		[self AddRiskGroupTemplate:@"Under 15 Women"];
		[self AddRiskGroupTemplate:@"Under 17 Men"];
		[self AddRiskGroupTemplate:@"Under 17 Women" ];
		[self AddRiskGroupTemplate:@"Under 19 Men"];
		[self AddRiskGroupTemplate:@"Under 19 Women"];
		[self AddRiskGroupTemplate:@"Open Men"];
		[self AddRiskGroupTemplate:@"Open Women"];
		[self AddRiskGroupTemplate:@"Officials"];
		[self AddRiskGroupTemplate:@"Spectators"];
		[self AddRiskGroupTemplate:@"Water Safety"];
        [self AddRiskGroupTemplate:@"Open Male Boat"];
        [self AddRiskGroupTemplate:@"Open Female Boat"];
        [self AddRiskGroupTemplate:@"Masters Boat"];
        [self AddRiskGroupTemplate:@"U/23 Male Boat"];
        [self AddRiskGroupTemplate:@"U/23 Female Boat"];
        [self AddRiskGroupTemplate:@"Junior Boat"];
		
	}

    
    if ([[self GetDataArray:@"ControlTemplate"] count] == 0)
	{
		[self AddControlTemplate:@"Area Buffer zones" DisplayOrder:1];
        [self AddControlTemplate:@"Lower competitor numbers" DisplayOrder:2];
    	[self AddControlTemplate:@"Water safety" DisplayOrder:3];
    	[self AddControlTemplate:@"Match skill level to conditions" DisplayOrder:4];
    	[self AddControlTemplate:@"Event Postponed" DisplayOrder:5];
    	[self AddControlTemplate:@"Move Event" DisplayOrder:6];
    	[self AddControlTemplate:@"Recommend use of helmets" DisplayOrder:7];
    	[self AddControlTemplate:@"Personal floatation Devices" DisplayOrder:8];
    	[self AddControlTemplate:@"Increased observation" DisplayOrder:9];
    	[self AddControlTemplate:@"Cancel event" DisplayOrder:10];
    	[self AddControlTemplate:@"Hi Vis Vests" DisplayOrder:11];
    	[self AddControlTemplate:@"Scrutineering of equipment" DisplayOrder:12];
    	[self AddControlTemplate:@"Gate buoys" DisplayOrder:13];
    	[self AddControlTemplate:@"Safety consultation with competitors" DisplayOrder:14];
    	[self AddControlTemplate:@"Safety consultation with manager" DisplayOrder:15];
    	[self AddControlTemplate:@"Consultation with event management" DisplayOrder:16];
        [self AddControlTemplate:@"Monitor" DisplayOrder:16];
        
        
    }
    fieldinError = [[NSMutableArray alloc] init];
       if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
       {
           imagePrefix = @"ipad";
           header_length =768;
           header_height = 149;
       }
       else
       {
           imagePrefix = @"iphone";
           header_length =300;
           header_height = 81;
       }
    return YES;
}
-(void) AddEffectivenessTemplate:(NSString*) description DisplayOrder:(int)order
{
	EffectivenessTemplate* item = (EffectivenessTemplate *)[NSEntityDescription insertNewObjectForEntityForName:@"EffectivenessTemplate" inManagedObjectContext:[self managedObjectContext]];
	[item setValue:description forKey:@"Description"];
    [item setValue:[NSNumber numberWithInt:order]  forKey:@"Order"];
	
	
}
-(bool)isFieldInError:(NSString*)FieldToCheck
{
          
        
        for (NSString *thisString in fieldinError) {
           if ([FieldToCheck  isEqualToString:thisString] == YES)
               return YES;
                }
        
        return NO;
        
    }

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
 	locationController.delegate = self;
	[locationController.locationManager startUpdatingLocation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark -
#pragma mark Core Data stack
- (void)saveContext {
    
    NSError *error = nil;
    if (managedObjectContext_ != nil) {
        if ([managedObjectContext_ hasChanges] && ![managedObjectContext_ save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext {
    
    if (managedObjectContext_ != nil) {
        return managedObjectContext_;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext_ = [[NSManagedObjectContext alloc] init];
        [managedObjectContext_ setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext_;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel {
    
    if (managedObjectModel_ != nil) {
        return managedObjectModel_;
    }
    NSString *modelPath = [[NSBundle mainBundle] pathForResource:@"SLSARiskAssessment" ofType:@"momd"];
    NSURL *modelURL = [NSURL fileURLWithPath:modelPath];
    managedObjectModel_ = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel_;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (persistentStoreCoordinator_ != nil) {
        return persistentStoreCoordinator_;
    }
   NSURL *storeURL = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"SLSARiskAssessment.sqlite"]];
    
    NSError *error = nil;
    persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }	
    
    return persistentStoreCoordinator_;
}
#pragma mark - Application's Documents directory
- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}
#pragma mark -
#pragma mark General Data Functions
-(NSArray*) GetAreas
{
    //Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"GovernmentArea" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);
	return FetchResults;

}


-(NSArray*) GetHazardTemplates
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"HazardTemplate" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);
	return FetchResults;
	
	
	//return the Array
	
}
-(void)AddAreas:(GovernmentArea *) governmentArea
{
    
    LocalSite* localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"U15 Area 2" forKey:@"Name"];
    [localSite setValue:@"0" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"U15 Area 1" forKey:@"Name"];
    [localSite setValue:@"1" forKey:@"BeachKey"];
  	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"U17 Area 2" forKey:@"Name"];
    [localSite setValue:@"2" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"U17 Area 1" forKey:@"Name"];
    [localSite setValue:@"3" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Open & U19 Women" forKey:@"Name"];
    [localSite setValue:@"4" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Open & U19 Male" forKey:@"Name"];
    [localSite setValue:@"5" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Open & U19 Male" forKey:@"Name"];
    [localSite setValue:@"6" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Boat Area 2" forKey:@"Name"];
    [localSite setValue:@"7" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Boat Area 1" forKey:@"Name"];
    [localSite setValue:@"8" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"R&R & Lifesaving" forKey:@"Name"];
    [localSite setValue:@"9" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Water" forKey:@"Name"];
    [localSite setValue:@"10" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Road" forKey:@"Name"];
    [localSite setValue:@"11" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"River" forKey:@"Name"];
    [localSite setValue:@"12" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Park" forKey:@"Name"];
    [localSite setValue:@"13" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Ocean" forKey:@"Name"];
    [localSite setValue:@"14" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Beach" forKey:@"Name"];
    [localSite setValue:@"15" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Back of beach" forKey:@"Name"];
    [localSite setValue:@"15" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
  
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Warm up" forKey:@"Name"];
    [localSite setValue:@"15" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    localSite = (LocalSite *)[NSEntityDescription insertNewObjectForEntityForName:@"LocalSite" inManagedObjectContext:[self managedObjectContext]];
    [localSite setValue:0 forKey:@"Longitude"];
    [localSite setValue:0 forKey:@"Latitute"];
    [localSite setValue:@"Pool" forKey:@"Name"];
    [localSite setValue:@"15" forKey:@"BeachKey"];
	[governmentArea  addSitesObject:localSite];
    
    
}
-(void) DeleteRecord:(NSManagedObject *)record
{
	[[self managedObjectContext] deleteObject:record];
    
	[self saveContext];
}
-(void) AddHazardTemplate:(NSString*) description  DisplayOrder:(int)order
{
	HazardTemplate* item = (HazardTemplate *)[NSEntityDescription insertNewObjectForEntityForName:@"HazardTemplate" inManagedObjectContext:[self managedObjectContext]];
	[item setValue:description forKey:@"Description"];
    [item setValue:[NSNumber numberWithInt:order]  forKey:@"Order"];
	
	
}
-(GovernmentArea* ) AddGovernmentArea:(NSString*) name Description:(NSString*)description PhoneNumber:(NSString*)phoneNumber ContactName:(NSString*)contactName Address:(NSString*)address
{
	GovernmentArea* governmentArea = (GovernmentArea *)[NSEntityDescription insertNewObjectForEntityForName:@"GovernmentArea" inManagedObjectContext:[self managedObjectContext]];
	[governmentArea setValue:name forKey:@"Name"];
	[governmentArea setValue:description forKey:@"Description"];
	[governmentArea setValue:address forKey:@"Address"];
	[governmentArea setValue:contactName forKey:@"ContactName"];
	[governmentArea setValue:description forKey:@"PhoneNumber"];
    [self AddAreas:governmentArea];
	[self saveContext];
    return governmentArea;
}
-(NSArray*) GetControls
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"ControlTemplate" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Description" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	

	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);

	return FetchResults;
	
	
	//return the Array
	
}
-(NSArray*) GetLocalGovermentArea
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"GovernmentArea" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Name" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	

	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);

	return FetchResults;
	
	
	//return the Array
	
}
-(NSArray*) GetAssessmentTemplates
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"AssessmentTemplate" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];

	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);

	return FetchResults;
	
	
	//return the Array
	
}
-(NSArray*) GetWaveTypeTemplates
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"WaveTypeTemplate" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
  
	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);

	return FetchResults;
	
	
	//return the Array
	
}
-(NSArray*) GetSwellSizeTemplates
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"SwellSizeTemplate" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
  
	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);

	return FetchResults;
	
	
	//return the Array
	
}

-(NSArray*) GetTideTemplates
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"TideTemplate" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	

	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);

	return FetchResults;
	
	
	//return the Array
	
}
-(NSArray*) GetCloudTemplates
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"CloudTemplate" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	

	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);

	return FetchResults;
	
	
	//return the Array
	
}

-(NSArray*) GetWindDirectionTemplates
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"WindDirectionTemplate" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	

	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);

	return FetchResults;
	
	
	//return the Array
	
}

-(NSArray*) GetWindSpeedTemplates
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"WindSpeedTemplate" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];

	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);

	return FetchResults;
	
	
	//return the Array
	
}
-(NSArray*) GetEffectivenessTemplate
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"EffectivenessTemplate" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	

	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);

	return FetchResults;
	
	
	//return the Array
	
}
-(void) AddEvent:(NSString*) description  ParentRecord:(Assessment *)parent
{
	
       if (description == nil)
           return;
           
    for (Event *existing in parent.Events)
    {
        if ([existing.name isEqualToString:description] == YES)
            return;
    }
    
    Event* item = (Event *)[NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:[self managedObjectContext]];
    
    
    
    
	[item setValue:description forKey:@"name"];
    
	[parent addEventsObject:item];
	
}
-(void) AddControlTemplate:(NSString*) description  DisplayOrder:(int)order
{
	NSManagedObject* control = (NSManagedObject *)[NSEntityDescription insertNewObjectForEntityForName:@"ControlTemplate" inManagedObjectContext:[self managedObjectContext]];
	[control setValue:description forKey:@"Description"];
    [control setValue:[NSNumber numberWithInt:order]  forKey:@"Order"];
	
	
}
-(void) AddEventTemplate:(NSString*) description DisplayOrder:(int)order
{
	EventTemplate* event = (EventTemplate *)[NSEntityDescription insertNewObjectForEntityForName:@"EventTemplate" inManagedObjectContext:[self managedObjectContext]];
	[event setValue:description forKey:@"Description"];
    [event setValue:[NSNumber numberWithInt:order]  forKey:@"Order"];
    
	
}
-(void) AddRiskTemplate:(NSString*) description
{
    [self AddTemplate:description templateTable:@"RiskTemplate"];
}
-(void) AddRiskGroupTemplate:(NSString*) description
{
    [self AddTemplate:description templateTable:@"RiskGroupTemplate"];
}
-(void) AddTemplate:(NSString*) name  templateTable:(NSString*)templateTable
{
    
	NSManagedObject* item = [NSEntityDescription insertNewObjectForEntityForName:templateTable inManagedObjectContext:[self managedObjectContext]];
	[item setValue:name forKey:@"Description"];
    [self saveContext];
   
	
	
	
}
-(NSManagedObject*) AddRisk:(NSString*) name ParentRecord:(Hazard*)hazard

{
    
	Risk* item = [NSEntityDescription insertNewObjectForEntityForName:@"Risk" inManagedObjectContext:[self managedObjectContext]];
	[item setValue:name forKey:@"Description"];
    [self saveContext];
   
    [hazard addRisksObject:item];
    
     [self saveContext];
	return item;
	
}
-(NSManagedObject*) AddRiskGroup:(NSString*) name ParentRecord:(Hazard*)hazard

{
    
	RiskGroup* item = [NSEntityDescription insertNewObjectForEntityForName:@"RiskGroup" inManagedObjectContext:[self managedObjectContext]];
	[item setValue:name forKey:@"Description"];
    [self saveContext];

	
    [hazard addRiskGroupsObject:item];
	return item;
	
}
-(void) AddRecommendControl:(NSString*) description  ParentRecord:(Hazard*)hazard   
{
    
    
    for (Control *existing in hazard.RecommendedControls)
    {
        if ([existing.Description isEqualToString:description] == YES)
            return;
    }
    
    
	Control* control = (Control *)[NSEntityDescription insertNewObjectForEntityForName:@"Control" inManagedObjectContext:[self managedObjectContext]];
	[control setValue:description forKey:@"Description"];
	
	[hazard addRecommendedControlsObject:control];
}
-(void) AddExistingControl:(NSString*) description  ParentRecord:(Hazard*)hazard
{
    
    for (Control *existing in hazard.ExistingControls)
    {
        if ([existing.Description isEqualToString:description] == YES)
            return;
    }
    
    
	Control* control = (Control *)[NSEntityDescription insertNewObjectForEntityForName:@"Control" inManagedObjectContext:[self managedObjectContext]];
	[control setValue:description forKey:@"Description"];
	
	[hazard addExistingControlsObject:control];
	
}

-(Assessment*) CreateAssessment
{
	Assessment* assessment = (Assessment *)[NSEntityDescription insertNewObjectForEntityForName:@"Assessment" inManagedObjectContext:[self managedObjectContext]];
    [assessment setValue:[NSDate date] forKey:@"AssessmentDate"];
	return assessment;
}
-(NSManagedObject*) AddPhoto:(NSString*) name hazard:(Hazard*)hazard
{
    
    
    Photo * item = (Photo *)[NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:[self managedObjectContext]];
    [item setValue:name forKey:@"name"];
    [hazard addPhotosObject:item];
    [self saveContext];
    

    [hazard addPhotosObject:item];
    return item;
	
}

-(void) AddAssessmentTemplate:(NSString*) description  DisplayOrder:(int)order
{
	AssessmentTemplate* item = (AssessmentTemplate *)[NSEntityDescription insertNewObjectForEntityForName:@"AssessmentTemplate" inManagedObjectContext:[self managedObjectContext]];
	[item setValue:description forKey:@"Description"];
    [item setValue:[NSNumber numberWithInt:order]  forKey:@"Order"];
	
	
}
-(void) AddWindDirectionTemplate:(NSString*) description DisplayOrder:(int)order
{
	WindDirectionTemplate* item = (WindDirectionTemplate *)[NSEntityDescription insertNewObjectForEntityForName:@"WindDirectionTemplate" inManagedObjectContext:[self managedObjectContext]];
	[item setValue:description forKey:@"Description"];
    [item setValue:[NSNumber numberWithInt:order]  forKey:@"Order"];
	
	
}
-(void) AddWindSpeedTemplate:(NSString*) description DisplayOrder:(int)order
{
	WindSpeedTemplate* item = (WindSpeedTemplate *)[NSEntityDescription insertNewObjectForEntityForName:@"WindSpeedTemplate" inManagedObjectContext:[self managedObjectContext]];
	[item setValue:description forKey:@"Description"];
    [item setValue:[NSNumber numberWithInt:order]  forKey:@"Order"];
	
}
-(void) AddWaveTypeTemplates:(NSString*) description DisplayOrder:(int)order
{
	WaveTypeTemplate* item = (WaveTypeTemplate *)[NSEntityDescription insertNewObjectForEntityForName:@"WaveTypeTemplate" inManagedObjectContext:[self managedObjectContext]];
	[item setValue:description forKey:@"Description"];
    [item setValue:[NSNumber numberWithInt:order]  forKey:@"Order"];
	
}
-(void) AddSwellSizeTemplates:(NSString*) description DisplayOrder:(int)order
{
	SwellSizeTemplate* item = (SwellSizeTemplate *)[NSEntityDescription insertNewObjectForEntityForName:@"SwellSizeTemplate" inManagedObjectContext:[self managedObjectContext]];
	[item setValue:description forKey:@"Description"];
    [item setValue:[NSNumber numberWithInt:order]  forKey:@"Order"];
	
}

-(void) AddCloudTemplate:(NSString*) description DisplayOrder:(int)order
{
	CloudTemplate* item = (CloudTemplate *)[NSEntityDescription insertNewObjectForEntityForName:@"CloudTemplate" inManagedObjectContext:[self managedObjectContext]];
	[item setValue:description forKey:@"Description"];
    [item setValue:[NSNumber numberWithInt:order]  forKey:@"Order"];
	
}
-(void) AddTideTemplate:(NSString*) description  DisplayOrder:(int)order
{
	TideTemplate* item = (TideTemplate *)[NSEntityDescription insertNewObjectForEntityForName:@"TideTemplate" inManagedObjectContext:[self managedObjectContext]];
	[item setValue:description forKey:@"Description"];
    [item setValue:[NSNumber numberWithInt:order]  forKey:@"Order"];
	
	
}
-(NSArray*) GetEventTemlates
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:@"EventTemplate" inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
   //Fetch the results
	
	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);
	return FetchResults;
	
	
	//return the Array
	
}
-(NSArray*) GetDataArray:(NSString*) tableName
{
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
	
	
	//*****************
	//Load the settings from the Data Base.  NB, there should only be one row in the settings table but multiple tax items
	//If the DB is empty then create a new record with a single Tax Item
	
	//retrieve the events from the db
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *appSet = [NSEntityDescription entityForName:tableName inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:appSet];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"Order" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
	//Fetch the results
	NSError *error;
	NSArray *FetchResults = [[self managedObjectContext] executeFetchRequest:request error:&error];
	if (FetchResults == nil) {
		// Handle the error.
	}
	
	//if no results returned then we need to create a new record
	if([FetchResults count]==0){
		
		////NSLog(@"No Member");
		
		
		
	}
	
	
	
	
	//*****************
	////NSLog(@"Get members for id: %@ finished", id);
	return FetchResults;
	
	
	//return the Array
	
}

#pragma mark - GPS
- (void)locationUpdate:(CLLocation *)location
{
    accuracy = location.horizontalAccuracy;
    longField  = location.coordinate.longitude;
    latField  =  location.coordinate.latitude;
    
    
    //	   [self.tableView reloadData];
}

- (void)locationError:(NSError *)error {
}
-(UIImage*)scaledToSize:(UIImage*)sourceImage newSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [sourceImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (UIImage*)imageWithImage:(UIImage*)sourceImage scaledToSizeWithSameAspectRatio:(CGSize)targetSize
{
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor) {
            scaleFactor = widthFactor; // scale to fit height
        }
        else {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor < heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    CGImageRef imageRef = [sourceImage CGImage];
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
    CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
    
    if (bitmapInfo == kCGImageAlphaNone) {
        bitmapInfo = kCGImageAlphaNoneSkipLast;
    }
    
    CGContextRef bitmap;
    
    if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown) {
        bitmap = CGBitmapContextCreate(NULL, targetWidth, targetHeight, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, kCGImageAlphaNoneSkipLast);
        
    } else {
        bitmap = CGBitmapContextCreate(NULL, targetHeight, targetWidth, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, kCGImageAlphaNoneSkipLast);
        
    }
    
    // In the right or left cases, we need to switch scaledWidth and scaledHeight,
    // and also the thumbnail point
    if (sourceImage.imageOrientation == UIImageOrientationLeft) {
        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
        CGFloat oldScaledWidth = scaledWidth;
        scaledWidth = scaledHeight;
        scaledHeight = oldScaledWidth;
        
        CGContextRotateCTM (bitmap, radians(90));
        CGContextTranslateCTM (bitmap, 0, -targetHeight);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationRight) {
        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
        CGFloat oldScaledWidth = scaledWidth;
        scaledWidth = scaledHeight;
        scaledHeight = oldScaledWidth;
        
        CGContextRotateCTM (bitmap, radians(-90));
        CGContextTranslateCTM (bitmap, -targetWidth, 0);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationUp) {
        // NOTHING
    } else if (sourceImage.imageOrientation == UIImageOrientationDown) {
        CGContextTranslateCTM (bitmap, targetWidth, targetHeight);
        CGContextRotateCTM (bitmap, radians(-180.));
    }
    
    CGContextDrawImage(bitmap, CGRectMake(thumbnailPoint.x, thumbnailPoint.y, scaledWidth, scaledHeight), imageRef);
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage* newImage = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);
    CGImageRelease(ref);
    
    return newImage;
}
-(void)CopyAssessment:(NSManagedObject *)parentRecord AssessmentRecord:(NSManagedObject *)record
{
    
    Assessment* assessment  = (Assessment*)record;
    LocalSite* localSite  = (LocalSite*)parentRecord;
    Assessment* copyassessment = (Assessment *)[NSEntityDescription insertNewObjectForEntityForName:@"Assessment" inManagedObjectContext:[self managedObjectContext]];
    [copyassessment setValue:[NSDate date] forKey:@"AssessmentDate"];
    [copyassessment setValue:[assessment Description] forKey:@"Description"];
    [copyassessment setValue:[assessment CurrentWindDirection] forKey:@"CurrentWindDirection"];
    [copyassessment setValue:[assessment CurrentWindSpeed] forKey:@"CurrentWindSpeed"];
    [copyassessment setValue:[assessment Type] forKey:@"Type"];
    [copyassessment setValue:[assessment OverviewPhoto] forKey:@"OverviewPhoto"];
    [copyassessment setValue:[assessment CurrentWeatherTemp] forKey:@"CurrentWeatherTemp"];
    [copyassessment setValue:[assessment PredictionWeatherTemp] forKey:@"PredictionWeatherTemp"];
    [copyassessment setValue:[assessment CurrentCloud] forKey:@"CurrentCloud"];
    [copyassessment setValue:[assessment PredictionCloud] forKey:@"PredictionCloud"];
    [copyassessment setValue:[assessment PredictionWindDirection] forKey:@"PredictionWindDirection"];
    [copyassessment setValue:[assessment PredictionWindSpeed] forKey:@"PredictionWindSpeed"];
    [copyassessment setValue:[assessment CurrentSwellSize] forKey:@"CurrentSwellSize"];
    [copyassessment setValue:[assessment CurrentWaveType] forKey:@"CurrentWaveType"];
    [copyassessment setValue:[assessment CurrentTide] forKey:@"CurrentTide"];
    [copyassessment setValue:[assessment CurrentSwellTime] forKey:@"CurrentSwellTime"];
    [copyassessment setValue:[assessment PredictionLowTide] forKey:@"PredictionLowTide"];
    [copyassessment setValue:[assessment PredictionHighTide] forKey:@"PredictionHighTide"];
    
    [copyassessment setValue:[assessment PredictionWaveType] forKey:@"PredictionWaveType"];
    [copyassessment setValue:[assessment PredictionSwellTime] forKey:@"PredictionSwellTime"];
    [copyassessment setValue:[assessment PredictionSwellSize] forKey:@"PredictionSwellSize"];
    if ([assessment Events] != nil && [[assessment Events] count] > 0)
    {
        NSArray *eventItems = [[assessment Events] allObjects];
        Event* newitem;
        for(Event* item in eventItems)
        {
            newitem = (Event *)[NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:[self managedObjectContext]];
            [newitem setValue:[item name] forKey:@"name"];
            [copyassessment addEventsObject:newitem];
        }
    }
    if ([assessment Hazzards] != nil && [[assessment Hazzards] count] > 0)
    {
        NSArray *hazzardsItems = [[assessment Hazzards] allObjects];
        Hazard*  newitem;
        for(Hazard* item in hazzardsItems)
        {
            newitem = (Hazard *)[NSEntityDescription insertNewObjectForEntityForName:@"Hazard" inManagedObjectContext:[self managedObjectContext]];
            [newitem setValue:[item Description] forKey:@"Description"];
            [newitem setValue:[item RiskScoreConsequence] forKey:@"RiskScoreConsequence"];
            [newitem setValue:[item RiskScore] forKey:@"RiskScore"];
            [newitem setValue:[item Latitute] forKey:@"Latitute"];
            [newitem setValue:[item ResidualRiskScoreConsequence] forKey:@"ResidualRiskScoreConsequence"];
            [newitem setValue:[item ResidualRiskScoreLikelihood] forKey:@"ResidualRiskScoreLikelihood"];
            [newitem setValue:[item ResidualRiskScore] forKey:@"ResidualRiskScore"];
            [newitem setValue:[item Longitude] forKey:@"Longitude"];
            [newitem setValue:[item RiskScoreLikelihood] forKey:@"RiskScoreLikelihood"];
            if ([item RiskGroups] != nil && [[item RiskGroups] count] > 0)
            {
                NSArray *riskGroupsItems = [[item RiskGroups] allObjects];
                RiskGroup*  newriskGroupitem;
                for(RiskGroup* riskGroupitem in riskGroupsItems)
                {
                    newriskGroupitem = (RiskGroup *)[NSEntityDescription insertNewObjectForEntityForName:@"RiskGroup" inManagedObjectContext:[self managedObjectContext]];
                    [newriskGroupitem setValue:[riskGroupitem Description] forKey:@"Description"];
                    [newitem addRiskGroupsObject:newriskGroupitem];
                }
            }
            if ([item ExistingControls] != nil && [[item ExistingControls] count] > 0)
            {
                NSArray *existingControlsItems = [[item ExistingControls] allObjects];
                
                Control*  newcontrolitem;
                for(Control* controlitem in existingControlsItems)
                {
                    newcontrolitem = (Control *)[NSEntityDescription insertNewObjectForEntityForName:@"Control" inManagedObjectContext:[self managedObjectContext]];
                    [newcontrolitem setValue:[controlitem Description] forKey:@"Description"];
                    [newcontrolitem setValue:[controlitem Effectiveness] forKey:@"Effectiveness"];
                    
                    [newitem addExistingControlsObject:newcontrolitem];
                }
            }
            if ([item RecommendedControls] != nil && [[item RecommendedControls] count] > 0)
            {
                NSArray *existingControlsItems = [[item RecommendedControls] allObjects];
                
                Control*  newcontrolitem;
                for(Control* controlitem in existingControlsItems)
                {
                    newcontrolitem = (Control *)[NSEntityDescription insertNewObjectForEntityForName:@"Control" inManagedObjectContext:[self managedObjectContext]];
                    [newcontrolitem setValue:[controlitem Description] forKey:@"Description"];
                    [newcontrolitem setValue:[controlitem Effectiveness] forKey:@"Effectiveness"];
                    
                    [newitem addRecommendedControlsObject:newcontrolitem];
                }
            }
            if ([item Risks] != nil && [[item Risks] count] > 0)
            {
                NSArray *existingriskItems = [[item Risks] allObjects];
                
                Risk*  newriskitem;
                for(Risk* riskitem in existingriskItems)
                {
                    newriskitem = (Risk *)[NSEntityDescription insertNewObjectForEntityForName:@"Risk" inManagedObjectContext:[self managedObjectContext]];
                    [newriskitem setValue:[riskitem Description] forKey:@"Description"];
                    [newitem addRisksObject:newriskitem];
                }
            }
            
            [copyassessment addHazzardsObject:newitem];
        }
    }
    [localSite addAssessmentsObject:copyassessment];
    [self saveContext];
}

@end
