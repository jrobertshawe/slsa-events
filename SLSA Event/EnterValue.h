//
//  EnterValue.h
//  SLSARiskAssessment
//
//  Created by Julian on 16/03/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "assessment.h"

@interface EnterValue : UITableViewController {
    IBOutlet UITextField			*nameField;
        IBOutlet UITextView		*nameView;
    int editkeyboardtype;
    NSManagedObject *selectedObject;
    NSString *promptText;
   NSString *titleText;
    NSString *recordkey;
    Boolean isNotes;
}
@property (nonatomic, retain) IBOutlet UITextField *nameField;
@property  int  editkeyboardtype;
@property  Boolean  isNotes;
@property (nonatomic, retain) NSString *promptText;
@property (nonatomic, retain) NSString *titleText;
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityLabel:(NSString*) entityLabel;
@end
