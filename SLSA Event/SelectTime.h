//
//  SelectTime.h
//  SLSARiskAssessment
//
//  Created by Julian on 25/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

@interface SelectTime : UIViewController {
    IBOutlet UIDatePicker            *datePicker;
	NSInteger	timeType;
    IBOutlet UILabel				*timeValue;
    IBOutlet UILabel				*timeLabel;
    NSManagedObject *selectedObject;
    NSString *promptText;
    NSString *titleText;
    NSString *recordkey;
     NSString *recordValuekey;
    Boolean isNotes;
}

@property (nonatomic, retain) NSString *promptText;
@property (nonatomic, retain) NSString *titleText;
@property NSInteger	timeType;
@property (nonatomic, retain) UIDatePicker *datePicker;
@property (nonatomic, retain) IBOutlet UILabel				*timeValue;
@property (nonatomic, retain) IBOutlet UILabel				*timeLabel;
-(IBAction)dateChanged;
-(IBAction)done;
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityKeyStringValue:(NSString*) entityKeyStringValue;

@end
