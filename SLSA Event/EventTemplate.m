//
//  EventTemplate.m
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventTemplate.h"


@implementation EventTemplate

@dynamic Order;
@dynamic Description;

@end
