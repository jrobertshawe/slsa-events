//
//  EventViewControllerAssessment.m
//  SLSA Event
//
//  Created by Julian robertshawe on 20/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerAssessment.h"
#import "EventViewControllerHazards.h"
@interface EventViewControllerAssessment ()

@end

@implementation EventViewControllerAssessment
@synthesize _tableView;
@synthesize footerView;
-(void)setupData:(LocalSite*) entityObject
{
    site = entityObject;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *navBar;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        navBar = @"ipad_img_blue-header.png";
    }
    else {
        navBar = @"iphone_img_blueheader.png";
    }
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:navBar];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonImage = [UIImage imageNamed:@"but_copy_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(CopyMode) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.RightBarButtonItem = rightButtonItem;
    
    
    
    Items = [[NSMutableArray alloc] init];
  
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"refreshView"
     object:nil];
    isCopying = NO;
       EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    delegate.areaViewController = self;
    
}
-(void)handleNotification:(NSNotification *)pNotification
{
     [self dismissModalViewControllerAnimated:YES];
    [self._tableView reloadData];
}
- (void)TurnOffCopy {
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_copy_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(CopyMode) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.RightBarButtonItem = rightButtonItem;
    isCopying = NO;

}
- (void)CopyMode {
    if (isCopying == YES)

	{
        [self TurnOffCopy];
			}
	
	else
	{
		UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage* buttonImage = [UIImage imageNamed:@"but_copy_on.png"];
        [backButton setImage:buttonImage forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(CopyMode) forControlEvents:UIControlEventTouchUpInside];
        backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
        self.navigationItem.RightBarButtonItem = rightButtonItem;
        isCopying = YES;
		
	}
}

- (void) didTapBackButton:(id)sender {
    
    
    
    
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
- (void) viewWillAppear:(BOOL)animated
{
    [_tableView reloadData];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}
#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav2.png",delegate.imagePrefix];
    
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    
    [hv addSubview:image];
    //  hv.backgroundColor = [UIColor whiteColor];
    tableView.tableHeaderView = hv;
    [Items removeAllObjects];
	[Items addObjectsFromArray:[site.Assessments allObjects]];
    [ Items insertObject:@"Add Assessment"  atIndex:0];    //add temp line to
    hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,500)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 000, self._tableView.bounds.size.width-40, 420)];
    label.text = @"Select the assessment type to begin the assessment. Or select detail to enter updated area name.    \nYou may also copy an existing assessment if conditions are similar or unchanged. To do this:        \n1. Press the 'copy' button - top right corner of screen       \n2. Press the assessment record that you wish to copy        \n3. A new assessment record will appear showing the current date and time.        \n4. Select the new assessment record to validate the copied assessment and make any necessary modifications.";
    self.footerView.backgroundColor = [UIColor clearColor];
    hv.backgroundColor  = [UIColor clearColor];
    label.backgroundColor  = [UIColor clearColor];
    label.textColor =  [UIColor lightGrayColor];
    label.numberOfLines = 70;
    [hv addSubview:label];
    CGRect newFrame = CGRectMake(0.0, 0.0, self._tableView.bounds.size.width,700);
    hv.frame = newFrame;
    tableView.tableFooterView = hv;

    
    
    return 2;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)] ;
	tableView.sectionHeaderHeight = headerView.frame.size.height;
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, headerView.frame.size.width - 20, 22)] ;
	label.text = [self tableView:tableView titleForHeaderInSection:section];
	label.font = [UIFont boldSystemFontOfSize:16.0];
	label.shadowOffset = CGSizeMake(0, 1);
	label.shadowColor = [UIColor blackColor];
	label.backgroundColor = [UIColor clearColor];
    
	label.textColor = [UIColor whiteColor];
    
	[headerView addSubview:label];
	return headerView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if (section == 1)
        return [Items count];
    else
        return 1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *titleString =@"";
	switch ( section)
    {
        case 0:
            titleString =  @"Area";
            break;
        case 1:
            titleString =  @"Assessments";
            break;
    }
    return titleString;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger row = [indexPath row];
    NSUInteger section = [indexPath section];
    NSUInteger sectionRow = (section * 10) + row;
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    }

    
    if (section == 0)
    {
        cell.textLabel.text = @"Details";
        cell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
    }
    
    else
        [self configureCell:cell atIndexPath:indexPath];
    
    
    
      
    // Configure the cell.
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSString class]])
    {
        NSString* stringObject = (NSString*)managedObject;
        cell.textLabel.text = stringObject;
          cell.textLabel.textColor = [UIColor lightGrayColor];
             cell.accessoryType =    UITableViewCellAccessoryNone;
    }
    else
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterMediumStyle];
                cell.textLabel.textColor = [UIColor blackColor];
        Assessment *assessment = (Assessment*)managedObject;
        NSDate * assementDate = [assessment valueForKey:@"AssessmentDate"];

        if (assessment.Description)
            cell.textLabel.text =assessment.Description;
        cell.detailTextLabel.text = [formatter stringFromDate:assementDate];
             cell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([indexPath section] == 0)
    {
      //   [self TurnOffCopy];
        [self performSegueWithIdentifier:@"AreaDetails" sender: nil];
        return;
    }
    
    
    if ([managedObject isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
	{
        
        
         [self TurnOffCopy];
        SelectItemItem *popoverContent = [[SelectItemItem alloc] initWithNibName:@"SelectItemItem" bundle:nil];
        popoverContent.parentController  = self;
        [popoverContent setupData:site entityKeyString:@"Assessments" entityselectListType:SELECTLISTTEMPLATE entitySelectList:@"AssessmentTemplate" parentQueue:@"refreshView"];
        popoverContent.label = @"Add Assessment";
        //resize the popover view shown
        //in the current view to the view's size
        popoverContent.contentSizeForViewInPopover =
        CGSizeMake(500, 500);
        
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 520.0, 100);
        
        //create a popover controller
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.08627450980392 green:0.15686274509804 blue:0.20392156862745 alpha:1];
        
        navigationController.view.frame = CGRectMake(0.0, -10.0, 300, 504);
        //release the popover content
        //[popoverContent release];
        [self presentModalViewController:navigationController animated:YES];
        //release the popover content
        

        
        
        
        
       	/*AssessmentTemplates*  assessmentTemplates = [[AssessmentTemplates alloc] initWithNibName: @"AssessmentTemplates" bundle:nil];
        [assessmentTemplates setupData:site];
	
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:assessmentTemplates] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        toolsNavController.title = @"Assessment type";
        [self presentModalViewController:toolsNavController animated:YES];
         */
        
    }
	else
	{
         
        if (isCopying == YES)
        {
            [self TurnOffCopy];
            EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
            [delegate CopyAssessment:site AssessmentRecord:managedObject];
            isCopying = NO;
            [self._tableView reloadData];
            return;
        }
        [self performSegueWithIdentifier:@"Assessment" sender: nil];
    }
    
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        return UITableViewCellEditingStyleDelete;
    }
    else
        return UITableViewCellEditingStyleNone;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        return YES;
    }
    else
        return NO;
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        [delegate.managedObjectContext deleteObject:managedObject];
        
     
        [delegate saveContext];
        [tableView reloadData];
	}
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    NSManagedObject *item = [Items objectAtIndex:[self._tableView.indexPathForSelectedRow row]];
    if ([segue.identifier isEqualToString:@"AreaDetails"] == YES)
    {
    EventViewControllerAreaDetail *ev = (EventViewControllerAreaDetail *)[segue destinationViewController];
    
    [ev setupData:site];
    }
    else if ([segue.identifier isEqualToString:@"Assessment"] == YES)
        
    {
        EventViewControllerAssessmentMenu *ev = (EventViewControllerAssessmentMenu *)[segue destinationViewController];
        
        [ev setupData:item localSite:site];
        
    }
}
@end
