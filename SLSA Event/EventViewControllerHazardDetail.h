//
//  EventViewControllerHazardDetail.h
//  SLSA Event
//
//  Created by Julian robertshawe on 1/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SingleEditCell.h"
#import "MultiLineEditCell.h"
#import "SingleLineListItemCell.h"
#import "SingleLineListControlCell.h"
#import "SingleImageCell.h"
#import "hazard.h"
#import "LocationCell.h"
#import "ChooseImage.h"

@interface EventViewControllerHazardDetail  : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSString* objectString;
    NSString*               notifyQueue;
    NSString*               parentViewQueue;
    NSMutableArray								*Items;
	IBOutlet UITableView		*_tableView;
    UIPopoverController *popoverController;
    NSMutableArray              *risks;
    NSMutableArray              *riskgroups;
    NSMutableArray              *existingControls;
    NSMutableArray              *recommendControls;
    NSMutableArray              *photos;
    Hazard *hazardItem;
    Assessment* assessment;
    bool GPSon;
}
@property (nonatomic, retain) UITableView		*_tableView;
- (void)EditMode;
-(void)setupData:(NSManagedObject*) entityObject assessment:(Assessment*) assessment;
-(void)handleNotification:(NSNotification *)pNotification;
- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;
-(void)AddPopover:(NSString*)listName entityKey:(NSString*) entityKey forRowAtIndexPath:(NSIndexPath *)indexPath selectListType:(int)selectListType  title:(NSString*) title;
- (void)DonePressed;
-(void)handleNotificationGPSOff:(NSNotification *)pNotification;
-(void)handleNotificationGPSOn:(NSNotification *)pNotification;
@end