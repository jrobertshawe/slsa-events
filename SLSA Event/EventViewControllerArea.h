//
//  EventViewControllerArea
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"
#import "EventViewControllerAssessment.h"
#import "EventViewControllerEventDetail.h"
@interface EventViewControllerArea : UIViewController< UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate >
{
      
    NSMutableArray								*Items;
    GovernmentArea*                                      area;
    IBOutlet UITextField			*nameField;
    IBOutlet UITextField			*descriptionField;
    IBOutlet UITextField			*contactNameField;
    IBOutlet UITextField			*addressField;
    IBOutlet UITextField			*phoneNumberField;

}
@property (nonatomic, retain) IBOutlet UITextField *nameField;
@property (nonatomic, retain) IBOutlet UITextField *addressField;
@property (nonatomic, retain) IBOutlet UITextField *contactNameField;
@property (nonatomic, retain) IBOutlet UITextField *phoneNumberField;
@property (nonatomic, retain) IBOutlet UITextField *descriptionField;
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (nonatomic, retain) IBOutlet UIView		*footerView;
-(void)setupData:(GovernmentArea*) entityObject;
- (void) didTapBackButton:(id)sender;
@property CGSize keyboardSize;

@property (strong, nonatomic) UITextField *currentTextField;
@property (nonatomic) int addressSection;

- (BOOL)fieldIsInHeader:(UITextField *)textField;
- (void)cancelRequest;
- (void)insertNewObject;
- (void)registerForKeyboardNotifications;
@end
