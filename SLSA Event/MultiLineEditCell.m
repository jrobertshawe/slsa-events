//
//  MultiLineEditCell.m
//  SLSA Risk Assessment
//
//  Created by Julian on 21/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "MultiLineEditCell.h"
#import "AussieAppDelegate.h"

@implementation MultiLineEditCell
@synthesize celllabelText;
@synthesize cellDetailText,updateButton;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setupData:(NSString*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString
{
    AussieAppDelegate *delegate = (AussieAppDelegate* )[[UIApplication sharedApplication] delegate];
     NSManagedObject* entity;
    if (entityObject != nil)
    {
        entityString = [[NSString alloc] initWithString:entityObject];
        entityKey = [[NSString alloc] initWithString:entityKeyString];

        NSURL* tempURL = [[NSURL alloc] initWithString:entityObject];
        entity = [delegate objectWithURI:tempURL];
         cellDetailText.text = [entity valueForKey:entityKey];
        
    }
     celllabelText.text = entityLabelString;
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:entityKey
     object:nil];   
     [updateButton addTarget:self action:@selector(UpdateButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)handleNotification:(NSNotification *)pNotification
{
    AussieAppDelegate *delegate = (AussieAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSManagedObject* entity;
    if (entityString != nil)
    {
        NSURL* tempURL = [[NSURL alloc] initWithString:entityString];
        entity = [delegate objectWithURI:tempURL];
     }

   if([popoverController isPopoverVisible])
    {
        
        //close the popover view         
        [popoverController dismissPopoverAnimated:YES];
        cellDetailText.text = [entity valueForKey:entityKey];
        
    }
    
}
-(IBAction)UpdateButtonPressed:(id)sender
{
    if([popoverController isPopoverVisible])
    {
        //close the popover view if toolbar button was touched
        //again and popover is already visible
        
        [popoverController dismissPopoverAnimated:YES];
        return;
    }
    
    UIButton *button = (UIButton*) sender;
    
    EditMultiLineText* popoverContent = [[EditMultiLineText alloc]
                                          init];
    [popoverContent setupData:entityString entityKeyString:entityKey entityLabelString:celllabelText.text notifyQueueString:entityKey];
    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.contentSizeForViewInPopover =
    CGSizeMake(530, 262);
    
    //create a popover controller
    UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
    toolsNavController.view.frame = CGRectMake(0.0, -10.0, 320.0, 100);
    
    //create a popover controller
    popoverController = [[UIPopoverController alloc]
                         initWithContentViewController:toolsNavController];
    //present the popover view non-modal with a
    //refrence to the toolbar button which was pressed
    CGRect buttonFrameInDetailView = button.frame;
    [popoverController presentPopoverFromRect:buttonFrameInDetailView
                                       inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
   
}
@end