//
//  SingleLineListItemCell.m
//  SLSA Risk Assessment
//
//  Created by Julian on 24/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "SingleLineListItemCell.h"
#import "EventAppDelegate.h"

@implementation SingleLineListItemCell
@synthesize cellDetailText;
@synthesize cellDateText;
@synthesize cellStatusText;
@synthesize cellDescriptionText;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)setupData:(NSManagedObject*) entityObject 
{
 
    if ([entityObject isKindOfClass:[NSString class]])
    {
           
        NSString* labelText = (NSString*) entityObject;
       cellDetailText.text  = labelText;
           cellDetailText.textColor = [UIColor lightGrayColor];
       
    }
    else if ([entityObject isKindOfClass:[Assessment class]])
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        
        cellDetailText.textColor = [UIColor blackColor];
        
        cellDetailText.text  = [entityObject valueForKey:@"Description"];
        
        NSDate * assementDate = [entityObject valueForKey:@"AssessmentDate"];
        cellDateText.text = [formatter stringFromDate:assementDate];
        self.editingAccessoryType = UITableViewCellAccessoryNone;
        
        self.editingAccessoryType = UITableViewCellAccessoryNone;
        cellDescriptionText.text = @"Type";
    }
    else if ([entityObject isKindOfClass:[LocalSite class]])
    {
        cellDetailText.textColor = [UIColor blackColor];
        
        cellDetailText.text  = [entityObject valueForKey:@"Name"];
        
        
        self.editingAccessoryType = UITableViewCellAccessoryNone;

    }
    else if ([entityObject isKindOfClass:[Hazard class]])
    {
        cellDetailText.textColor = [UIColor blackColor];
        
        cellDetailText.text  = [entityObject valueForKey:@"Description"];
        
        cellStatusText.text  = [entityObject valueForKey:@"Status"];
        
        
        self.editingAccessoryType = UITableViewCellAccessoryNone;
        
    }
    else
    {
        cellDetailText.textColor = [UIColor blackColor];
      
           cellDetailText.text  = [entityObject valueForKey:@"Description"];
          
      
         self.editingAccessoryType = UITableViewCellAccessoryNone;
    }

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
