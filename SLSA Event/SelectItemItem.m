//
//  SelectItemItem.m
//  SLSA Risk Assessment
//
//  Created by Julian on 20/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "SelectItemItem.h"
#import "EventAppDelegate.h"
#import "hazard.h"
@implementation SelectItemItem
@synthesize tableview;
@synthesize selectView;
@synthesize contactDetailsView;
@synthesize name;
@synthesize company;
@synthesize phone;
@synthesize position;
@synthesize address;
@synthesize label;
@synthesize rightButton;
@synthesize notifyQueue;
@synthesize selectListType;
@synthesize parentController;
@synthesize keyboardSize;
@synthesize currentTextField;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityselectListType:(int) entityselectListType  entitySelectList:(NSString*) entitySelectList  parentQueue:(NSString*) parentQueue
{
    selectedObject = entityObject;
    
    entityKey = entityKeyString;
    selectList = entitySelectList;
    selectListType = entityselectListType;
    notifyQueue = parentQueue;

    
    
    
    
     selectList = entitySelectList;
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"SelectItemItem"
     object:nil];  


}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
     [super viewDidLoad];
 
    
    NSString *navBar;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        navBar = @"ipad_img_blue-header.png";
    }
    else {
        navBar = @"iphone_img_blueheader.png";
        CGRect newFrame = CGRectMake(0.0, 0.0, self.tableview.bounds.size.width,700);
    //    self.tableview.frame = self.tableview.bounds.size.width;
        
    }
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:navBar];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
    
    
    items = [[NSMutableArray alloc] init];
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];

    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(EditButtonPressed)];
  //  self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(DoneButtonPressed)];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    self.navigationItem.title = label;
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"SelectItemItem"
     object:nil];  
  
    [tableview reloadData];
    if (selectListType == SELECTLISTCONTACT)
    self.navigationItem.title = @"Select Contact";
    
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
    
    
}
-(void)handleNotification:(NSNotification *)pNotification
{
    if([popoverController isPopoverVisible])
    {
        //close the popover view         
        [popoverController dismissPopoverAnimated:YES];
        
        
    }
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    [items removeAllObjects];
    [items addObjectsFromArray:[delegate GetDataArray:selectList]];

    [self.tableview reloadData]; 
    
} 
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(IBAction)DoneButtonPressed
{
    [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];
}

-(IBAction)EditButtonPressed
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    if ([self.tableview isEditing] == NO)
    {
        
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(EditButtonPressed)];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
         
        [self.tableview setEditing:YES animated:YES];
        [ items insertObject:@"Add new item" atIndex:0];   //add temp line to allow adding items
        [tableview reloadData];
    }
    else
    {
        
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(EditButtonPressed)];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        
        [self.tableview setEditing:NO animated:YES];
        [items removeAllObjects];
        [items addObjectsFromArray:[delegate GetDataArray:selectList]];

        [tableview reloadData];
    }
}
#pragma mark -
#pragma mark Table view data source





#pragma mark -
#pragma mark Table view data source
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
    // Configure the cell.
    NSManagedObject* item = [items objectAtIndex:row];
    if ([item isKindOfClass:[NSString class]])
    {
        return UITableViewCellEditingStyleNone;
    }
    else
    {
        return UITableViewCellEditingStyleDelete;
    }

    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    NSManagedObject* item = [items objectAtIndex:row];
        EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
       NSString * itemString = [[[item objectID] URIRepresentation] absoluteString];
    NSManagedObject* copyItem;
    if ([entityKey isEqualToString:@"Risks"] == YES)
        {
            [delegate AddRisk:[item valueForKey:@"Description"] ParentRecord:selectedObject];
        }
        else if ([entityKey isEqualToString:@"RiskGroups"] == YES)
        {
            [delegate AddRiskGroup:[item valueForKey:@"Description"] ParentRecord:selectedObject];
            
        }
        else if ([entityKey isEqualToString:@"ExistingControls"] == YES)
        {
            [delegate AddExistingControl:[item valueForKey:@"Description"] ParentRecord:selectedObject];
            
            
           }
        else if ([entityKey isEqualToString:@"RecommendedControls"] == YES)
        {
            [delegate AddRecommendControl:[item valueForKey:@"Description"] ParentRecord:selectedObject];
            
   
         }
        else if ([entityKey isEqualToString:@"Assessments"] == YES)
        {
       
            Assessment* assessmentObject = (Assessment *)[NSEntityDescription insertNewObjectForEntityForName:@"Assessment" inManagedObjectContext:[delegate managedObjectContext]];
            [assessmentObject setValue:[NSDate date] forKey:@"AssessmentDate"];
            [assessmentObject setValue:[item valueForKey:@"Description"]  forKey:@"Description"];
            LocalSite* site  = (LocalSite*)selectedObject;
            [site addAssessmentsObject:assessmentObject];
        }
        else if ([entityKey isEqualToString:@"Hazzards"] == YES)
        {
            

            
            
            Hazard* hazardObject = (Hazard *)[NSEntityDescription insertNewObjectForEntityForName:@"Hazard" inManagedObjectContext:[delegate managedObjectContext]];
            [hazardObject setValue:[item valueForKey:@"Description"]  forKey:@"Description"];
            Assessment* assessment  = (Assessment*)selectedObject;
            [assessment addHazzardsObject:hazardObject];
            
        }

        else
        {
             [selectedObject setValue:[item valueForKey:@"Description"] forKey:entityKey];
            
        }
    [delegate saveContext];
        		[[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];

}
// Update the data model according to edit actions delete or insert.
- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) 
    {
        
         NSUInteger row = [indexPath row];
        EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
         NSManagedObject* item = [items objectAtIndex:row];
        if ([item isKindOfClass:[NSManagedObject class]])
        {
            [[delegate managedObjectContext] deleteObject:item];
            [items removeObjectAtIndex:indexPath.row];
            [tableview reloadData];
        }
    } 
    else if (editingStyle == UITableViewCellEditingStyleInsert) 
    {
       
        //UITableViewCell *cell = [aTableView cellForRowAtIndexPath:indexPath];
       
       
        
        
            
     /*   EditSingleLineText* popoverContent = [[EditSingleLineText alloc] initWithNibName:@"EditSingleLineText" bundle:nil];
        
        [popoverContent setupData:nil entityKeyString:selectList entityLabelString:label parent:nil];

        popoverContent.notifyQueue = @"SelectItemItem";
        popoverContent.isTemplate = YES;
       //resize the popover view shown
        //in the current view to the view's size
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 320.0, 100);
        
        
        //present the popover view non-modal with a
        //refrence to the toolbar button which was pressed
        toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        [parentController presentModalViewController:toolsNavController animated:YES];
*/
       
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    [items removeAllObjects];
    [items addObjectsFromArray:[delegate GetDataArray:selectList]];
    [ items insertObject:@"Add new item" atIndex:0];   //add temp line to allow adding items
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
       return [items count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSUInteger row = [indexPath row];
    // Configure the cell.
    NSManagedObject* item = [items objectAtIndex:row];
    int editFieldWidth;
    if ([item isKindOfClass:[NSString class]])
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            editFieldWidth =460;
        }
        else
        {
            editFieldWidth =280;
        }

        cell.textLabel.text  = nil;
        UITextField *textField  = [[UITextField alloc] initWithFrame:CGRectMake(10,0, editFieldWidth  , 44)];
        textField.clearButtonMode = UITextFieldViewModeAlways;
        //	[textField addTarget:self action:@selector(textFieldDoneEditing:) forControlEvents:UIControlEventEditingDidEndOnExit];
        textField.textAlignment = UITextAlignmentLeft;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.returnKeyType = UIReturnKeyDone;
        textField.placeholder = @"Enter new item";
        textField.tag = 1;
        [textField addTarget:self
                      action:@selector(textFieldFinished:)  forControlEvents:UIControlEventEditingDidEndOnExit];
        [textField setDelegate:self];
        [cell.contentView addSubview:textField];
         [self scrollIntoView:textField];
        currentTextField = textField;
    }
    else
    {
        UITextField* textf  = [cell viewWithTag:1];
        if (textf != nil)
            textf.hidden = YES;
        cell.textLabel.text =  [item valueForKey:@"Description"];
    }
    
    return cell;
}
- (IBAction)textFieldFinished:(id)sender
{
   
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    [delegate AddTemplate:currentTextField.text templateTable:selectList];
    
    [currentTextField removeFromSuperview] ;
    [currentTextField resignFirstResponder];
     
    currentTextField = nil;
    [tableview reloadData];
  //  [self EditButtonPressed];
    
    
    // [sender resignFirstResponder];
}
- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    currentTextField = textField;
    [self scrollIntoView:textField];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 15)
        textField.tag = 9;
    [[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
    [self scrollIntoView:textField];
    return YES;
}
- (void)scrollIntoView:(UITextField *)textField {
    if ((keyboardSize.height != 0) && (![self fieldIsInHeader:textField])) {
        
        [self.tableview scrollToRowAtIndexPath:[self indexPathForField:textField] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}
///
/// Only use this function for rows in the table SECTIONS not the HEADER
///
- (NSIndexPath *)indexPathForField:(UITextField *)theTextField {
    
    //set a default value
    NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
   
    
    return currentIndexPath;
}

#pragma mark - Keyboard Delegate

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)deregisterFortKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWillShow:(NSNotification*)notification
{
    
    NSDictionary* info = [notification userInfo];
    keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
    //
    [UIView animateWithDuration:[[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]
                          delay:0
                        options:[[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]
                     animations:^{
                         //                         UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
                         //                         self._tableView.contentInset = contentInsets;
                         //                         self._tableView.scrollIndicatorInsets = contentInsets;
                         CGRect frame = self.tableview.frame;
                         frame.size.height -= keyboardSize.height;
                         self.tableview.frame = frame;
                     }
                     completion:^(BOOL done){
                         if ([self fieldIsInHeader:currentTextField]) {
                             return;
                         } else {
                             [self scrollIntoView:currentTextField];
                         }
                         
                     }
     ];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    //Clear the keyboard size variable
    keyboardSize.height = 0;
    keyboardSize.width = 0;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableview.contentInset = contentInsets;
    self.tableview.scrollIndicatorInsets = contentInsets;
}
- (BOOL)fieldIsInHeader:(UITextField *)textField {
    
      return NO;
}

    
@end
