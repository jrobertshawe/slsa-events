//
//  HazardTemplateDetails.m
//  SLSARiskAssessment
//
//  Created by Julian on 14/12/10.
//  Copyright 2010 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "HazardTemplateDetails.h"
#import "HazardTemplate.h"

@implementation HazardTemplateDetails
@synthesize descriptionField;


#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
	
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelRequest)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(insertNewObject)];
    self.navigationItem.rightBarButtonItem = doneButton;
  	self.navigationItem.title = @"Add";
 
	 
		if ([selectedObject isKindOfClass:[HazardTemplate class]]) //It is a risk object so update data
		{
			self.navigationItem.title = @"Update";
		}
    
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
    
    
	
}
-(void)setupData:(NSManagedObject*) entityObject
{
    selectedObject = entityObject;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return NO;
}
- (void)cancelRequest
{
	[[self navigationController] popViewControllerAnimated:YES];
}
- (void)insertNewObject
{
	EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
	
	NSString* description = descriptionField.text;
		if ([selectedObject isKindOfClass:[HazardTemplate class]]) //It is a risk object so update data
		{
			HazardTemplate* item = (HazardTemplate*) selectedObject;
			[item setValue:description forKey:@"Description"];
		}
		else
		{
			
			[delegate AddHazardTemplate:description  DisplayOrder:99 ];
		}
	
	
	[delegate saveContext];
	[[self navigationController] popViewControllerAnimated:YES];
	
}


/*
 - (void)viewWillAppear:(BOOL)animated {
 [super viewWillAppear:animated];
 }
 */
/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
 
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iphone_img_background.png"]];
    [tempImageView setFrame:self.tableView.frame];
    
    tableView.backgroundView = tempImageView;

    return 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		UITextField *textField  = [[UITextField alloc] initWithFrame:CGRectMake(10,0, 280  , 44)];
		textField.clearButtonMode = UITextFieldViewModeAlways;
		//	[textField addTarget:self action:@selector(textFieldDoneEditing:) forControlEvents:UIControlEventEditingDidEndOnExit];
		textField.textAlignment = UITextAlignmentLeft;
		textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
		textField.keyboardType = UIKeyboardTypeDefault;
		textField.returnKeyType = UIReturnKeyDefault;
		
		if (sectionRow == 0)
		{
			textField.tag = 2;
			textField.placeholder = @"Description";
			descriptionField = textField;
				if ([selectedObject isKindOfClass:[HazardTemplate class]]) //It is a risk object so update data
				{
					HazardTemplate* item = (HazardTemplate*) selectedObject;
					descriptionField.text = [[item valueForKey:@"Description"] description];
				}
        }
		[cell.contentView addSubview:textField];

		
    }
    
    // Configure the cell...
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate




#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}





@end

