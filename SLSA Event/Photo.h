//
//  Photo.h
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Hazard;

@interface Photo : NSManagedObject

@property (nonatomic, retain) NSData * newAttribute;
@property (nonatomic, retain) NSString * Name;
@property (nonatomic, retain) Hazard *Hazard;

@end
