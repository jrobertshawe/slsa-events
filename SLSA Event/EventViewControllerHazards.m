//
//  EventViewControllerHazards.m
//  SLSA Event
//
//  Created by Julian robertshawe on 1/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerHazards.h"
#import "EventViewControllerHazardDetail.h"
@interface EventViewControllerHazards ()

@end

@implementation EventViewControllerHazards

@synthesize _tableView;
@synthesize footerView;
@synthesize footerLabel;
-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite
{
    assessment = entityObject;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *navBar;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        navBar = @"ipad_img_blue-header.png";
    }
    else {
        navBar = @"iphone_img_blueheader.png";
    }
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:navBar];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
  
 
     
    
    Items = [[NSMutableArray alloc] init];
  
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"refreshView"
     object:nil];
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
self._tableView.autoresizesSubviews = YES;
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav3.png",delegate.imagePrefix];
    
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    
    [hv addSubview:image];
    //  hv.backgroundColor = [UIColor whiteColor];
    _tableView.tableHeaderView = hv;
    
  //  CGSize cgsize = [footerLabel.text sizeWithFont:footerLabel.font constrainedToSize:CGSizeMake(footerLabel.frame.size.width,MAXFLOAT) lineBreakMode:footerLabel.lineBreakMode];
  //  self.footerView.frame = CGRectMake(0.0, 0.0, self._tableView.bounds.size.width, cgsize.height+500);
    
    // set up the table's footer view based on our UIView 'myFooterView' outlet
    //   CGRect newFrame = CGRectMake(0.0, 0.0, self._tableView.bounds.size.width, self.footerView.frame.size.height);
  //  self.footerView.backgroundColor = [UIColor clearColor];
    //   self.footerView.frame = newFrame;
  //  self._tableView.tableFooterView = self.footerView; // note this will override U
    hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,500)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 000, self._tableView.bounds.size.width-40, 160)];
    label.text = @"Observe the area you are assessing and identify anything that may pose a threat to the health and safety of people using the area.";
    self.footerView.backgroundColor = [UIColor clearColor];
    hv.backgroundColor  = [UIColor clearColor];
    label.backgroundColor  = [UIColor clearColor];
    label.textColor =  [UIColor lightGrayColor];
    label.numberOfLines = 40;
    [hv addSubview:label];
    CGRect newFrame = CGRectMake(0.0, 0.0, self._tableView.bounds.size.width,700);
    hv.frame = newFrame;
    self._tableView.tableFooterView = hv;
}
- (IBAction)CreateReport:(id)sender
{
    GenerateReport*  generateReport = [[GenerateReport alloc] initWithNibName: @"GenerateReport" bundle:nil];
    generateReport.assessment = assessment;
    [[self navigationController] pushViewController:generateReport animated:YES];
	
	
}
-(void)handleNotification:(NSNotification *)pNotification
{
    
    [self._tableView reloadData];
}
- (void)EditMode {
    if (_tableView.isEditing == YES)
        
	{
		UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage* buttonImage = [UIImage imageNamed:@"but_edit_off.png"];
        [backButton setImage:buttonImage forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
        backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
        self.navigationItem.RightBarButtonItem = rightButtonItem;
		[self._tableView setEditing:NO animated:YES];
	}
	
	else
	{
		UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage* buttonImage = [UIImage imageNamed:@"but_done_off.png"];
        [backButton setImage:buttonImage forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
        backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
        self.navigationItem.RightBarButtonItem = rightButtonItem;
        [self._tableView setEditing:YES animated:YES];
		
	}
}

- (void) didTapBackButton:(id)sender {
    
      assessment.hazardscheck = [NSNumber numberWithBool: YES];
    
    
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
- (void) viewWillAppear:(BOOL)animated
{
    [_tableView reloadData];
    self.navigationItem.rightBarButtonItem = nil;
    bool weather = NO;
    bool surf = false;
    bool activity = NO;
    bool hazard = NO;
    
    if (assessment.weatherChecked != nil)
        if ([assessment.weatherChecked boolValue] == YES)
                weather = YES;
    if (assessment.activityChecked != nil)
        if ([assessment.activityChecked boolValue] == YES)
                activity = YES;
    if (assessment.hazardscheck != nil)
        if ([assessment.hazardscheck boolValue] == YES)
                hazard = YES;
    if ( weather== YES && activity == YES && hazard == YES)
    {
        UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
       UIImage*  buttonImage = [UIImage imageNamed:@"but_report_off.png"];
        [backButton setImage:buttonImage forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(CreateReport:) forControlEvents:UIControlEventTouchUpInside];
        backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.RightBarButtonItem = rightButtonItem;
    }

}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}
#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    [Items removeAllObjects];
	[Items addObjectsFromArray:[assessment.Hazzards allObjects]];
    [ Items insertObject:@"Add Hazard"  atIndex:0];    //add temp line to
     
 

    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [Items count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    }
    
    // Configure the cell.
    [self configureCell:cell atIndexPath:indexPath];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSString class]])
    {
        NSString* stringObject = (NSString*)managedObject;
        cell.textLabel.text = stringObject;
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.accessoryType =    UITableViewCellAccessoryNone;
    }
    else
    {
        cell.textLabel.textColor = [UIColor blackColor];
        Hazard *hazard = (Hazard*)managedObject;
        if (hazard.Description)
            cell.textLabel.text =hazard.Description;
            cell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSString class]])
    {
        
        
        SelectItemItem *popoverContent = [[SelectItemItem alloc] initWithNibName:@"SelectItemItem" bundle:nil];
        popoverContent.parentController  = self;
        [popoverContent setupData:assessment entityKeyString:@"Hazzards" entityselectListType:SELECTLISTTEMPLATE entitySelectList:@"HazardTemplate" parentQueue:@"refreshView"];
        popoverContent.label = @"Add hazard";
        //resize the popover view shown
        //in the current view to the view's size
        popoverContent.contentSizeForViewInPopover =
        CGSizeMake(500, 500);
        
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 520.0, 100);
        
        //create a popover controller
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.08627450980392 green:0.15686274509804 blue:0.20392156862745 alpha:1];
        
        navigationController.view.frame = CGRectMake(0.0, -10.0, 300, 504);
        //release the popover content
        //[popoverContent release];
        [self presentModalViewController:navigationController animated:YES];
        //release the popover content
               return;
    }
    
	if (_tableView.isEditing == YES)
	{
        [self performSegueWithIdentifier:@"HazardDetail" sender: nil];
    }
	else
	{
        [self performSegueWithIdentifier:@"HazardDetail" sender: nil];
    }
    
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        return UITableViewCellEditingStyleDelete;
    }
    else
        return UITableViewCellEditingStyleNone;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        return YES;
    }
    else
        return NO;
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSManagedObject *managedObject = [Items objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        [delegate.managedObjectContext deleteObject:managedObject];
        
           [delegate saveContext];
        [tableView reloadData];
	}
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    NSManagedObject *item = [Items objectAtIndex:[self._tableView.indexPathForSelectedRow row]];
    EventViewControllerHazardDetail *ev = (EventViewControllerHazardDetail *)[segue destinationViewController];
    
    [ev setupData:item assessment:assessment];
    
}

@end
