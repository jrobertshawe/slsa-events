//
//  ChooseImage.m
//  SLSA Risk Assessment
//
//  Created by Julian on 5/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "ChooseImage.h"
#import "EventAppDelegate.h"

@implementation ChooseImage
@synthesize imageview;
@synthesize label;
@synthesize toolbar;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    imagePicker = nil;
    photoobjectString = nil;
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *navBar;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        navBar = @"ipad_img_blue-header.png";
    }
    else {
        navBar = @"iphone_img_blueheader.png";
    }
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:navBar];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }

    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DoneButtonPressed)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    self.navigationItem.title = label;
    
    
      CGRect viewrect = self.view.frame;
    
   
    
    CGRect toolbarrect = toolbar.frame;
    
    
    
    imageview.frame = CGRectMake(0,0,320,viewrect.size.height-130);
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
    toolbar.frame = CGRectMake(toolbarrect.origin.x,460,540,toolbarrect.size.height);
    
    }
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
    
    
}
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString queue:(NSString*) queue

{
     
    selectedObject  = entityObject;
    entityKey = [[NSString alloc] initWithString:entityKeyString];
    notifyQueue = [[NSString alloc] initWithString:queue];
     NSLog(@"%@ %@ ",entityObject , entityKey);
}
-(void) viewDidAppear:(BOOL)animated
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
    }     
    NSManagedObject*       entity; 
   
     NSObject* id =  [entity valueForKey:entityKey];
      if ([id isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
      {
        NSString *_imageName = [entity valueForKey:entityKey];
        if (_imageName)
        {
            NSString *appFile = [documentsDirectory stringByAppendingPathComponent:_imageName];
            
            UIImage *image  = [UIImage imageWithContentsOfFile:appFile];
            [imageview setImage:image];
        }
      }
  //  imageview.frame = CGRectMake(0,0,320,300);
}
-(IBAction)DoneButtonPressed
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];
}
-(IBAction)SelectPhotoPressed: (id)sender
{
     imagePicker = [[UIImagePickerController alloc] init];
    // Set source to the camera
    
    // Delegate is self
    imagePicker.delegate = self;
     imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    // Allow editing of image ?
    
 //  
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
    popoverController = [[UIPopoverController alloc] initWithContentViewController: imagePicker];
    
    [popoverController presentPopoverFromBarButtonItem:sender
                              permittedArrowDirections:UIPopoverArrowDirectionDown  animated:YES];
    
    // Show image picker
    [popoverController setPopoverContentSize:CGSizeMake(300, 400)];
    imagePicker.contentSizeForViewInPopover =
    CGSizeMake(300, 400);
    }
    else
    {
        [self presentModalViewController:imagePicker animated:YES];
    }
    
    
}
-(IBAction)TakePhotoPressed: (id)sender
{
    imagePicker = [[UIImagePickerController alloc] init];
    if (popoverController != nil)
    {
        if([popoverController isPopoverVisible])
        {
            //close the popover view         
            [popoverController dismissPopoverAnimated:YES];
            
        }
        
    }    
    // Set source to the camera
    
    // Delegate is self
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    // Allow editing of image ?
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        popoverController = [[UIPopoverController alloc] initWithContentViewController: imagePicker];
        
        [popoverController presentPopoverFromBarButtonItem:sender
                                  permittedArrowDirections:UIPopoverArrowDirectionDown  animated:YES];
        
        // Show image picker
        [popoverController setPopoverContentSize:CGSizeMake(300, 400)];
        imagePicker.contentSizeForViewInPopover =
        CGSizeMake(300, 400);
    }
    else
    {
   // [popoverController presentPopoverFromBarButtonItem:sender
     //                         permittedArrowDirections:UIPopoverArrowDirectionDown  animated:YES];
    
    // Show image picker
    
       [self presentModalViewController:imagePicker animated:YES];
    }
  

    
    
    
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 2 )
        return;
    // Create image picker controller
  	    imagePicker = [[UIImagePickerController alloc] init];
 
    // Set source to the camera
    
    // Delegate is self
    imagePicker.delegate = self;
    if (buttonIndex == 0 ) 
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    // Allow editing of image ?

    popoverController = [[UIPopoverController alloc] initWithContentViewController: imagePicker];
  
    [popoverController presentPopoverFromRect:CGRectMake(600, 600, 600, 600)
                                       inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    // Show image picker
     [popoverController setPopoverContentSize:CGSizeMake(600, 600)];
    imagePicker.contentSizeForViewInPopover = 
    CGSizeMake(600, 600);

    
}
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    CGSize newsize;
    newsize.width = 480;
    newsize.height = 480;
    [popoverController dismissPopoverAnimated:true];
      
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    int w= image.size.width;
    int h= image.size.height;
    NSLog(@"w:%d h:%d",w,h);

    
    if (image.size.height >= newsize.height && image.size.width >= newsize.width)
        imageview.image = [delegate imageWithImage:image scaledToSizeWithSameAspectRatio:newsize];
   else
       imageview.image = image;
   

   
    // Access the uncropped image from info dictionary
     NSString *filename = [NSString stringWithFormat:@"%d.jpg",[[NSDate date] timeIntervalSince1970]];
    
    
    if ([entityKey isEqualToString:@"Photos"] == YES) //It is a sting object row must be add mode
    {
        
        
        
        NSManagedObject* photoItem = [delegate AddPhoto:filename hazard:selectedObject];
        entityKey = @"name";
           photoobjectString = [[NSString alloc] initWithString:[[[photoItem objectID] URIRepresentation] absoluteString]];
        
     }
   
        NSString *parrentRecord  = nil;;
        NSManagedObject*       entity;
        
     //   
         
    [self saveImage:image withName:filename];
 //   [self saveImageThumb:image withName:filename];
    [self saveImageSmall:image withName:filename];
     [picker dismissModalViewControllerAnimated:YES];
    
    
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[imagePicker dismissModalViewControllerAnimated:YES];
    
}
- (void)saveImage:(UIImage *)image withName:(NSString *)name
{
	
	//save image
    //	NSData *data = UIImageJPEGRepresentation(image, 1);
    //	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,  YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:name];
    //	[fileManager createFileAtPath:fullPath contents:data attributes:nil];
    [UIImageJPEGRepresentation(image, 1.0) writeToFile:fullPath atomically:YES];
	
}
- (void)saveImageThumb:(UIImage *)image withName:(NSString *)name
{
    
    
    int w= image.size.width;
    int h= image.size.height;
    NSLog(@"w:%d h:%d",w,h);
    
    
    CGSize newsize;
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    newsize.width = image.size.width/(image.size.height/140);
    newsize.height = 140;
    UIImage *resizedimage;
    
    if (image.size.height > newsize.height && image.size.width > newsize.width)
        resizedimage = [delegate imageWithImage:image scaledToSizeWithSameAspectRatio:newsize];
    else
        resizedimage = image;
    resizedimage = [UIImage imageWithImage:image scaledToSize:CGSizeMake(newsize.width,  newsize.height)];
    
	//save image
	NSString *thumbName = [NSString stringWithFormat:@"thumb_%@",name];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,  YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:thumbName];
    [UIImageJPEGRepresentation(resizedimage, 1.0) writeToFile:fullPath atomically:YES];
    
	
}
- (void)saveImageSmall:(UIImage *)image withName:(NSString *)name
{
    int w= image.size.width;
    int h= image.size.height;
    NSLog(@"w:%d h:%d",w,h);
    
    
    
    CGSize newsize;
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    newsize.width = image.size.width/(image.size.height/140);
    newsize.height = 140;
    UIImage *resizedimage;
    
    if (image.size.height > newsize.height && image.size.width > newsize.width)
        resizedimage = [delegate imageWithImage:image scaledToSizeWithSameAspectRatio:newsize];
    else
        resizedimage = image;
    resizedimage =   [delegate scaledToSize:image newSize:CGSizeMake(newsize.width,  newsize.height)];
    
    //save image
	NSString *smallName = [NSString stringWithFormat:@"small_%@",name];
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,  YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:smallName];
    [UIImageJPEGRepresentation(resizedimage, 1.0) writeToFile:fullPath atomically:YES];
    
}

@end
