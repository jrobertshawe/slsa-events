//
//  AssessmentDetails.m
//  SLSARiskAssessment
//
//  Created by Julian on 2/12/10.
//  Copyright 2010 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "AssessmentDetails.h"

#import "Events.h"
#import "Assessment.h"
#import "WindSpeedTemplates.h"
#import "WaveTypeTemplates.h"
#import "WindDirectionTemplates.h"
#import "EnterValue.h"
#import "SwellSizeTemplates.h"
#import "CloudCover.h"
#import "TideTemplates.h"
#import "SelectTime.h"
//#import "AssessmentTemplates.h"
#define DETAILS 0
#define CURRENTWEATHER 1
#define CURRENTSURF 3
#define PREDICTIONWEATHER 2 
#define PREDICTIONSURF 4 
#define EVENTS 5

@implementation AssessmentDetails

@synthesize conditionField;
@synthesize descriptionField;
@synthesize temperatureField;
@synthesize governmentArea;
@synthesize _tableView;
#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
	UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
	Assessment* assessmentObject;
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelRequest)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    	EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(saveObject)];
    self.navigationItem.rightBarButtonItem = addButton;
	self.navigationItem.title = @"Add Assessment";
	if (objectString != nil)
	{
		if ([selectedObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
		{
			self.navigationItem.title = @"Update Assessment";
			assessmentObject = (Assessment*) selectedObject;
			newRecord = NO;
		}
		else
		{
			assessmentObject = [delegate CreateAssessment];
			newRecord = YES;
            [assessmentObject setValue:governmentArea.ContactName forKey:@"Assessor"];
		}
	}
	else
	{
		assessmentObject = [delegate CreateAssessment];
         [assessmentObject setValue:governmentArea.ContactName forKey:@"Assessor"];
		newRecord = YES;
	}
    [delegate saveContext];
    assessmenturi =  [[NSString alloc] initWithString:[[[assessmentObject objectID] URIRepresentation] absoluteString]];
[self._tableView setEditing:YES animated:YES];
    [    self.navigationItem.rightBarButtonItem setEnabled:NO];
}
-(void)setupData:(NSString*) entityObject
{
    
    
    objectString = [[NSString alloc] initWithString:entityObject];
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
  
    return NO;
}
-(void) checkValid
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
      [    self.navigationItem.rightBarButtonItem setEnabled:NO];
 
   
    if (item.Description == nil)
        return;
    
    if (item.Assessor == nil)
        return;
    if (item.CurrentCloud == nil)
        return;
 //   if (item.CurrentSwellSize == nil)
 //       return;  
//    if (item.CurrentSwellTime == nil)
 //       return;  
 //   if (item.CurrentTide == nil)
   //     return;  
  //  if (item.CurrentWaveType == nil)
  //      return;   
    if (item.CurrentWeatherTemp == nil)
        return; 
    if (item.CurrentWindDirection == nil)
        return; 
    if (item.CurrentWindSpeed == nil)
        return; 
    
    if (item.PredictionCloud == nil)
        return;
  //  if (item.PredictionSwellSize == nil)
 //       return;  
 //   if (item.PredictionSwellTime == nil)
 //       return;  
 //   if (item.PredictionLowTide == nil)
 //       return;  
//    if (item.PredictionHighTide == nil)
//        return;  
 //   if (item.PredictionWaveType == nil)
 //       return;   
    if (item.PredictionWeatherTemp == nil)
        return; 
    if (item.PredictionWindDirection == nil)
        return; 
    if (item.PredictionWindSpeed == nil)
        return; 
   
    
    if([item.Events count] == 0)
        return;

      [    self.navigationItem.rightBarButtonItem setEnabled:YES];
    
}
- (void)cancelRequest
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
	if (newRecord == YES)
		[delegate DeleteRecord:item];
	[[self navigationController] popViewControllerAnimated:YES];
}
- (void)saveObject
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
  
	
	
		if ([item isKindOfClass:[Assessment class]]) //It is a risk object so update data
		{
			
			
				if (localsite != nil) //It is a risk object so update data
				{
					[localsite addAssessmentsObject:item];
				}
				
			}
	[delegate saveContext];
	
	[[self navigationController] popViewControllerAnimated:YES];
	
	
	
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath   ///what edit mode are we entering
{
	
	if (self._tableView.editing == YES)
	{
		
		NSUInteger row = [indexPath row];
		NSUInteger section = [indexPath section];
		if (section == EVENTS)   
		{
			NSManagedObject *item = [eventItems objectAtIndex:row];
			if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
			{
				return UITableViewCellEditingStyleInsert;
				
			}
			else //else show delete option
			{			
				return UITableViewCellEditingStyleDelete;
			}
			
		}
    }
    return UITableViewCellEditingStyleNone;
}


 - (void)viewWillAppear:(BOOL)animated {
 [super viewWillAppear:animated];
	 [self._tableView reloadData];
     [self checkValid];
 }

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 6;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int sectionRowCount;
	
    switch (section) {
		case DETAILS:
			sectionRowCount = 4;
			break;
		case EVENTS:
			sectionRowCount = [eventItems count];	
			
			break;	
        case CURRENTWEATHER:
    		sectionRowCount = 4;
	        
            break;
            
        case PREDICTIONWEATHER:
    		sectionRowCount = 4;
	        
            break;
        case CURRENTSURF:
    		sectionRowCount = 4;
	        
            break;
        case PREDICTIONSURF:
    		sectionRowCount = 5;
	        
            break;

		default:
			sectionRowCount = 0;
			break;
	}
	
    return sectionRowCount;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
     
	NSString *headerTitle = @"";
	
		switch (section) {
			case DETAILS:
				headerTitle =  @"Details";
				
				break;
            case CURRENTWEATHER:
				headerTitle =  @"Current Weather";
				
				break;

			case PREDICTIONWEATHER:
				headerTitle =  @"Prediction Weather";
				
				break;
         	case CURRENTSURF:
				headerTitle =  @"Current Surf";
				
				break;
            case PREDICTIONSURF:
				headerTitle =  @"Prediction Surf";
				
				break;
            
            case EVENTS:
                if (eventItems == nil)
					eventItems = [[NSMutableArray alloc] init];
				else
					[eventItems removeAllObjects];
				
				[eventItems addObjectsFromArray:[item.Events allObjects]];
                [ eventItems addObject:@"Add Activity"];   //add temp line to allow adding items
				headerTitle =  @"Activity";
				break;
			default:
				headerTitle = @"";
				break;
		}
    

return headerTitle;
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {;
  	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
    static NSString *CellIdentifier = @"Cell";
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		
		
		//	UILabel *labelField  = [[UILabel alloc] initWithFrame:CGRectMake(13,10, 280  , 25)];
		//	labelField.tag = 11;
		//	labelField.textColor = [UIColor lightGrayColor];
		//	[cell.contentView addSubview:labelField];
		//	[labelField release];
		//	cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
		//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
       //  if (section == EVENTS)
	//	{
	//		NSManagedObject *item = [eventItems objectAtIndex:row];
	//		if ([item isKindOfClass:[NSString class]])
	//		{
	//			NSString* labelText = (NSString*) item;
	//			cell.textLabel.text  = labelText;
	//			cell.editingAccessoryType = UITableViewCellAccessoryNone;
	//		}
	//		else
	//		{
	//			Events   *control = (Events* )item;
	//			cell.textLabel.text  = [[control valueForKey:@"Description"] description];
	//			cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
	//		}
	//		
	//	}

        
	 			
	}
   	/*	if (assessmentObject != nil)
		{
			if ([assessmentObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
			{
				UILabel *cell.textLabel  = (UILabel *)[cell viewWithTag:11];
				UILabel *label12  = (UILabel *)[cell viewWithTag:12];
				Assessment* item = (Assessment*) assessmentObject;
				conditionField.text = [[item valueForKey:@"Conditions"] description];
				temperatureField.text = [[item valueForKey:@"temperature"] description];
				NSDate * assementDate = [item valueForKey:@"AssessmentDate"];
				label12.text =  [formatter stringFromDate:assementDate];
				if (item.Description != nil)
				{
				cell.textLabel.text = [[item valueForKey:@"Description"] description];
					cell.textLabel.textColor = [UIColor blackColor];
				}
					
				else
				{
					cell.textLabel.text = @"Assessment Type";
					cell.textLabel.textColor = [UIColor blackColor];
				}
					
					

			}
		}
     */
    cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    if (sectionRow == 0)
    {
        
     	if (item.Description != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"Description"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Assessment Type";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    if (sectionRow == 1)
    {
        
     	if (item.Assessor != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"Assessor"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Your Name";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    if (sectionRow == 3)
    {
        
     	if (item.Notes != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"Notes"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Notes";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 10)
    {
        
     	if (item.CurrentWeatherTemp != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"CurrentWeatherTemp"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Air Temperature";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    
    else if (sectionRow == 11)
    {
        
     	if (item.CurrentWindDirection != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"CurrentWindDirection"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Wind Direction";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 12)
    {
        
     	if (item.CurrentWindSpeed != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"CurrentWindSpeed"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Wind Speed";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 13)
    {
        
     	if (item.CurrentCloud != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"CurrentCloud"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Weather";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 20)
    {
        
     	if (item.PredictionWeatherTemp != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"PredictionWeatherTemp"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Air Temperature";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 21)
    {
        
     	if (item.PredictionWindDirection != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"PredictionWindDirection"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Wind Direction";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 22)
    {
        
     	if (item.PredictionWindSpeed != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"PredictionWindSpeed"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Wind Speed";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 23)
    {
        
     	if (item.PredictionCloud != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"PredictionCloud"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Weather";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 30)
    {
        
     	if (item.CurrentSwellSize != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"CurrentSwellSize"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Swell Size";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 31)
    {
        
     	if (item.CurrentWaveType != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"CurrentWaveType"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Wave Type";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 32)
    {
        
     	if (item.CurrentTide != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"CurrentTide"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Tide";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 33)
    {
        
     	if (item.CurrentSwellTime != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"CurrentSwellTime"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Swell Period";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 40)
    {
        
     	if (item.PredictionSwellSize != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"PredictionSwellSize"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Swell Size";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 41)
    {
        
     	if (item.PredictionWaveType != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"PredictionWaveType"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Wave Type";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 42)
    {
        
     	if (item.PredictionLowTide != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"PredictionLowTide"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Low Tide Time";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 43)
    {
        
     	if (item.PredictionHighTide != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"PredictionHighTide"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"High Tide Time";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 44)
    {
        
     	if (item.PredictionSwellTime != nil)
        {
            cell.textLabel.text = [[item valueForKey:@"PredictionSwellTime"] description];
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Swell Period";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    }
    else if (sectionRow == 2)
    {
        cell.editingAccessoryType = UITableViewCellAccessoryNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
		cell.textLabel.textColor = [UIColor blueColor];
    	NSDate * assementDate = [item valueForKey:@"AssessmentDate"];
        cell.textLabel.text =  [formatter stringFromDate:assementDate];
        
        
    }
    
    
     if (section == EVENTS)
		{
			UITextField *textField = (UITextField *)[cell viewWithTag:1];
			if (textField != nil)
				textField.hidden = YES;
			NSManagedObject *item = [eventItems objectAtIndex:row];
			if ([item isKindOfClass:[NSString class]])
			{
				NSString* labelText = (NSString*) item;
				cell.textLabel.text  = labelText;
                cell.textLabel.textColor = [UIColor lightGrayColor];

				cell.editingAccessoryType = UITableViewCellAccessoryNone;
			}
			else
			{
                cell.textLabel.textColor = [UIColor blackColor];

				Event *event = (Event* )item;
				cell.textLabel.text  = [[event valueForKey:@"Description"] description];
						cell.editingAccessoryType = UITableViewCellAccessoryNone;
			}
		}
  
    
    // Configure the cell...
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	NSUInteger section = [indexPath section];
	if (section == EVENTS )  //if add items then indent
		return YES;
	
	return NO;
}      
/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	  
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	if (section == EVENTS)
	{
		NSManagedObject *eventitem = [eventItems objectAtIndex:row];
		if ([eventitem isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
		{
            Events*  events = [[Events alloc] initWithNibName: @"Events" bundle:nil];
            [events setupData:assessmenturi];
            
            [[self navigationController] pushViewController:events animated:YES];
 
			
		}
		else //else show delete option
		{		
            [item removeEventsObject:eventitem];
            [self._tableView reloadData];
			;
		}
	}
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
    WindDirectionTemplates*  windDirectionTemplates;
    WindSpeedTemplates*  windSpeedTemplates;
    SwellSizeTemplates* swellSizeTemplates; 
    WaveTypeTemplates* waveTypeTemplates;
    TideTemplates* tideTemplates;
    SelectTime * selectTime;
    CloudCover * cloudCover;
    EnterValue*  enterValue;
    if (sectionRow  == 0)
	{
	//	AssessmentTemplates*  assessmentTemplates = [[AssessmentTemplates alloc] initWithNibName: @"AssessmentTemplates" bundle:nil];
    //    [assessmentTemplates setupData:assessmenturi];
	//	[[self navigationController] pushViewController:assessmentTemplates animated:YES];
	
		
	}
    else if (sectionRow  == 1)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
        [enterValue setupData:assessmenturi entityKeyString:@"Assessor"];
	    enterValue.promptText  = @"First name Last name";
        enterValue.titleText = @"Your Name";
        enterValue.editkeyboardtype = UIKeyboardTypeAlphabet;

		[[self navigationController] pushViewController:enterValue animated:YES];
		
	}
    else if (sectionRow  == 3)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
        [enterValue setupData:assessmenturi entityKeyString:@"Notes"];
        enterValue.promptText  = @"";
        enterValue.titleText = @"Assessment Notes";
        enterValue.editkeyboardtype = UIKeyboardTypeAlphabet;
        enterValue.isNotes = YES;
		[[self navigationController] pushViewController:enterValue animated:YES];
		
	}
    else if (sectionRow  == 11)
	{
		windDirectionTemplates = [[WindDirectionTemplates alloc] initWithNibName: @"WindDirectionTemplates" bundle:nil];
         [windDirectionTemplates setupData:assessmenturi entityKeyString:@"CurrentWindDirection"];
		[[self navigationController] pushViewController:windDirectionTemplates animated:YES];
		
	}
    else if (sectionRow  == 12)
	{
		windSpeedTemplates = [[WindSpeedTemplates alloc] initWithNibName: @"WindSpeedTemplates" bundle:nil];
         [windSpeedTemplates setupData:assessmenturi entityKeyString:@"CurrentWindSpeed"];
		[[self navigationController] pushViewController:windSpeedTemplates animated:YES];
			
	}
    else if (sectionRow  == 13)
	{
		cloudCover = [[CloudCover alloc] initWithNibName: @"CloudCover" bundle:nil];
         [cloudCover setupData:assessmenturi entityKeyString:@"CurrentCloud"];
		[[self navigationController] pushViewController:cloudCover animated:YES];
		
	}
    else if (sectionRow  == 10)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
		[enterValue setupData:assessmenturi entityKeyString:@"CurrentWeatherTemp"];
        enterValue.promptText  = @"Enter temperature as number";
        enterValue.titleText  = @"Air Temperature";
        enterValue.editkeyboardtype = UIKeyboardTypeDecimalPad;
		[[self navigationController] pushViewController:enterValue animated:YES];
			
	}
    else if (sectionRow  == 20)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
		[enterValue setupData:assessmenturi entityKeyString:@"PredictionWeatherTemp"];
         enterValue.editkeyboardtype = UIKeyboardTypeDecimalPad;
        enterValue.promptText  = @"Enter temperature as number";
          enterValue.titleText  = @"Air Temperature";
		[[self navigationController] pushViewController:enterValue animated:YES];
		
	}
    else if (sectionRow  == 21)
	{
		windDirectionTemplates = [[WindDirectionTemplates alloc] initWithNibName: @"WindDirectionTemplates" bundle:nil];
        [windDirectionTemplates setupData:assessmenturi entityKeyString:@"PredictionWindDirection"];
        
	    [[self navigationController] pushViewController:windDirectionTemplates animated:YES];
		}
    else if (sectionRow  == 22)
	{
		windSpeedTemplates = [[WindSpeedTemplates alloc] initWithNibName: @"WindSpeedTemplates" bundle:nil];
        [windSpeedTemplates setupData:assessmenturi entityKeyString:@"PredictionWindSpeed"];
        
		[[self navigationController] pushViewController:windSpeedTemplates animated:YES];
		
	}
    else if (sectionRow  == 23)
	{
		cloudCover = [[CloudCover alloc] initWithNibName: @"CloudCover" bundle:nil];
         [cloudCover setupData:assessmenturi entityKeyString:@"PredictionCloud"];
		[[self navigationController] pushViewController:cloudCover animated:YES];
		
	}
    else if (sectionRow  == 30)
	{
		swellSizeTemplates = [[SwellSizeTemplates alloc] initWithNibName: @"SwellSizeTemplates" bundle:nil];
         [swellSizeTemplates setupData:assessmenturi entityKeyString:@"CurrentSwellSize"];
		[[self navigationController] pushViewController:swellSizeTemplates animated:YES];
		
	}
    else if (sectionRow  == 31)
	{
		waveTypeTemplates = [[WaveTypeTemplates alloc] initWithNibName: @"WaveTypeTemplates" bundle:nil];
         [waveTypeTemplates setupData:assessmenturi entityKeyString:@"CurrentWaveType"];
		[[self navigationController] pushViewController:waveTypeTemplates animated:YES];
		
	}
    else if (sectionRow  == 32)
	{
        tideTemplates = [[TideTemplates alloc] initWithNibName: @"TideTemplates" bundle:nil];
         [tideTemplates setupData:assessmenturi entityKeyString:@"CurrentTide"];
		[[self navigationController] pushViewController:tideTemplates animated:YES];
		
	}
    else if (sectionRow  == 33)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
		[enterValue setupData:assessmenturi entityKeyString:@"CurrentSwellTime"];
        enterValue.promptText  = @"in seconds";
        enterValue.titleText = @"Swell Period";
               enterValue.editkeyboardtype = UIKeyboardTypeDecimalPad;
		[[self navigationController] pushViewController:enterValue animated:YES];
			
	}
    else if (sectionRow  == 40)
	{
		swellSizeTemplates = [[SwellSizeTemplates alloc] initWithNibName: @"SwellSizeTemplates" bundle:nil];
         [swellSizeTemplates setupData:assessmenturi entityKeyString:@"PredictionSwellSize"];
		[[self navigationController] pushViewController:swellSizeTemplates animated:YES];
		
	}
    else if (sectionRow  == 41)
	{
		waveTypeTemplates = [[WaveTypeTemplates alloc] initWithNibName: @"WaveTypeTemplates" bundle:nil];
         [waveTypeTemplates setupData:assessmenturi entityKeyString:@"PredictionWaveType"];
		[[self navigationController] pushViewController:waveTypeTemplates animated:YES];

		
	}
    else if (sectionRow  == 42)
	{
		selectTime = [[SelectTime alloc] initWithNibName: @"SelectTime" bundle:nil];
        [selectTime setupData:assessmenturi entityKeyString:@"PredictionLowTide" entityKeyStringValue:@"PredictionLowTideValue"];
        
	    
        selectTime.titleText = @"Low Tide Time";	
        [[self navigationController] pushViewController:selectTime animated:YES];
		
	}
    else if (sectionRow  == 43)
	{
		selectTime = [[SelectTime alloc] initWithNibName: @"SelectTime" bundle:nil];
        [selectTime setupData:assessmenturi entityKeyString:@"PredictionHighTide" entityKeyStringValue:@"PredictionHighTideValue"];
	    
        selectTime.titleText = @"High Tide Time";	
        [[self navigationController] pushViewController:selectTime animated:YES];
		
	}
    else if (sectionRow  == 44)
	{
		enterValue = [[EnterValue alloc] initWithNibName: @"EnterValue" bundle:nil];
		[enterValue setupData:assessmenturi entityKeyString:@"PredictionSwellTime"];
        enterValue.promptText  = @"in seconds";
        enterValue.titleText = @"Swell Period";
        enterValue.editkeyboardtype = UIKeyboardTypeDecimalPad;
        
		[[self navigationController] pushViewController:enterValue animated:YES];
		
	}
	if (section  == EVENTS)
	{
        NSManagedObject *item = [eventItems objectAtIndex:row];
		if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
		{
            Events*  events = [[Events alloc] initWithNibName: @"Events" bundle:nil];
            [events setupData:assessmenturi];
             [[self navigationController] pushViewController:events animated:YES];
  		}
	}
    
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}




@end

