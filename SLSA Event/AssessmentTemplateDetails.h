//
//  AssessmentTemplateDetails.h
//  SLSARiskAssessment
//
//  Created by Julian on 14/12/10.
//  Copyright 2010 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

@interface AssessmentTemplateDetails : UITableViewController {
	IBOutlet UITextField			*descriptionField;
	NSManagedObject* selectedObject;
}
@property (nonatomic, retain) IBOutlet UITextField *descriptionField;
- (void)cancelRequest;
- (void)insertNewObject;
-(void)setupData:(NSString*) entityObject;

@end
