//
//  RiskMatrix.h
//  SLSA Risk Assessment
//
//  Created by Julian on 10/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface RiskMatrix : UIViewController {
    NSManagedObject *entity;
    
    NSString*               entityKey;
    UIButton *lastButton;
    NSString*               notifyQueue;  
        NSString*               label; 
}
@property (nonatomic,retain)  NSString                              *label;
@property (nonatomic,retain)          NSString*               notifyQueue; 
@property (nonatomic,retain)  NSManagedObject						*entity;
@property (nonatomic,retain)  NSString                              *entityKey;
-(IBAction)MatrixButton:(id)sender;
-(IBAction)DoneButtonPressed;
-(IBAction)CancelButtonPressed;
-(void)setupData:(NSString*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString;
@end
