//
//  EditMultiLineText.m
//  SLSA Risk Assessment
//
//  Created by Julian on 21/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "EditMultiLineText.h"
#import "AussieAppDelegate.h"

@implementation EditMultiLineText
@synthesize textField;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)setupData:(NSString*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString notifyQueueString:(NSString*) notifyQueueString
{
    AussieAppDelegate *delegate = (AussieAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSManagedObject*       entity; 
    
    
    entityKey = [[NSString alloc] initWithString:entityKeyString];;
    notifyQueue = [[NSString alloc] initWithString:notifyQueueString];
    label = [[NSString alloc] initWithString:entityLabelString];
    self.navigationItem.title = entityLabelString;
    
    if (entityObject != nil)
    {
        entityString = [[NSString alloc] initWithString:entityObject];
        
        NSURL* tempURL = [[NSURL alloc] initWithString:entityObject];
        entity = [delegate objectWithURI:tempURL];
        if (entity != nil)
        {   
            NSString *stringValue = nil;
            stringValue = [entity valueForKey:entityKey];
            if (stringValue != nil)
                textField.text = stringValue;
            [textField setKeyboardType:UIKeyboardTypeAlphabet];
            
        }
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    AussieAppDelegate *delegate = (AussieAppDelegate* )[[UIApplication sharedApplication] delegate];
     NSManagedObject*       entity;
    [super viewDidLoad];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone                                                                                        target:self action:@selector(DoneButtonPressed)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(CancelButtonPressed)];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
     self.navigationItem.title = label;
    if (entityString != nil)
    {
         
        NSURL* tempURL = [[NSURL alloc] initWithString:entityString];
        entity = [delegate objectWithURI:tempURL];
        if (entity != nil)
        {   
          

            NSString *stringValue = nil;
            stringValue = [entity valueForKey:entityKey];
            if (stringValue != nil)
                textField.text = stringValue;
            [textField setKeyboardType:UIKeyboardTypeAlphabet];
            
        }
        
    }

    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];

}
-(IBAction)DoneButtonPressed
{
    AussieAppDelegate *delegate = (AussieAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSManagedObject*       entity = nil; 
    
    

    if (entityString != nil)
    {
        
        NSURL* tempURL = [[NSURL alloc] initWithString:entityString];
        entity = [delegate objectWithURI:tempURL];
     }
    [entity setValue:textField.text forKey:entityKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];
    return;
   
}
-(IBAction)CancelButtonPressed
{
    [[NSNotificationCenter defaultCenter] postNotificationName:notifyQueue object:nil userInfo:nil];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
