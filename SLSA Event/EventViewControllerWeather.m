//
//  EventViewControllerWeather.m
//  SLSA Event
//
//  Created by Julian robertshawe on 3/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerWeather.h"
#import "events.h"
#import "AssessmentTemplates.h"
#import "SelectTemperature.h"
@interface EventViewControllerWeather ()

@end

@implementation EventViewControllerWeather

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad {
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonImage = [UIImage imageNamed:@"but_done_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
  //  self.navigationItem.rightBarButtonItem = backButtonItem;
    
    
  	if (selectedObject != nil)
	{
        
		if ([selectedObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
		{
			newRecord = NO;
		}
		else
		{
            Assessment* assessmentObject = (Assessment *)[NSEntityDescription insertNewObjectForEntityForName:@"Assessment" inManagedObjectContext:[delegate managedObjectContext]];
            [assessmentObject setValue:[NSDate date] forKey:@"AssessmentDate"];
            
			newRecord = YES;
            [assessmentObject setValue:governmentArea.ContactName forKey:@"Assessor"];
            
		}
	}
	else
	{
        selectedObject = (Assessment *)[NSEntityDescription insertNewObjectForEntityForName:@"Assessment" inManagedObjectContext:[delegate managedObjectContext]];
        [selectedObject setValue:[NSDate date] forKey:@"AssessmentDate"];
        
        [selectedObject setValue:governmentArea.ContactName forKey:@"Assessor"];
		newRecord = YES;
	}
    [delegate saveContext];
 //   [self._tableView setEditing:YES animated:YES];
   // [    self.navigationItem.leftBarButtonItem setEnabled:NO];
    [delegate.fieldinError removeAllObjects];
    firstTime = YES;
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"refreshView"
     object:nil];

}
-(void)handleNotification:(NSNotification *)pNotification
{
  
    [self._tableView reloadData];
}
- (void) didTapBackButton:(id)sender {
    
    
    if ([self checkValid] == YES)
    {
        [self._tableView reloadData];
       return;
    };
    selectedObject.weatherChecked = [NSNumber numberWithBool: YES];
    
        
    
    if ([[localSiteObject.Assessments allObjects] containsObject:selectedObject] == NO)
        [localSiteObject addAssessmentsObject:selectedObject];
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    [delegate saveContext];
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite
{
    
    localSiteObject = localSite;
    if ([entityObject isKindOfClass:[Assessment class]])
    {
        selectedObject = entityObject;
    }
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    
    return NO;
}

-(bool) checkValid
{
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    bool notValid = NO;
   // [  self.navigationItem.leftBarButtonItem setEnabled:NO];
    
    if (selectedObject.CurrentCloud == nil)
    {
          [delegate.fieldinError addObject:@"CurrentCloud"];
        notValid = YES;
    }
       if (selectedObject.CurrentWeatherTemp == nil)
       {
           [delegate.fieldinError addObject:@"CurrentWeatherTemp"];
           notValid = YES;
       }
    if (selectedObject.CurrentWindDirection == nil)
    {
        [delegate.fieldinError addObject:@"CurrentWindDirection"];
        notValid = YES;
    }
    if (selectedObject.CurrentWindSpeed == nil)
    {
        [delegate.fieldinError addObject:@"CurrentWindSpeed"];
        notValid = YES;
    }
    
    if (selectedObject.PredictionCloud == nil)
    {
        [delegate.fieldinError addObject:@"PredictionCloud"];
        notValid = YES;
    }
     if (selectedObject.PredictionWeatherTemp == nil)
     {
         [delegate.fieldinError addObject:@"PredictionWeatherTemp"];
         notValid = YES;
     }
    if (selectedObject.PredictionWindDirection == nil)
    {
        [delegate.fieldinError addObject:@"PredictionWindDirection"];
        notValid = YES;
    }
    if (selectedObject.PredictionWindSpeed == nil)
    {
        [delegate.fieldinError addObject:@"PredictionWindSpeed"];
        notValid = YES;
    }
    [self._tableView reloadData];
    
    return notValid;
    
    //if (notValid == NO)
      //  [    self.navigationItem.leftBarButtonItem setEnabled:YES];
    
}
- (void)cancelRequest
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
	if (newRecord == YES)
		[[delegate managedObjectContext] deleteObject:selectedObject];
	[[self navigationController] popViewControllerAnimated:YES];
}
- (void)saveObject
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
	
	if (selectedObject != nil)
	{
		if ([selectedObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
		{
            
			if (objectString != nil)
			{
                
				if ([selectedObject isKindOfClass:[LocalSite class]]) //It is a risk object so update data
				{
                    [localSiteObject addAssessmentsObject:selectedObject];
					
                    
				}
				
			}
		}
	}
	[delegate saveContext];
	
	[[self navigationController] popViewControllerAnimated:YES];
	
	
	
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)] ;
	tableView.sectionHeaderHeight = headerView.frame.size.height;
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, headerView.frame.size.width - 20, 22)] ;
	label.text = [self tableView:tableView titleForHeaderInSection:section];
	label.font = [UIFont boldSystemFontOfSize:16.0];
	label.shadowOffset = CGSizeMake(0, 1);
	label.shadowColor = [UIColor blackColor];
	label.backgroundColor = [UIColor clearColor];
    
	label.textColor = [UIColor whiteColor];
    
	[headerView addSubview:label];
	return headerView;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath   ///what edit mode are we entering
{
	
	if (self._tableView.editing == YES)
	{
		
		NSUInteger row = [indexPath row];
		NSUInteger section = [indexPath section];
		if (section == EVENTS)
		{
			NSManagedObject *item = [eventItems objectAtIndex:row];
			if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
			{
				return UITableViewCellEditingStyleInsert;
				
			}
			else //else show delete option
			{
				return UITableViewCellEditingStyleDelete;
			}
			
		}
    }
    return UITableViewCellEditingStyleNone;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   [self._tableView reloadData];
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav3.png",delegate.imagePrefix];
    
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    
    [hv addSubview:image]; //  hv.backgroundColor = [UIColor whiteColor];
    tableView.tableHeaderView = hv;
    
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int sectionRowCount;
	
    switch (section) {
		case DETAILS:
			sectionRowCount = 4;
			break;
		case EVENTS:
			sectionRowCount = [eventItems count];
			
			break;
        case CURRENTWEATHER:
    		sectionRowCount = 4;
	        
            break;
            
        case PREDICTIONWEATHER:
    		sectionRowCount = 4;
	        
            break;
        case CURRENTSURF:
    		sectionRowCount = 4;
	        
            break;
        case PREDICTIONSURF:
    		sectionRowCount = 5;
	        
            break;
            
		default:
			sectionRowCount = 0;
			break;
	}
	
    return sectionRowCount;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
  	NSString *headerTitle = @"";
	
    switch (section) {
        case DETAILS:
            headerTitle =  @"Details";
            
            break;
        case CURRENTWEATHER:
            headerTitle =  @"Current Weather";
            
            break;
            
        case PREDICTIONWEATHER:
            headerTitle =  @"Prediction Weather";
            
            break;
        case CURRENTSURF:
            headerTitle =  @"Current Surf";
            
            break;
        case PREDICTIONSURF:
            headerTitle =  @"Prediction Surf";
            
            break;
            
        case EVENTS:
            if (eventItems == nil)
                eventItems = [[NSMutableArray alloc] init];
            else
                [eventItems removeAllObjects];
            
            [eventItems addObjectsFromArray:[selectedObject.Events allObjects]];
            [ eventItems insertObject:@"Add Activity"  atIndex:0];   //add temp line to allow adding items
            headerTitle =  @"Activity";
            break;
        default:
            headerTitle = @"";
            break;
    }
    
    
    return headerTitle;
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {;
    
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
    static NSString *CellIdentifier = @"Cell";
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
		
		
		//	UILabel *labelField  = [[UILabel alloc] initWithFrame:CGRectMake(13,10, 280  , 25)];
		//	labelField.tag = 11;
		//	labelField.textColor = [UIColor lightGrayColor];
		//	[cell.contentView addSubview:labelField];
		//	[labelField release];
		//	cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
		//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        //  if (section == EVENTS)
        //	{
        //		NSManagedObject *item = [eventItems objectAtIndex:row];
        //		if ([item isKindOfClass:[NSString class]])
        //		{
        //			NSString* labelText = (NSString*) item;
        //			cell.textLabel.text  = labelText;
        //			cell.editingAccessoryType = UITableViewCellAccessoryNone;
        //		}
        //		else
        //		{
        //			Events   *control = (Events* )item;
        //			cell.textLabel.text  = [[control valueForKey:@"Description"] description];
        //			cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
        //		}
        //
        //	}
        
        
        
	}
    /*	if (assessmentObject != nil)
     {
     if ([assessmentObject isKindOfClass:[Assessment class]]) //It is a risk object so update data
     {
     UILabel *cell.textLabel  = (UILabel *)[cell viewWithTag:11];
     UILabel *label12  = (UILabel *)[cell viewWithTag:12];
     Assessment* item = (Assessment*) assessmentObject;
     conditionField.text = [[item valueForKey:@"Conditions"] description];
     temperatureField.text = [[item valueForKey:@"temperature"] description];
     NSDate * assementDate = [item valueForKey:@"AssessmentDate"];
     label12.text =  [formatter stringFromDate:assementDate];
     if (item.Description != nil)
     {
     cell.textLabel.text = [[item valueForKey:@"Description"] description];
     cell.textLabel.textColor = [UIColor blackColor];
     }
     
     else
     {
     cell.textLabel.text = @"Assessment Type";
     cell.textLabel.textColor = [UIColor blackColor];
     }
     
     
     
     }
     }
     */
    cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    

    
    
    if (sectionRow == 0)
    {
        
     	if (selectedObject.CurrentWeatherTemp != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"CurrentWeatherTemp"] description];
            cell.textLabel.textColor = [UIColor blackColor];
            cell.detailTextLabel.text = @"Air Temperature";
        }
        
        else
        {
            cell.textLabel.text = @"Air Temperature";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"CurrentWeatherTemp"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
            
        }
    }
    
    else if (sectionRow == 1)
    {
        
     	if (selectedObject.CurrentWindDirection != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"CurrentWindDirection"] description];
            cell.detailTextLabel.text = @"Wind Direction";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Wind Direction";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"CurrentWindDirection"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 2)
    {
        
     	if (selectedObject.CurrentWindSpeed != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"CurrentWindSpeed"] description];
            cell.detailTextLabel.text = @"Wind Speed";
            cell.textLabel.textColor = [UIColor blackColor];
          
        }
        
        else
        {
            cell.textLabel.text = @"Wind Speed";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"CurrentWindSpeed"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 3)
    {
        
     	if (selectedObject.CurrentCloud != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"CurrentCloud"] description];
            cell.detailTextLabel.text = @"Weather";
            cell.textLabel.textColor = [UIColor blackColor];
            
        }
        
        else
        {
            cell.textLabel.text = @"Weather";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"CurrentCloud"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 10)
    {
        
     	if (selectedObject.PredictionWeatherTemp != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"PredictionWeatherTemp"] description];
            cell.detailTextLabel.text = @"Air Temperature";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Air Temperature";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"PredictionWeatherTemp"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 11)
    {
        
     	if (selectedObject.PredictionWindDirection != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"PredictionWindDirection"] description];
            cell.detailTextLabel.text = @"Wind Direction";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Wind Direction";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"PredictionWindDirection"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 12)
    {
        
     	if (selectedObject.PredictionWindSpeed != nil)
        {
            cell.textLabel.text = [[selectedObject valueForKey:@"PredictionWindSpeed"] description];
            cell.detailTextLabel.text = @"Wind Speed";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Wind Speed";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"PredictionWindSpeed"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
    else if (sectionRow == 13)
    {
        
     	if (selectedObject.PredictionCloud != nil)
        {
            cell.textLabel.text = selectedObject.PredictionCloud ;
            cell.detailTextLabel.text = @"Weather";
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
            cell.textLabel.text = @"Weather";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.detailTextLabel.text = nil;
            if  ([delegate isFieldInError:@"PredictionCloud"] == YES)
                cell.textLabel.textColor = [UIColor redColor];
        }
    }
        
    
    // Configure the cell...
       cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	NSUInteger section = [indexPath section];
	if (section == EVENTS )  //if add items then indent
		return YES;
	
	return NO;
}
/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	if (section == EVENTS)
	{
		NSManagedObject *item = [eventItems objectAtIndex:row];
		if ([item isKindOfClass:[NSString class]]) //It is a sting object row must be add mode
		{
            Events*  events = [[Events alloc] initWithNibName: @"Events" bundle:nil];
            [events setupData:selectedObject];
            
            [[self navigationController] pushViewController:events animated:YES];
          	
		}
		else //else show delete option
		{
            [selectedObject removeEventsObject:item];
            [self._tableView reloadData];
			;
		}
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
    WindDirectionTemplates*  windDirectionTemplates;
    WindSpeedTemplates*  windSpeedTemplates;
    SelectTemperature *selectTemperature;
     CloudCover * cloudCover;
    EnterValue*  enterValue;
    if (sectionRow  == 1)
	{
		windDirectionTemplates = [[WindDirectionTemplates alloc] initWithNibName: @"WindDirectionTemplates" bundle:nil];
        [windDirectionTemplates setupData:selectedObject entityKeyString:@"CurrentWindDirection"];
	    UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:windDirectionTemplates] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 300, 504);
        toolsNavController.title = @"Select Photo";
        toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentModalViewController:toolsNavController animated:YES];

		
	}
    else if (sectionRow  == 2)
	{
		windSpeedTemplates = [[WindSpeedTemplates alloc] initWithNibName: @"WindSpeedTemplates" bundle:nil];
        [windSpeedTemplates setupData:selectedObject entityKeyString:@"CurrentWindSpeed"];
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:windSpeedTemplates] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
            toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentModalViewController:toolsNavController animated:YES];
		
	}
    else if (sectionRow  == 3)
	{
		cloudCover = [[CloudCover alloc] initWithNibName: @"CloudCover" bundle:nil];
        [cloudCover setupData:selectedObject entityKeyString:@"CurrentCloud"];
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:cloudCover] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
            toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentModalViewController:toolsNavController animated:YES];
		
	}
    else if (sectionRow  == 0)
	{
        selectTemperature = [[SelectTemperature alloc] initWithNibName: @"SelectTemperature" bundle:nil];
        [selectTemperature setupData:selectedObject entityKeyString:@"CurrentWeatherTemp" entityLabelString:@"Current Weather" parent:@"EventViewControllerWeather"];
        
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:selectTemperature] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:toolsNavController animated:YES];
        
		
	}
    else if (sectionRow  == 10)
	{
        selectTemperature = [[SelectTemperature alloc] initWithNibName: @"SelectTemperature" bundle:nil];
        [selectTemperature setupData:selectedObject entityKeyString:@"PredictionWeatherTemp" entityLabelString:@"Prediction Weather" parent:@"EventViewControllerWeather"];
	 
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:selectTemperature] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
            toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:toolsNavController animated:YES];
        
		
	}
    else if (sectionRow  == 11)
	{
		windDirectionTemplates = [[WindDirectionTemplates alloc] initWithNibName: @"WindDirectionTemplates" bundle:nil];
        [windDirectionTemplates setupData:selectedObject entityKeyString:@"PredictionWindDirection"];
        
	    UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:windDirectionTemplates] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
            toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentModalViewController:toolsNavController animated:YES];
        
	}
    else if (sectionRow  == 12)
	{
		windSpeedTemplates = [[WindSpeedTemplates alloc] initWithNibName: @"WindSpeedTemplates" bundle:nil];
        [windSpeedTemplates setupData:selectedObject entityKeyString:@"PredictionWindSpeed"];
        
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:windSpeedTemplates] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
            toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentModalViewController:toolsNavController animated:YES];
        
		
	}
    else if (sectionRow  == 13)
	{
		cloudCover = [[CloudCover alloc] initWithNibName: @"CloudCover" bundle:nil];
        [cloudCover setupData:selectedObject entityKeyString:@"PredictionCloud"];
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:cloudCover] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 416, 320);
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
            toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:toolsNavController animated:YES];
		
	}
       
}






@end

