//  SingleImageCell.m
//  SLSA Risk Assessment
//
//  Created by Julian on 21/04/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "SingleImageCell.h"
#import "ChooseImage.h"
#import "RiskMatrix.h"
#import "EventAppDelegate.h"

// Are we compiling for the device, simulator, or desktop?
#if TARGET_OS_IPHONE && TARGET_IPHONE_SIMULATOR
#define CAMERA 0

#elif TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR
#define CAMERA 1

#else
#define CAMERA 0

#endif
@implementation SingleImageCell
@synthesize entityKey;
@synthesize mapThumbnail;
@synthesize celllabelText;
@synthesize resend,updateButton;
@synthesize parentController;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
      return self;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

   }

-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString cameraImage:(bool)cameraImage riskImage:(bool)riskImage imageList:(bool)imageList
{
    isCameraImage = cameraImage;
    isRiskImage = riskImage;
    entity = entityObject;
    
    if (entityObject == nil)
    {
        mapThumbnail.hidden = YES;
        return;
    }
    celllabelText.textColor = [UIColor lightGrayColor];
    
    isImageList = imageList;
    entityKey = entityKeyString;
    celllabelText.text = entityLabelString;
    NSLog(@"%@ %@ ",entityLabelString , entityKey);
    queueName = [[NSString alloc] initWithFormat:@"%f.jpg",[[NSDate date] timeIntervalSince1970]] ;
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:queueName
     object:nil];  
   
    [updateButton addTarget:self action:@selector(UpdateButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self updateImage];
}


-(void)updateImage
{
    if (entity == nil)
        return;
    
     
    if ([entity isKindOfClass:[NSString class]]) 
    {
        mapThumbnail.hidden = YES;
        
        return;
    }
    if (entity == nil)
    {
        mapThumbnail.hidden = YES;
        
        return;
    }
    mapThumbnail.hidden = NO;
    if (isImageList == YES)
    {
        celllabelText.hidden = YES;
        resend.hidden = NO;
    }
    if (isRiskImage == YES)
    {
         celllabelText.textColor = [UIColor blackColor];
        NSString* consequenceColum = [NSString stringWithFormat:@"%@Consequence",entityKey];
        NSString* likelihoodColum = [NSString stringWithFormat:@"%@Likelihood",entityKey];
        NSString *riskimage = nil;
        if ([entity valueForKey:likelihoodColum] == nil)
            return;
        int row =  	 [[entity valueForKey:likelihoodColum] intValue];
        switch (row) {
            case 5:
                row = 0;
                break;
            case 4:
                row = 1;
                break;
            case 3:
                row = 2;
                break;
            case 2:
                row = 3;
                break;
            case 1:
                row = 4;
                break;
            default:
                break;
        }

        int column =   [[entity valueForKey:consequenceColum] intValue];
        if (column == 0)
            row++;
        
        int buttontag = row*5;
        buttontag += column;
        
     
        
        switch (buttontag)
        {
            case 6:
                riskimage = @"img_L4.png";
                break;
            case 11:
                riskimage = @"img_L3.png";
                break;
            case 16:
                riskimage = @"img_L2.png";
                break;
            case 17:
                riskimage = @"img_L4.png";
                break;
            case 21:
                riskimage = @"img_L1.png";
                break;
            case 22:
                riskimage = @"img_L2.png";
                break;
            case 23:
                riskimage = @"img_L3.png";
                break;
            case 1:
                riskimage = @"img_M4.png";
                break;
            case 7:
                riskimage = @"img_M8.png";
                break;
            case 12:
                riskimage = @"img_M6.png";
                break;
            case 18:
                riskimage = @"img_M6.png";
                break;
            case 24:
                riskimage = @"img_M4.png";
                break;
            case 25:
                riskimage = @"img_M5.png";
                break;
                
            case 2:
                riskimage = @"img_H10.png";
                break;
            case 3:
                riskimage = @"img_H15.png";
                break;
            case 8:
                riskimage = @"img_H12.png";
                break;
            case 13:
                riskimage = @"img_H9.png";
                break;
            case 14:
                riskimage = @"img_H12.png";
                break;
            case 19:
                riskimage = @"img_H8.png";
                break;
            case 20:
                riskimage = @"img_H10.png";
                break;
                
            case 4:
                riskimage = @"img_E20.png";
                break;
            case 5:
                riskimage = @"img_E25.png";
                break;
            case 9:
                riskimage = @"img_E16.png";
                break;
            case 10:
                riskimage = @"img_E20.png";
                break;
            case 15:
                
                riskimage = @"img_E15.png";
                break;
                
            default:
                break;
        }

        
        CGRect frame =   mapThumbnail.frame;
        frame.size.height = 53;
        frame.size.width = 53;
    //    frame.origin.x = 450;

        mapThumbnail.frame = frame;
        UIImage *image  = [UIImage imageNamed:riskimage];
        [mapThumbnail setImage:image];
        
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        if (!documentsDirectory) {
            NSLog(@"Documents directory not found!");
        }     
        NSString *_imageName = [entity valueForKey:entityKey];
        if (_imageName != nil && [_imageName length])
        {
              resend.hidden = NO;
        }
        NSString *smallName = [NSString stringWithFormat:@"small_%@",_imageName];
        
        NSString *appFile = [documentsDirectory stringByAppendingPathComponent:smallName];
        
        if ([entity isKindOfClass:[Photo class]] || [entity isKindOfClass:[Assessment class]]) //It is a sting object row must be add mode
        {
            
               UIImage *image  = [UIImage imageWithContentsOfFile:appFile];
                
                [mapThumbnail setImage:image];
           
        }
       
        else
        {
            UIImage *image  = [UIImage imageWithContentsOfFile:appFile];
            
            [mapThumbnail setImage:image];
            
        }
        
        
   //     
    }
}
-(void)handleNotification:(NSNotification *)pNotification
{
         //close the popover view
          [self updateImage];        
    [parentController dismissModalViewControllerAnimated:YES];
    
}
-(IBAction)ResendButtonPressed:(id)sender
{
    NSString *parrentRecord = nil;
    
    NSString *_imageName = [entity valueForKey:entityKey];
    
    NSString *smallName = [NSString stringWithFormat:@"small_%@",_imageName];
    
    
    if ([entity isKindOfClass:[Photo class]]) 
    {
        Photo* objectitem = (Photo*) entity;
        if (objectitem.Hazard != nil)
        {
            entity =    objectitem.Hazard;
  
            
        }
        else
        {
            return;
            
            
        }

    }
    
    
        
}
-(IBAction)UpdateButtonPressed:(id)sender
{
    if([popoverController isPopoverVisible])
    {
        //close the popover view if toolbar button was touched
        //again and popover is already visible
        
        [popoverController dismissPopoverAnimated:YES];
        return;
    }
    
    UIButton *button = (UIButton*) sender;
    CGRect buttonFrameInDetailView = button.frame;
     if (isRiskImage == YES)
    {
        NSString*  NibName;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    
            NibName = @"RiskMatrixPad";
        else
            NibName = @"RiskMatrix";
        
          RiskMatrix *popoverContent = [[RiskMatrix alloc] initWithNibName:NibName bundle:nil];

        
        //resize the popover view shown
        //in the current view to the view's size
 
        
        //create a popover controller
         popoverContent.notifyQueue = queueName;
        [popoverContent setupData:entity entityKeyString:entityKey entityLabelString:celllabelText.text];
        //present the popover view non-modal with a
        //refrence to the toolbar button which was pressed
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent] ;
      
        NSString *navBar;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            navBar = @"ipad_img_blue-header.png";
        }
        else {
            navBar = @"iphone_img_blueheader.png";
        }
        
        UIImage *image = [UIImage imageNamed:navBar];
        [toolsNavController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
            toolsNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        toolsNavController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                         [UIColor whiteColor], UITextAttributeTextColor,
                                                                         [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                         [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                         
                                                                         nil];
       [parentController presentModalViewController:toolsNavController animated:YES];
        //release the popover content
     
    }
      
    else
    {
      
     
        ChooseImage *popoverContent = [[ChooseImage alloc] initWithNibName:@"ChooseImage" bundle:nil];

        
        //resize the popover view shown
        //in the current view to the view's size
        popoverContent.contentSizeForViewInPopover =
        CGSizeMake(380, 544);
        
        //create a popover controller
        popoverController = [[UIPopoverController alloc]
                             initWithContentViewController:popoverContent];
         [popoverContent setupData:entity entityKeyString:entityKey  queue:queueName];
        
        popoverContent.label = celllabelText.text;
         //present the popover view non-modal with a
        //refrence to the toolbar button which was pressed
        UINavigationController *toolsNavController = [[UINavigationController alloc] initWithRootViewController:popoverContent] ;
        toolsNavController.view.frame = CGRectMake(0.0, -10.0, 380, 544);
        
        //create a popover controller
        popoverController = [[UIPopoverController alloc]
                             initWithContentViewController:toolsNavController];
        [popoverController presentPopoverFromRect:buttonFrameInDetailView
                                           inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        
        

        }    

}

@end
