//
//  LocalSite.m
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "LocalSite.h"
#import "Assessment.h"
#import "GovernmentArea.h"


@implementation LocalSite

@dynamic Longitude;
@dynamic Latitute;
@dynamic Name;
@dynamic BeachKey;
@dynamic Assessments;
@dynamic GovernmentArea;

@end
