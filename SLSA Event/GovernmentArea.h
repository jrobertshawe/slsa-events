//
//  GovernmentArea.h
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class LocalSite;

@interface GovernmentArea : NSManagedObject

@property (nonatomic, retain) NSString * PhoneNumber;
@property (nonatomic, retain) NSString * ContactName;
@property (nonatomic, retain) NSString * Address;
@property (nonatomic, retain) NSNumber * Order;
@property (nonatomic, retain) NSString * Name;
@property (nonatomic, retain) NSString * Description;
@property (nonatomic, retain) NSSet *Sites;
@end

@interface GovernmentArea (CoreDataGeneratedAccessors)

- (void)addSitesObject:(LocalSite *)value;
- (void)removeSitesObject:(LocalSite *)value;
- (void)addSites:(NSSet *)values;
- (void)removeSites:(NSSet *)values;

@end
