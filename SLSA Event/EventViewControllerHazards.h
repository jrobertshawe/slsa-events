//
//  EventViewControllerHazards.h
//  SLSA Event
//
//  Created by Julian robertshawe on 1/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"
#import "HazardTemplates.h"
#import "GenerateReport.h"
@interface EventViewControllerHazards : UIViewController< UITableViewDelegate, UITableViewDataSource >
{
    NSMutableArray								*Items;
    Assessment*                                  assessment;
    
}
@property (nonatomic, retain) IBOutlet UIView		*footerView;
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (nonatomic, retain) IBOutlet UILabel		*footerLabel;
-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite;
- (void) didTapBackButton:(id)sender;
- (IBAction)CreateReport:(id)sender;
@end
