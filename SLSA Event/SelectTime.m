//
//  SelectTime.m
//  SLSARiskAssessment
//
//  Created by Julian on 25/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "SelectTime.h"
#import "Assessment.h"
#import "EventAppDelegate.h"
@implementation SelectTime
@synthesize datePicker;
@synthesize timeType;
@synthesize timeLabel;
@synthesize timeValue;
@synthesize promptText;
@synthesize titleText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityKeyStringValue:(NSString*) entityKeyStringValue
{
   
    selectedObject = entityObject;
    
     recordkey = [[NSString alloc] initWithString:entityKeyString];
    recordValuekey = [[NSString alloc] initWithString:entityKeyStringValue];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(IBAction)done
{
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
     self.navigationItem.title = titleText;
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
}
- (void) didTapBackButton:(id)sender {
    
    
    
         [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshView" object:nil userInfo:nil];
    [self dismissModalViewControllerAnimated:YES];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)viewDidAppear:(BOOL)animated
{
    Assessment *assessment = (Assessment*)selectedObject;
    timeValue.text  =  [assessment valueForKey:recordkey];
    NSDate* currentDate = [assessment valueForKey:recordValuekey];
    if (currentDate != nil)
        [datePicker setDate:currentDate];
    [super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    Assessment *assessment = (Assessment*)selectedObject;
    [assessment setValue:timeValue.text forKey:recordkey];
     [assessment setValue:[datePicker date] forKey:recordValuekey];
     [super viewWillDisappear:animated];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(IBAction)dateChanged
{
   NSDate * pickerDate = [datePicker date];
	
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:kCFDateFormatterNoStyle];
	[formatter setTimeStyle:kCFDateFormatterShortStyle];
    
    
    	timeValue.text = [formatter stringFromDate:pickerDate];

}
@end
