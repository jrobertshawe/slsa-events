//
//  EventViewControllerAssessment.h
//  SLSA Event
//
//  Created by Julian robertshawe on 20/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"
#import "EventViewControllerAssessmentDetail.h"
#import "EventViewControllerAssessmentMenu.h"
#import "EventViewControllerAreaDetail.h"
#import "AssessmentTemplates.h"
#import "SelectItemItem.h"
@interface EventViewControllerAssessment : UIViewController< UITableViewDelegate, UITableViewDataSource >
{
    NSMutableArray								*Items;
    LocalSite*                                  site;
    bool                                    isCopying;
}
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (nonatomic, retain) IBOutlet UIView		*footerView;
-(void)setupData:(LocalSite*) entityObject;
- (void) didTapBackButton:(id)sender;
- (void)TurnOffCopy;
@end
