//
//  EventViewControllerEventDetail.m
//  SLSA Event
//
//  Created by Julian robertshawe on 17/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "EventViewControllerEventDetail.h"
#import "SingleEditCell.h"
@interface EventViewControllerEventDetail ()

@end

@implementation EventViewControllerEventDetail

@synthesize _tableView;
@synthesize nameField;
@synthesize currentTextField;
@synthesize addressSection;
@synthesize contactNameField;
@synthesize descriptionField;
@synthesize addressField;
@synthesize phoneNumberField;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
	
     backButton = [UIButton buttonWithType:UIButtonTypeCustom];
     buttonImage = [UIImage imageNamed:@"but_done_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(insertNewObject) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
  //  self.navigationItem.rightBarButtonItem = backButtonItem;
	
    
     
    self.navigationItem.title = @"Venue";
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    if (object == nil)
        object = [delegate AddGovernmentArea:@"" Description:@"" PhoneNumber:@""  ContactName:@"" Address:@"" ];
    
   
     [delegate.fieldinError removeAllObjects];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (void) didTapBackButton:(id)sender {
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    if ([object.Name length] >0 )
    {
        [delegate saveContext];
        if(self.navigationController.viewControllers.count > 1) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else
    {
        [delegate.fieldinError addObject:@"Name"];
        [_tableView reloadData];
    }
}
-(void)setupData:(GovernmentArea*) entityObject

{
     EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    if (entityObject == nil)
        object = [delegate AddGovernmentArea:@"" Description:@"" PhoneNumber:@""  ContactName:@"" Address:@"" ];
    else
        object = entityObject;
}
- (void)viewWillAppear:(BOOL)animated {
    
      [_tableView reloadData];

}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// custom rotation code based on interfaceOrientation here...
	return NO;
}
- (void)cancelRequest
{
	[[self navigationController] popViewControllerAnimated:YES];
}
- (void)insertNewObject
{
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
	NSString* name = nameField.text;
	NSString* description = descriptionField.text;
	NSString* address = addressField.text;
	NSString* contactName = contactNameField.text;
	NSString* phoneNumber = phoneNumberField.text;
    
    if ([object isKindOfClass:[GovernmentArea class]]) //It is a risk object so update data
    {
        GovernmentArea* item = (GovernmentArea*) object;
        [item setValue:name forKey:@"Name"];
        [item setValue:description forKey:@"Description"];
        [item setValue:address forKey:@"Address"];
        [item setValue:phoneNumber forKey:@"PhoneNumber"];
        [item setValue:contactName forKey:@"ContactName"];
    }
	else
	{
		[delegate AddGovernmentArea:name Description:description PhoneNumber:phoneNumber  ContactName:contactName Address:address ];
	}
	[delegate saveContext];
    
    [[self navigationController] popViewControllerAnimated:YES];
    
}




/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    addressSection = 0;
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UIView* hv = [[UIView alloc] initWithFrame:CGRectMake(0,0,delegate.header_length,delegate.header_height)];
    NSString* imagefile = [NSString stringWithFormat:@"%@-Nav3.png",delegate.imagePrefix];
    
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imagefile]];
    
    [hv addSubview:image];
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 4;
}
- (IBAction)textFieldDoneEditing:(id)sender {
    
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSUInteger sectionRow = (section * 10) + row;
    UITableViewCell *tblcell;
      if (sectionRow == DETAILS)
        {
            SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SingleEditCell"];
            if (cell == nil) {
                cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
                
            }
            
            [cell setupData:object entityKeyString:@"Name" entityLabelString:@"Name" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerArea"];
            cell.parentController = self;
            tblcell = cell;
        }
        else    if (sectionRow == DETAIL_SECTION_LINE2_ROW)
        {
            SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SingleEditCell"];
            if (cell == nil) {
                cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
                
            }
            
            [cell setupData:object entityKeyString:@"Description" entityLabelString:@"Description" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerArea"];
            cell.parentController = self;
            tblcell = cell;
            
        }
        else    if (sectionRow == DETAIL_SECTION_LINE3_ROW)
        {
            SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SingleEditCell"];
            if (cell == nil) {
                cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
                
            }
            
            [cell setupData:object entityKeyString:@"ContactName" entityLabelString:@"Contact Name" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerArea"];
            cell.parentController = self;
            tblcell = cell;
            
        }
        else    if (sectionRow == DETAIL_SECTION_LINE4_ROW)
        {
            SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SingleEditCell"];
            if (cell == nil) {
                cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
                
            }
            
            [cell setupData:object entityKeyString:@"Address" entityLabelString:@"Email Address" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerArea"];
            cell.parentController = self;
            tblcell = cell;
            
        }
        else    if (sectionRow == DETAIL_SECTION_LINE5_ROW)
        {
            SingleEditCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[SingleEditCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
                
            }
            
            [cell setupData:object entityKeyString:@"PhoneNumber" entityLabelString:@"Phone Number" entitySelectList:nil  entitySelectListType:0 parentQueue:@"EventViewControllerArea"];
            cell.parentController = self;
            tblcell = cell;
            
        }
        // Configure the cell...
      tblcell.accessoryType =    UITableViewCellAccessoryDisclosureIndicator;
       tblcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return tblcell;
}






///
/// Only use this function for rows in the table SECTIONS not the HEADER
///




#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	/*
	 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
	 [self.navigationController pushViewController:detailViewController animated:YES];
	 [detailViewController release];
	 */
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    _tableView = nil;
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}




@end
