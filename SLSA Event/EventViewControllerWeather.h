//
//  EventViewControllerWeather.h
//  SLSA Event
//
//  Created by Julian robertshawe on 3/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"

#define HEADER_FIELD_TAG_1 0
#define HEADER_FIELD_TAG_2 1


///Table Cell Indexes and Identifiers
#define DETAILS 2
#define CURRENTWEATHER 0
#define CURRENTSURF 3
#define PREDICTIONWEATHER 1
#define PREDICTIONSURF 4
#define EVENTS 5



@interface EventViewControllerWeather : UIViewController< UITableViewDelegate, UITableViewDataSource>
{
	IBOutlet UITextField			*conditionField;
    IBOutlet UITextField		*temperatureField;
	IBOutlet UITextField			*descriptionField;
	NSString *objectString;
    bool newRecord;
    bool firstTime;
    NSMutableArray					*eventItems;
    GovernmentArea* governmentArea;
    LocalSite* localSiteObject;
    
    Assessment* selectedObject;
}
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (nonatomic, retain) GovernmentArea* governmentArea;
-(void)handleNotification:(NSNotification *)pNotification;
- (void)cancelRequest;
- (void)saveObject;
-(void)setupData:(Assessment*) entityObject localSite:(LocalSite*)localSite;
@end