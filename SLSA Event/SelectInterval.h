//
//  SelectInterval.h
//  AussieIpad
//
//  Created by Julian robertshawe on 18/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"
@interface SelectInterval : UIViewController <UIPickerViewDelegate>
{
    NSString*       entityString; 
    NSString*       entityParentString; 
      NSManagedObject*       entity; 
    NSString*               entityKey;
    NSString*               label; 
    NSString*               notifyQueue;  
    IBOutlet UILabel *      labelField;
       IBOutlet UIPickerView *intervalSelectPicker;
       NSMutableArray *intervals;
    NSString *stringValue;
}
    @property (nonatomic,retain)  NSString						*notifyQueue;
    @property (nonatomic,retain)  IBOutlet UIPickerView                  *intervalSelectPicker;
@property (nonatomic,retain)  IBOutlet UILabel                  *labelField;

-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityLabelString:(NSString*) entityLabelString parent:(NSString*) parentString;
-(IBAction)DoneButtonPressed;
- (IBAction)stepperPressed:(id)sender;
@end
