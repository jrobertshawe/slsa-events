//
//  SelectFixedItemList.h
//  SLSA Risk Assessment
//
//  Created by Julian on 6/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SelectionValues.h"
@interface SelectFixedItemList : UITableViewController {
    NSManagedObject*                   entity;
    NSString*               	entityKey;
    NSString*                   entitySelectList;
    NSString*               label; 
    NSString*                 notifyQueue; 
    int                        selectListType;
    NSMutableArray              *items;
    NSIndexPath                 *currentSelected;

}
@property (nonatomic,retain)     NSString*               label; 

-(IBAction)DoneButtonPressed;
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString entityselectListType:(int) entityselectListType  entitySelectListString:(NSString*) entitySelectListString  parentQueue:(NSString*) parentQueue;
@end
