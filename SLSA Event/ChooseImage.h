//
//  ChooseImage.h
//  SLSA Risk Assessment
//
//  Created by Julian on 5/05/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "photo.h"
#import "UIImage+Resize.h"
@interface ChooseImage : UIViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate> {
     NSString*               entityKey;
    NSManagedObject*               selectedObject;
     NSString*               photoobjectString;
    UIImagePickerController *imagePicker;
     IBOutlet UIImageView             *imageview;
        NSString*               label; 
      NSString*               notifyQueue;  
     UIPopoverController *popoverController;
    
}

@property (nonatomic,retain)          NSString*               label; 
 @property (nonatomic,retain) IBOutlet UIToolbar             *toolbar;
 @property (nonatomic,retain) IBOutlet UIImageView             *imageview;
- (void)saveImage:(UIImage *)image withName:(NSString *)name;
- (void)saveImageSmall:(UIImage *)image withName:(NSString *)name;
- (void)saveImageThumb:(UIImage *)image withName:(NSString *)name;
-(IBAction)SelectPhotoPressed: (id)sender;
-(IBAction)DoneButtonPressed;
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString queue:(NSString*) queue ;
-(IBAction)TakePhotoPressed: (id)sender;
@end
