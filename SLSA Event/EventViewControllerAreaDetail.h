//
//  EventViewControllerAreaDetail.h
//  SLSA Event
//
//  Created by Julian robertshawe on 13/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"
#define HEADER_FIELD_TAG_1 0
#define HEADER_FIELD_TAG_2 1


///Table Cell Indexes and Identifiers

#define DETAIL_SECTION_LINE1_ROW 0
#define DETAIL_SECTION_LINE1_TAG 10
#define DETAIL_SECTION_LINE1_PLACEHOLDER @"Name"
#define DETAIL_SECTION_LINE2_ROW 1
#define DETAIL_SECTION_LINE2_TAG 11
#define DETAIL_SECTION_LINE2_PLACEHOLDER @"Area ID (Optional)"




@interface EventViewControllerAreaDetail : UIViewController < UITableViewDelegate, UITableViewDataSource>
{
	IBOutlet UITextField			*nameField;
	IBOutlet UITextField			*idField;
    LocalSite* object;
    
    IBOutlet UITableView *_tableView;
}
@property CGSize keyboardSize;
@property (nonatomic, retain) IBOutlet UITextField *nameField;
@property (nonatomic, retain) IBOutlet UITextField *idField;
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (strong, nonatomic) UITextField *currentTextField;
@property (nonatomic) int addressSection;

- (BOOL)fieldIsInHeader:(UITextField *)textField;
- (void)cancelRequest;
- (void)insertNewObject;
-(void)setupData:(NSString*) entityObject;
- (void)registerForKeyboardNotifications;
-(void)handleRefreshNotification:(NSNotification *)pNotification;
@end