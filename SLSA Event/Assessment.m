//
//  Assessment.m
//  SLSA Event
//
//  Created by Julian robertshawe on 18/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "Assessment.h"
#import "Event.h"
#import "Hazard.h"
#import "LocalSite.h"


@implementation Assessment

@dynamic PredictionCloud;
@dynamic PredictionWaveType;
@dynamic PredictionWindSpeed;
@dynamic PredictionHighTide;
@dynamic CurrentWindSpeed;
@dynamic PredictionHighTideValue;
@dynamic CurrentCloud;
@dynamic CurrentWindDirection;
@dynamic PredictionLowTide;
@dynamic weatherChecked;
@dynamic CurrentTide;
@dynamic Type;
@dynamic CurrentSwellTime;
@dynamic CurrentWeatherTemp;
@dynamic PredictionSwellSize;
@dynamic PredictionLowTideValue;
@dynamic PredictionWindDirection;
@dynamic PredictionWeatherTemp;
@dynamic Assessor;
@dynamic activityChecked;
@dynamic Description;
@dynamic AssessmentDate;
@dynamic Notes;
@dynamic hazardscheck;
@dynamic CurrentSwellSize;
@dynamic CurrentWaveType;
@dynamic OverviewPhoto;
@dynamic PredictionSwellTime;
@dynamic surfChecked;
@dynamic Events;
@dynamic Hazzards;
@dynamic LocalSite;

@end
