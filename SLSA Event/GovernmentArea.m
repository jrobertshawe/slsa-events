//
//  GovernmentArea.m
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import "GovernmentArea.h"
#import "LocalSite.h"


@implementation GovernmentArea

@dynamic PhoneNumber;
@dynamic ContactName;
@dynamic Address;
@dynamic Order;
@dynamic Name;
@dynamic Description;
@dynamic Sites;

@end
