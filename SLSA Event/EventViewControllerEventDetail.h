//
//  EventViewControllerEventDetail.h
//  SLSA Event
//
//  Created by Julian robertshawe on 17/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"
#define HEADER_FIELD_TAG_1 0
#define HEADER_FIELD_TAG_2 1


///Table Cell Indexes and Identifiers

#define DETAILS 0
#define DETAIL_SECTION_LINE1_TAG 10
#define DETAIL_SECTION_LINE1_PLACEHOLDER @"Name"
#define DETAIL_SECTION_LINE2_ROW 1
#define DETAIL_SECTION_LINE2_TAG 11
#define DETAIL_SECTION_LINE2_PLACEHOLDER @"Description (Optional)"
#define DETAIL_SECTION_LINE3_ROW 2
#define DETAIL_SECTION_LINE3_TAG 12
#define DETAIL_SECTION_LINE3_PLACEHOLDER @"Contact Name"
#define DETAIL_SECTION_LINE4_ROW 3
#define DETAIL_SECTION_LINE4_TAG 13
#define DETAIL_SECTION_LINE4_PLACEHOLDER @"Email Address"
#define DETAIL_SECTION_LINE5_ROW 4
#define DETAIL_SECTION_LINE5_TAG 14
#define DETAIL_SECTION_LINE5_PLACEHOLDER @"Phone Number"
#define DETAIL_SECTION_LINE6_ROW 5
#define DETAIL_SECTION_LINE6_TAG 15
#define DETAIL_SECTION_LINE7_PLACEHOLDER @"Description (Optional)"


@interface EventViewControllerEventDetail : UIViewController< UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate >
{
IBOutlet UITextField			*nameField;
IBOutlet UITextField			*descriptionField;
IBOutlet UITextField			*contactNameField;
IBOutlet UITextField			*addressField;
IBOutlet UITextField			*phoneNumberField;
GovernmentArea* object;
CGSize keyboardSize;
IBOutlet UITableView *_tableView;
}

@property (nonatomic, retain) IBOutlet UITextField *nameField;
@property (nonatomic, retain) IBOutlet UITextField *addressField;
@property (nonatomic, retain) IBOutlet UITextField *contactNameField;
@property (nonatomic, retain) IBOutlet UITextField *phoneNumberField;
@property (nonatomic, retain) IBOutlet UITextField *descriptionField;
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (strong, nonatomic) UITextField *currentTextField;
@property (nonatomic) int addressSection;

- (BOOL)fieldIsInHeader:(UITextField *)textField;
- (void)cancelRequest;
- (void)insertNewObject;
-(void)setupData:(GovernmentArea*) entityObject;
- (void)registerForKeyboardNotifications;
@end