//
//  Assessment.h
//  SLSA Event
//
//  Created by Julian robertshawe on 18/10/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Event, Hazard, LocalSite;

@interface Assessment : NSManagedObject

@property (nonatomic, retain) NSString * PredictionCloud;
@property (nonatomic, retain) NSString * PredictionWaveType;
@property (nonatomic, retain) NSString * PredictionWindSpeed;
@property (nonatomic, retain) NSString * PredictionHighTide;
@property (nonatomic, retain) NSString * CurrentWindSpeed;
@property (nonatomic, retain) NSDate * PredictionHighTideValue;
@property (nonatomic, retain) NSString * CurrentCloud;
@property (nonatomic, retain) NSString * CurrentWindDirection;
@property (nonatomic, retain) NSString * PredictionLowTide;
@property (nonatomic, retain) NSNumber * weatherChecked;
@property (nonatomic, retain) NSString * CurrentTide;
@property (nonatomic, retain) NSString * Type;
@property (nonatomic, retain) NSString * CurrentSwellTime;
@property (nonatomic, retain) NSString * CurrentWeatherTemp;
@property (nonatomic, retain) NSString * PredictionSwellSize;
@property (nonatomic, retain) NSDate * PredictionLowTideValue;
@property (nonatomic, retain) NSString * PredictionWindDirection;
@property (nonatomic, retain) NSString * PredictionWeatherTemp;
@property (nonatomic, retain) NSString * Assessor;
@property (nonatomic, retain) NSNumber * activityChecked;
@property (nonatomic, retain) NSString * Description;
@property (nonatomic, retain) NSDate * AssessmentDate;
@property (nonatomic, retain) NSString * Notes;
@property (nonatomic, retain) NSNumber * hazardscheck;
@property (nonatomic, retain) NSString * CurrentSwellSize;
@property (nonatomic, retain) NSString * CurrentWaveType;
@property (nonatomic, retain) NSString * OverviewPhoto;
@property (nonatomic, retain) NSString * PredictionSwellTime;
@property (nonatomic, retain) NSNumber * surfChecked;
@property (nonatomic, retain) NSSet *Events;
@property (nonatomic, retain) NSSet *Hazzards;
@property (nonatomic, retain) LocalSite *LocalSite;
@end

@interface Assessment (CoreDataGeneratedAccessors)

- (void)addEventsObject:(Event *)value;
- (void)removeEventsObject:(Event *)value;
- (void)addEvents:(NSSet *)values;
- (void)removeEvents:(NSSet *)values;

- (void)addHazzardsObject:(Hazard *)value;
- (void)removeHazzardsObject:(Hazard *)value;
- (void)addHazzards:(NSSet *)values;
- (void)removeHazzards:(NSSet *)values;

@end
