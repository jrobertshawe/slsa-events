//
//  SingleLineListControlCell.h
//  SLSA Risk Assessment
//
//  Created by Julian on 9/06/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface SingleLineListControlCell : UITableViewCell {
    IBOutlet UILabel *cellDetailText;
    IBOutlet UILabel *cellEffectText;
    
       UIViewController* parentController;
    NSManagedObject* entity;
    NSString*               	entityKey;
    NSString*                   entitySelectList;
    NSString*               label; 
    NSString*                 notifyQueue; 
    IBOutlet UIButton *updateButton;
}
@property (nonatomic,retain)  IBOutlet UIViewController *parentController;
@property (nonatomic,retain)  UIButton						*updateButton;
@property (nonatomic,retain)  UILabel						*cellDetailText;
@property (nonatomic,retain)  UILabel						*cellEffectText;
-(void)setupData:(NSManagedObject*) entityObject entityKeyString:(NSString*) entityKeyString   entitySelectListString:(NSString*) entitySelectListString  parentQueue:(NSString*) parentQueue;
-(IBAction)UpdateButtonPressed:(id)sender;
@end
