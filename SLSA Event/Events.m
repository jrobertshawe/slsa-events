//
//  Events.m
//  SLSARiskAssessment
//
//  Created by Julian on 15/03/11.
//  Copyright 2011 MISSING PIECE TECHNOLOGY. All rights reserved.
//

#import "Events.h"
#import "EventTemplate.h"
#import "Event.h"
#import "EventDetails.h"
@implementation Events
@synthesize _tableView;
@synthesize currentTextField;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)setupData:(NSManagedObject*) entityObject
{
  selectedObject = entityObject;
    
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
 
	
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonImage = [UIImage imageNamed:@"but_edit_off.png"];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(EditMode) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //self.navigationItem.RightBarButtonItem = rightButtonItem;
    Assessment * assessment = (Assessment*)selectedObject;
    
	selectedItems = [[NSMutableArray alloc] initWithArray:[assessment.Events allObjects]];
    self.navigationController.navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                                                     
                                                                     nil];
    
    self.title = @"Activities";

}
- (void) didTapBackButton:(id)sender {
    
       [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshView" object:nil userInfo:nil];
}
// Implement viewWillAppear: to do additional setup before the view is presented.
- (void)viewWillAppear:(BOOL)animated {
	itemArray = [[NSMutableArray alloc]  init];
	[self._tableView reloadData];
    [super viewWillAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
	
    for (EventTemplate* item in selectedItems)
    {
        [delegate AddEvent:[[item valueForKey:@"name"] description] ParentRecord:selectedObject];
        
    }
    [delegate saveContext];
}		
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    int row = [indexPath row];
	EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    Event  *event = [itemArray objectAtIndex:row ];
    
    if ([event isKindOfClass:[NSString class]])
    {
        int editFieldWidth;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            editFieldWidth =460;
        }
        else
        {
            editFieldWidth =280;
        }

        cell.textLabel.text  = nil;
        UITextField *textField  = [[UITextField alloc] initWithFrame:CGRectMake(10,0, editFieldWidth  , 44)];
        textField.clearButtonMode = UITextFieldViewModeAlways;
        //	[textField addTarget:self action:@selector(textFieldDoneEditing:) forControlEvents:UIControlEventEditingDidEndOnExit];
        textField.textAlignment = UITextAlignmentLeft;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.returnKeyType = UIReturnKeyDone;
        textField.placeholder = @"Enter new item";
        textField.tag = 1;
        [textField addTarget:self
                      action:@selector(textFieldFinished:)  forControlEvents:UIControlEventEditingDidEndOnExit];
        [textField setDelegate:self];
        [cell.contentView addSubview:textField];
         currentTextField = textField;

        
    }
    else
    {
        cell.textLabel.text = [[event valueForKey:@"Description"] description];
        cell.textLabel.textColor = [UIColor blackColor];
        UITextField *textField = (UITextField *)[cell viewWithTag:1];
        if (textField != nil)
            textField.hidden = YES;
        
        //      cell.detailTextLabel.text = [[managedObject valueForKey:@"Description"] description];
    }

    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    for (Events* eventitem in selectedItems )
    {
	if ([[eventitem valueForKey:@"name"] isEqualToString:cell.textLabel.text] == YES)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                NSLog(@"%@ %@",[event valueForKey:@"Description"],cell.textLabel.text);
            }
    }
	  
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
}
- (void)insertNewObject {
	EventDetails*  eventDetails = [[EventDetails alloc] initWithNibName: @"EventDetails" bundle:nil];
	[[self navigationController] pushViewController:eventDetails animated:YES];
	return;
	
}
- (IBAction)textFieldFinished:(id)sender
{
    
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    [currentTextField resignFirstResponder];
    [delegate AddEventTemplate:currentTextField.text DisplayOrder:0];
	
    [currentTextField removeFromSuperview] ;
    currentTextField = nil;
    [_tableView reloadData];
    //  [self EditButtonPressed];
    
    
    // [sender resignFirstResponder];
}
- (void)EditMode {
	if (_tableView.isEditing == YES)
	{
		[self._tableView setEditing:NO animated:YES];
		UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(EditMode)];
		self.navigationItem.rightBarButtonItem = addButton;

	}
	
	else
	{
		UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(EditMode)];
		self.navigationItem.rightBarButtonItem = addButton;
	
		[self._tableView setEditing:YES animated:YES];
		
	}
	
	return;
}

#pragma mark -
#pragma mark Add a new object



#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    Assessment * assessment = (Assessment*)selectedObject;
    EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];

    [selectedItems removeAllObjects];
	[selectedItems addObjectsFromArray:[assessment.Events allObjects]];
    [itemArray removeAllObjects];
    NSSortDescriptor* descriptor;
    descriptor = [[NSSortDescriptor alloc] initWithKey:@"Description" ascending:YES];
    [itemArray addObjectsFromArray:[[delegate GetEventTemlates]sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]]];
    
    [ itemArray insertObject:@"Add Activity" atIndex:0];   //add temp
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [itemArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    // Configure the cell.
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}



/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		int row = [indexPath row];
		EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
        NSManagedObject *managedObject = [itemArray objectAtIndex:[indexPath row]];
		[delegate DeleteRecord:managedObject];
		
		[self._tableView reloadData];
	}   
	else if (editingStyle == UITableViewCellEditingStyleInsert) {
		// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}   
	
	
}





#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	int row = [indexPath row];
	
	EventAppDelegate *delegate = (EventAppDelegate* )[[UIApplication sharedApplication] delegate];
    EventTemplate  *event = [itemArray objectAtIndex:row ];
    Assessment * assessment = (Assessment*)selectedObject;

	if (_tableView.isEditing == YES)
	{
	//	RiskGroupDetails*  riskDetails = [[RiskGroupDetails alloc] initWithNibName: @"RiskGroupDetails" bundle:nil];
	//	riskDetails.selectedObject = risk;
		
	//	[[self navigationController] pushViewController:riskDetails animated:YES];
	//	[riskDetails release];	
	}
	else
	{
        
		UITableViewCell *thisCell = [tableView cellForRowAtIndexPath:indexPath];
		
		
		if (thisCell.accessoryType == UITableViewCellAccessoryNone)
		{
            if ([event isKindOfClass:[NSManagedObject class]])
            {
			thisCell.accessoryType = UITableViewCellAccessoryCheckmark;
            [delegate AddEvent:[[event valueForKey:@"Description"] description] ParentRecord:assessment];
            
            }
		}
		else
		{
            for (NSManagedObject* eventitem in selectedItems )
            {
                if ([[eventitem valueForKey:@"name"] isEqualToString:[event valueForKey:@"Description"]] == YES)
                {
                    [delegate.managedObjectContext deleteObject:eventitem];

                }
            }

		
           
		}
                     
	}
                     [tableView reloadData];
	return;
	
	
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    NSManagedObject *managedObject = [itemArray objectAtIndex:[indexPath row]];
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        return UITableViewCellEditingStyleDelete;
    }
    else
        return UITableViewCellEditingStyleNone;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *managedObject = [itemArray objectAtIndex:[indexPath row]];
    
    if ([managedObject isKindOfClass:[NSManagedObject class]])
    {
        return YES;
    }
    else
        return NO;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
