//
//  EventViewControllerHome.h
//  SLSA Event
//
//  Created by Julian robertshawe on 12/09/12.
//  Copyright (c) 2012 Julian robertshawe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventAppDelegate.h"
#import "EventViewControllerArea.h"
@interface EventViewControllerHome : UIViewController < UITableViewDelegate, UITableViewDataSource >
{
    NSMutableArray								*Items;
    bool                                        isEditing;
}
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (nonatomic, retain) IBOutlet UIView		*footerView;
@property (nonatomic, retain) IBOutlet UILabel		*footerLabel;

@end
